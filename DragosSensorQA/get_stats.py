import datetime
import json
import requests

# This defines the AIS endpoint and uses the default admin creds for authentication
ais_url = "https://demo-dev.dragos.services/assets/api/v3/operations"
notification_url = "https://demo-dev.dragos.services/notifications/api/v2/notification"
headers = {"Content-Type": "application/json",
           "Authorization": "Basic YWRtaW46RHJAZ29zU3lzdDNt"}
timeout = 600

now = datetime.datetime.utcnow()
now_minus_90_days = now - datetime.timedelta(days=90)

def single_operation(url, request):
    return_json = requests.post(url, headers=headers, timeout=timeout, data=json.dumps({
        "requests": {
            "r1": request
        }
    }))
    return_json_json = return_json.json()
    return return_json_json["responses"]["r1"]

def get_asset_count(url = ais_url):
    asset_count = single_operation(url, {"requestType": "SEARCH_ASSETS", "pageSize": 999999})["totalCount"]
    print("Total Asset Count: {}".format(asset_count))
    return asset_count

def get_asset_count_by_attribute(url, attribute):
    attributeValue = [[attribute]]
    assets_by_attribute = single_operation(url, {
      "requestType": "GROUP_ASSETS_BY_ATTRIBUTES", 
      "groupByAttributes": attributeValue,
      "from": now_minus_90_days.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z',
      "to": now.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z',
      "onlyInternal": False
    })["summary"]
    
    print("Assets by {}".format(attribute))

    print("  --------------------------------------------------")
    print("  {:<40s}{:>10s}".format(attribute,"count"))
    print("  --------------------------------------------------")
    for group in assets_by_attribute:
      if attribute in group:
        if group[attribute]:
          print("  {:<40s}{:>10d}".format(group[attribute],group["count"]))
        else:
          print("  {:<40s}{:>10d}".format("EMPTY STRING",group["count"]))
      else:
        print("  {:<40s}{:>10d}".format("UNKNOWN",group["count"]))
    print("  --------------------------------------------------")

    return assets_by_attribute

def get_communication_summary(url):
    commmunication_summary = single_operation(url, {
      "requestType": "GET_COMMUNICATIONS_SUMMARY", 
      "from": now_minus_90_days.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z',
      "to": now.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z'
    })["total"]
    
    print("Total Bytes: {}".format(commmunication_summary["total"]["bytes"]))
    print("Total Packets: {}".format(commmunication_summary["total"]["packets"]))
    print("Protocol Breakdown")
    print("  --------------------------------------------------")
    print("  {:<30s}{:>10s}{:>10s}".format("protocol","bytes","packets"))
    print("  --------------------------------------------------")
    for protocol in commmunication_summary["protocols"]:
      print("  {:<30s}{:>10d}{:>10d}".format(protocol["id"],protocol["metrics"]["bytes"],protocol["metrics"]["packets"]))
    print("  --------------------------------------------------")
    return commmunication_summary

def get_notification_count(url = notification_url):
    query_params = {
        "pageNumber": 1,
        "pageSize": 999999,
        "sortField": "id",
        "sortDescending": True,
        "filter": "occurredAt=lt='{}';occurredAt=gt='{}';type!='System'".format(now.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z', now_minus_90_days.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z'),
        "resolveChildrenDepth": 1,
        "ignoreVisibilityPolicy": True,
        "limitTotalCount": 999999
    }
    notification_count = requests.get(url, headers=headers, timeout=timeout, params=query_params).json()["totalCount"]
    print("Total Notification Count: {}".format(notification_count))
    return notification_count

print("Getting Stats")
print("-------------------------------")
get_asset_count()
get_notification_count()
get_asset_count_by_attribute(ais_url, "type")
get_asset_count_by_attribute(ais_url, "Vendor")
get_communication_summary(ais_url)
print("Finished")
print("")
