#!/usr/bin/env python3
import argparse
import os
import datetime
import json
import requests
import ctypes

# Dependencies:
# pip install requests

# Adding support for color in a windows console window is hard
force_linux_color_codes = False
STD_OUTPUT_HANDLE = -11

def color_blue():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 1)
        return ""
    else:
        return '\033[0;34m'


def color_green():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 2)
        return ""
    else:
        return '\033[0;32m'


def color_cyan():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 3)
        return ""
    else:
        return '\033[0;36m'


def color_red():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 4)
        return ""
    else:
        return '\033[0;31m'


def color_reset():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 7)
        return ""
    else:
        return '\033[0;39m'

def get_recursively(search_dict, field):
    """
    Takes a dict with nested lists and dicts,
    and searches all dicts for a key of the field
    provided.
    """
    fields_found = []

    for key, value in search_dict.items():

        if key == field:
            fields_found.append(value)

        elif isinstance(value, dict):
            results = get_recursively(value, field)
            for result in results:
                fields_found.append(result)

        elif isinstance(value, list):
            for item in value:
                if isinstance(item, dict):
                    more_results = get_recursively(item, field)
                    for another_result in more_results:
                        fields_found.append(another_result)

    return fields_found


# Root User Shell Prompt:
ROOT_SHELL_PROMPT = r'\[root@{} ~\]# '
RESET_COMMAND = ('(/var/opt/releases/*_reset.sh ; ec=$? ; if [ ${ec} -eq 0 ] ; '
                 'then echo "-- reset exited successfully --" ; fi) '
                 '| tee /home/%s/reset.log && exit\n')


def delim(character='-', length=80, title=''):
    ''' Add a delimiter of chars, length, with optional title. '''
    head = '{:%s^%ss}' % (character, length)
    header = head.format(title)
    print(header)


def import_json(json_file):
    ''' Given a relative/absolute path to JSON file, return Python dictionary. '''
    with open(json_file, 'r') as jfile:
        data = json.loads(jfile.read())
    return data


def get_hostname(connection):
    ''' Fetch the hostname of the remote system '''
    result = connection.run('hostname', hide=True)
    return result.stdout.split('.')[0]


def ping_test(endpoints):
    ''' Pings a list of target FQDN or IPs '''
    try:
        for endpoint in endpoints:
            subprocess.run(f'ping -c 1 {endpoint}', shell=True, check=True)
    except (FileNotFoundError, subprocess.CalledProcessError):
        print(f'ERROR: Could not ping {endpoint}!.\nAre you connected to the VPN?\n'
              'Is local_env.json populated and valid?')
        sys.exit(1)
    print('INFO: Ping tests passed...\n')
    return True


def reset_system(connection, server, username):
    ''' Reset Sitestore(s) and Midpoint(s) '''

    # Fetch the hostname for the root shell prompt
    hostname = get_hostname(connection)
    # Create a readable logfile in user's home directory
    connection.run('touch reset.log && chmod 755 reset.log')

    # Reset the server using the /var/opt/releases reset script
    delim(title=f' Resetting {server} ')
    try:
        reset_cmd = RESET_COMMAND % (username)
        responder = Responder(pattern=ROOT_SHELL_PROMPT.format(hostname), response=reset_cmd)
        begin = time.time()
        connection.run('sudo sudosh', pty=True, watchers=[responder])

    # When exiting sudosh, it will cause an exception - handle it gracefully
    except UnexpectedExit:
        # Ignore the failing exit code from sudosh and check the log file
        cmd = 'cat /home/{}/reset.log'.format(username)
        log_output = connection.run(cmd, hide=True)
        if '-- reset exited successfully --' in log_output.stdout:
            print(f'INFO: {server} reset was successful (Time elapsed: '
                  f'{round((time.time() - begin), 2)} seconds).')
    else:
        print(f'ERROR: {server} reset has failed!')
        sys.exit(1)


def ssh_connection(system, username, password, key_filename):
    ''' Returns a ssh connection '''
    return Connection(system, username, connect_kwargs={"password": password, "key_filename": key_filename}, connect_timeout=15)
    #return Connection(system, username, connect_kwargs={"password": password}, connect_timeout=15)


def copy_pcaps(connection, pcaps, destination):
    ''' Copy a list of pcaps (abs/relative local paths) to target system (/home/username/pcaps) '''
    for pcap in pcaps:
        filename = pcap.split('/')[-1]
        delim(title=f' Copying pcap: {filename} ')
        connection.put(pcap, destination)


def pcap_playback(connection, pcap_path, mbps, username):
    ''' Playback pcaps on target system '''

    # Fetch the hostname for the root shell prompt
    hostname = get_hostname(connection)
    # Create a readable logfile in user's home directory
    connection.run('touch pcap_playback.log && chmod 755 pcap_playback.log')

    filename = pcap_path.split('/')[-1]
    delim(title=f' pcap Playback: {filename} ')
    cmd = ('(tcpreplay-edit -C --mtu-trunc -i bond0 --stats=30 %s -T nano %s ; ec=$? ; if [ ${ec} -eq 0 ] ; '
           'then echo "--- pcap playback successful ---" ; fi) '
           '| tee /home/%s/pcap_playback.log && exit\n') % (mbps, pcap_path, username)

    try:
        responder = Responder(pattern=ROOT_SHELL_PROMPT.format(hostname), response=cmd)
        begin = time.time()
        connection.run('sudo sudosh', pty=True, watchers=[responder])

    # When exiting sudosh, it will cause an exception - handle it gracefully
    except UnexpectedExit:
        # Ignore the failing exit code from sudosh and check the log file
        cmd = 'cat /home/{}/pcap_playback.log'.format(username)
        log_output = connection.run(cmd, hide=True)
        if '--- pcap playback successful ---' in log_output.stdout:
            print(f'INFO: Playback was successful (Time elapsed: '
                  f'{round((time.time() - begin), 2)} seconds).')
    else:
        print(f'ERROR: {filename} playback has failed!')
        sys.exit(1)


def main():
    now = datetime.datetime.utcnow()
    now_minus_90_days = now - datetime.timedelta(days=90)

    # load config file
    with open(args.config) as f:
        config_dict = json.load(f)

    headers = {"Content-Type": "application/json",
               "Authorization": config_dict["Authorization"]}

    # load test case file
    with open(args.testcases) as f:
        testcases_dict = json.load(f)

    print(testcases_dict)
    for testcase in testcases_dict['tests']:
        print(testcase["name"])
        if testcase["url"] in config_dict:
            url = config_dict[testcase["url"]]
        else:
            url = testcase["url"]
        testresponse = requests.post(
            url,
            headers=headers,
            timeout=args.timeout,
            data=json.dumps({
            "requests": {
                "r1": testcase["request"]
            }
            })
        )
        print(color_reset() + "Test Response")
        print(testresponse)
        if testresponse.ok:
            testresponse_dict = testresponse.json()
            for expected_response_dict in testcase["expected_results"]:
                #print(expected_response_dict)
                matching_keys = get_recursively(testresponse_dict, expected_response_dict["key"])
                #print(matching_keys)
                if expected_response_dict["value"] in matching_keys:
                    print(color_green() + expected_response_dict["name"] + " TEST PASS!" )
                    print("{} == {}".format(expected_response_dict, matching_keys))
                else:
                    print(color_red() + expected_response_dict["name"] + " TEST FAIL!" )
                    print("{} != {}".format(expected_response_dict, matching_keys))
        else:
            print(color_cyan() + "Dragos Platform API Error code returned!")


    # connect to api
    # query and download JSON

    # compare data to expected results
    # print results


if os.name == 'nt':
    std_out_handle = ctypes.windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)
print(color_cyan() + '                   //////////////////////////////////__                    ')
print('         ////////////////////////////////////////////////////////_                        ')
print('    //////////////////      ///////////////////////////                                   ')
print('  ////////               //////////////////////////                                       ')
print('///                  //////////////////////////                                           ')
print('/                 /////////    ////////////                   \\    \\                    ')
print('               ////////      ////////////         \\\\\\\\\\\\\\     \\\\   \\\\          ')
print('             //////         ///////////             /////////////  \\\\\\\\               ')
print('          //////           ///.//////      \\\\\\\\\\\\\\\\\\//////////////////\\\\       ')
print('        ////.            //// /////            /////           ////////\\\\               ')
print('      ///.              ///   ////     \\\\\\\\\\\\\\\\\\//               /////  \\\\\\   ')
print('    ///                 //   ////           ////                   //// .\\\\\\           ')
print('   /                   /     ////     \\\\\\\\\\\\///                   )///////\\\\\\    ')
print(' /                    /     ////        //////                    )////////\\\\\\         ')
print('                            .///   \\\\\\\\\\\\\\////                         //////\\\\  ')
print('                             ////       //////                           /////\\\\        ')
print('                              ////      //////                             ///\\\\\\      ')
print('                               /////     //////                                 \\        ')
print('                                 /////   ///////                                          ')
print('                                   ///////////////                                        ')
print('                                      ///////////////                                     ')
print('                                          ///////////////                                 ')
print('                                              //////////////                              ')
print('                                                  //////////////                          ')
print('                      ______                          ////////////////\\\\\\\\            ')
print('                //////////////////                        ////////////                    ')
print('              ///////////////////////                        /////////////\\\\\\\\\\      ')
print('            ///               ////////                          ///////////               ')
print('           ///                  ////////                          //////////\\\\\\\\      ')
print('           //                     ///////                          //////////\\  \\       ')
print('          //                       ///////                          /////////\\\\         ')
print('          \\\\                        ///////                         //////// \\\\\\     ')
print('          \\\\\\                        ////////                       ///////     \\     ')
print('           \\\\\\                         ///////                     /////               ')
print('            \\\\                         ////////                  /////                  ')
print('              \\\\                        /////////              /////                    ')
print('               \\\\  \\\\                    ////////////////////////                     ')
print('                 \\\\\\\\\\                    ////////////////                           ')
print('               \\\\\\\\\\\\\\\\\\                                                         ')
print(color_red() + "DRAGOS Platform Sensor Test Report")
parser = argparse.ArgumentParser(description='Execute Dragos Platoform Sensor Test Cases ' + color_reset())
parser.add_argument("-config", type=str, default="config.json",
                    help='Specify the path to the test case config file (default: %(default)s)')
parser.add_argument("-testcases", type=str, default="test.json",
                    help='Specify the path to the test case definition file (default: %(default)s)')
parser.add_argument("-timeout", type=int, default=600,
                    help='Dragos Platform API connection timeout (default: %(default)s)')

args = parser.parse_args()

main()