import http.server
import socketserver
import os
import tftpy
import socket
import json
from datetime import datetime
from urllib import parse
from io import BytesIO
import argparse

parser = argparse.ArgumentParser(description='Dragos Platform WinLogBeat Slingshot - V1.0')
parser.add_argument("-config", type=str, default="winlogslingshotconfig.json",
                    help='Specify the path to the test case config file (default: %(default)s)')
args = parser.parse_args()

# load config file
with open(args.config) as f:
    config_dict = json.load(f)

#PORT = 9200
#TFTP_BLKSIZE = 512
#TFTP_IP = "192.168.32.131"
#TFTP_IP = "224.1.1.69"
#TFTP_IP = "239.255.255.69"
#TFTP_PORT = 69

Handler = http.server.SimpleHTTPRequestHandler

elastic_server_json = """
{
  "name" : "DESKTOP-GOU2C7L",
  "cluster_name" : "elasticsearch",
  "cluster_uuid" : "30ji9cQYRNedq1fYnuRujw",
  "version" : {
    "number" : "7.6.2",
    "build_flavor" : "unknown",
    "build_type" : "unknown",
    "build_hash" : "ef48eb35cf30adf4db14086e8aabd07ef6fb113f",
    "build_date" : "2020-03-26T06:34:37.794943Z",
    "build_snapshot" : false,
    "lucene_version" : "8.4.0",
    "minimum_wire_compatibility_version" : "6.8.0",
    "minimum_index_compatibility_version" : "6.0.0-beta1"
  },
  "tagline" : "You Know, for Search"
}
"""

elastic_server_license_json = """
{
  "license" : {
    "status" : "active",
    "uid" : "b705d5d2-89a6-4924-ac84-fc5d211772fe",
    "type" : "basic",
    "issue_date" : "2019-09-04T18:25:14.581Z",
    "issue_date_in_millis" : 1567621514581,
    "max_nodes" : 1000,
    "issued_to" : "elasticsearch",
    "issuer" : "elasticsearch",
    "start_date_in_millis" : -1
  }
}
"""

elastic_server_xpack_json = """
{
   "build":{
      "hash":"ef48eb35cf30adf4db14086e8aabd07ef6fb113f",
      "date":"2020-03-26T06:34:37.794943Z"
   },
   "license":{
      "uid":"b705d5d2-89a6-4924-ac84-fc5d211772fe",
      "type":"basic",
      "mode":"basic",
      "status":"active"
   },
   "features":{
      "analytics":{
         "available":true,
         "enabled":true
      },
      "ccr":{
         "available":false,
         "enabled":true
      },
      "enrich":{
         "available":true,
         "enabled":true
      },
      "flattened":{
         "available":true,
         "enabled":true
      },
      "frozen_indices":{
         "available":true,
         "enabled":true
      },
      "graph":{
         "available":false,
         "enabled":true
      },
      "ilm":{
         "available":true,
         "enabled":true
      },
      "logstash":{
         "available":false,
         "enabled":true
      },
      "ml":{
         "available":false,
         "enabled":true,
         "native_code_info":{
            "version":"7.6.2",
            "build_hash":"e06ef9d86d5332"
         }
      },
      "monitoring":{
         "available":true,
         "enabled":true
      },
      "rollup":{
         "available":true,
         "enabled":true
      },
      "security":{
         "available":true,
         "enabled":false
      },
      "slm":{
         "available":true,
         "enabled":true
      },
      "spatial":{
         "available":true,
         "enabled":true
      },
      "sql":{
         "available":true,
         "enabled":true
      },
      "transform":{
         "available":true,
         "enabled":true
      },
      "vectors":{
         "available":true,
         "enabled":true
      },
      "voting_only":{
         "available":true,
         "enabled":true
      },
      "watcher":{
         "available":false,
         "enabled":true
      }
   },
   "tagline":"You know, for X"
}
"""

elastic_server_cat_templates_winlogbeat = """winlogbeat-7.6.2 [winlogbeat-7.6.2-*] 1 """

elastic_server_ilm_policy_winlogbeat = """{"winlogbeat":{"version":1,"modified_date":"2020-05-12T14:30:11Z","policy":{"phases":{"hot":{"min_age":"0ms","actions":{"rollover":{"max_size":"50gb","max_age":"30d"}}}}}}}"""

elastic_server_alias_winlogbeat = """{"winlogbeat-7.6.2-2020.05.12-000001":{"aliases":{"winlogbeat-7.6.2":{"is_write_index":true}}}}"""

elastic_server_bulk_write_response ="""
{
   "took":16,
   "errors":false,
   "items":[
      {
         "create":{
            "_index":"winlogbeat-7.6.2-2020.05.12-000001",
            "_type":"_doc",
            "_id":"pDozCnIBZSVrcS8d9MAC",
            "_version":1,
            "result":"created",
            "_shards":{
               "total":2,
               "successful":1,
               "failed":0
            },
            "_seq_no":69493,
            "_primary_term":5,
            "status":201
         }
      },
      {
         "create":{
            "_index":"winlogbeat-7.6.2-2020.05.12-000001",
            "_type":"_doc",
            "_id":"pTozCnIBZSVrcS8d9MAC",
            "_version":1,
            "result":"created",
            "_shards":{
               "total":2,
               "successful":1,
               "failed":0
            },
            "_seq_no":69494,
            "_primary_term":5,
            "status":201
         }
      }
   ]
}
"""

class Handler(http.server.SimpleHTTPRequestHandler):

    def do_GET(self):
        # Process Server Request
        parsed_path = parse.urlparse(self.path)
        message_parts = [
            'GET CLIENT VALUES:',
            'client_address={} ({})'.format(
                self.client_address,
                self.address_string()),
            'command={}'.format(self.command),
            'path={}'.format(self.path),
            'real path={}'.format(parsed_path.path),
            'query={}'.format(parsed_path.query),
            'request_version={}'.format(self.request_version),
            '',
            'SERVER VALUES:',
            'server_version={}'.format(self.server_version),
            'sys_version={}'.format(self.sys_version),
            'protocol_version={}'.format(self.protocol_version),
            '',
            'HEADERS RECEIVED:',
        ]
        for name, value in sorted(self.headers.items()):
            message_parts.append(
                '{}={}'.format(name, value.rstrip())
            )
        print("GET Server Request Information: ")
        print(message_parts)

        # Construct a server response.
        self.send_response(200)
        self.send_header('Content-type',
                         'application/json; charset=UTF-8')
        self.end_headers()
        if "_license" in self.path:
            self.wfile.write(elastic_server_license_json.encode('utf-8'))
        elif "_xpack" in self.path:
            self.wfile.write(elastic_server_xpack_json.encode('utf-8'))
        elif "_cat/templates/winlogbeat" in self.path:
            self.wfile.write(elastic_server_cat_templates_winlogbeat.encode('utf-8'))
        elif "_ilm/policy/winlogbeat" in self.path:
            self.wfile.write(elastic_server_ilm_policy_winlogbeat.encode('utf-8'))
        elif "_alias/winlogbeat" in self.path:
            self.wfile.write(elastic_server_alias_winlogbeat.encode('utf-8'))
        elif len(self.path) > 1:
            print("WARNING! UNEXPECTED ELASTIC PATH: " + self.path)
            self.wfile.write(elastic_server_json.encode('utf-8'))
        else:
            self.wfile.write(elastic_server_json.encode('utf-8'))
        return

    def do_POST(self):
        # Process Server Request
        parsed_path = parse.urlparse(self.path)
        message_parts = [
            'POST CLIENT VALUES:',
            'client_address={} ({})'.format(
                self.client_address,
                self.address_string()),
            'command={}'.format(self.command),
            'path={}'.format(self.path),
            'real path={}'.format(parsed_path.path),
            'query={}'.format(parsed_path.query),
            'request_version={}'.format(self.request_version),
            '',
            'SERVER VALUES:',
            'server_version={}'.format(self.server_version),
            'sys_version={}'.format(self.sys_version),
            'protocol_version={}'.format(self.protocol_version),
            '',
            'HEADERS RECEIVED:',
        ]
        for name, value in sorted(self.headers.items()):
            message_parts.append(
                '{}={}'.format(name, value.rstrip())
            )
        print("POST Server Request Information: ")
        print(message_parts)

        # Process POST Data into a buffer object
        print("POST DATA: ")
        content_length = int(self.headers['Content-Length'])
        post_data = self.rfile.read(content_length)
        print(post_data.decode('utf-8'))
        post_data_buffer = BytesIO(post_data)

        # Construct a server response.
        self.send_response(200)
        self.send_header('Content-type',
                         'application/json; charset=UTF-8')
        self.end_headers()
        self.wfile.write(elastic_server_bulk_write_response.encode('utf-8'))

        # Transmit the WinLogs
        now = datetime.now()
        filename_date_time = now.strftime("winlogbeats-%m-%d-%Y-%H-%M-%S-%f.winlog")

        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 2)
        pkt = tftpy.TftpStates.TftpPacketWRQ()
        pkt.filename = filename_date_time
        pkt.mode = "octet"
        sock.sendto(pkt.encode().buffer, (config_dict["TFTP_IP"], config_dict["TFTP_PORT"]))
        print("Init TFTP File Transfer: " +filename_date_time )
        finished = False
        blocknumber = 0
        while finished is False:
            blocknumber = blocknumber + 1
            print("Sending Block Number: " + str(blocknumber))
            dat = None
            buffer = post_data_buffer.read(config_dict["TFTP_BLKSIZE"])
            print("Block Buffer Size: " + str(len(buffer)))
            if len(buffer) < config_dict["TFTP_BLKSIZE"]:
                finished = True  # Reached EOF on file
                print("END OF FILE REACHED")
            dat = tftpy.TftpStates.TftpPacketDAT()
            dat.data = buffer
            dat.blocknumber = blocknumber
            sock.sendto(dat.encode().buffer, (config_dict["TFTP_IP"], config_dict["TFTP_PORT"]))

            print("SENT Block Number: " + str(blocknumber))

        sock = None
        print("Finished TFTP File Transfer")
        return

    def do_PUT(self):
        path = self.translate_path(self.path)

        # Process Server Request
        parsed_path = parse.urlparse(self.path)
        message_parts = [
            'PUT CLIENT VALUES:',
            'client_address={} ({})'.format(
                self.client_address,
                self.address_string()),
            'command={}'.format(self.command),
            'path={}'.format(self.path),
            'real path={}'.format(parsed_path.path),
            'query={}'.format(parsed_path.query),
            'request_version={}'.format(self.request_version),
            '',
            'SERVER VALUES:',
            'server_version={}'.format(self.server_version),
            'sys_version={}'.format(self.sys_version),
            'protocol_version={}'.format(self.protocol_version),
            '',
            'HEADERS RECEIVED:',
        ]
        for name, value in sorted(self.headers.items()):
            message_parts.append(
                '{}={}'.format(name, value.rstrip())
            )
        print("PUT Server Request Information: ")
        print(message_parts)

        # Construct a server response.
        if path.endswith('/'):
            self.send_response(405, "Method Not Allowed")
            self.wfile.write("PUT not allowed on a directory\n".encode())
            return
        else:
            try:
                os.makedirs(os.path.dirname(path))
            except FileExistsError: pass
            length = int(self.headers['Content-Length'])
            with open(path, 'wb') as f:
                f.write(self.rfile.read(length))
            self.send_response(201, "Created")


with socketserver.TCPServer(("", config_dict["PORT"]), Handler) as httpd:
    print("Emulating Elastic Search Server on Port: ", config_dict["PORT"])
    httpd.serve_forever()

