import requests

post_data_json = """
{"create":{"_index":"winlogbeat-7.6.2"}}
{"@timestamp":"2020-05-12T19:09:18.278Z","ecs":{"version":"1.4.0"},"agent":{"type":"winlogbeat","ephemeral_id":"7b5b45e0-5dfd-4ade-8df9-ea9186dec2f0","hostname":"DESKTOP-GOU2C7L","id":"dfbf31ed-5ca6-48e4-9add-321f6b35dc7a","version":"7.6.2"},"winlog":{"computer_name":"DESKTOP-GOU2C7L","channel":"Application","opcode":"Info","keywords":["Classic"],"event_data":{"param5":"-1023 (0xfffffc01)","param1":"svchost","param2":"31392,R,98","param3":"TILEREPOSITORYS-1-5-18: ","param4":"C:\\WINDOWS\\system32\\config\\systemprofile\\AppData\\Local\\TileDataLayer\\Database\\EDB.log"},"event_id":455,"record_id":66835,"api":"wineventlog","provider_name":"ESENT","task":"Logging/Recovery"},"event":{"kind":"event","code":455,"provider":"ESENT","action":"Logging/Recovery","created":"2020-05-12T19:09:18.794Z"},"host":{"name":"DESKTOP-GOU2C7L","architecture":"x86_64","os":{"version":"10.0","family":"windows","name":"Windows 10 Home","kernel":"10.0.18362.778 (WinBuild.160101.0800)","build":"18362.778","platform":"windows"},"id":"d66e2b13-d945-4996-9be1-a5937e23b5e9","hostname":"DESKTOP-GOU2C7L"},"log":{"level":"error"},"message":"svchost (31392,R,98) TILEREPOSITORYS-1-5-18: Error -1023 (0xfffffc01) occurred while opening logfile C:\\WINDOWS\\system32\\config\\systemprofile\\AppData\\Local\\TileDataLayer\\Database\\EDB.log."}
"""
# 'Content-Length=1365'

url = "http://localhost:9200/_bulk"
headers = {
    'Content-type': 'application/json; charset=UTF-8',
    'Accept': 'application/json',
    'Content-Length': str(len(post_data_json))
}
r = requests.post(url, data=post_data_json.encode("utf-8"), headers=headers)
print("SERVER RESPONSE STATUS CODE:")
print(r.status_code)
print("SERVER POST RESPONSE:")
print(r.content)

