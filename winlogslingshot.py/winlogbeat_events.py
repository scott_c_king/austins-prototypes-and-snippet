from dragos_data_broker import DragosDataBroker
from elasticsearch_dsl import Search
from elasticsearch_dsl.connections import connections
import asset_inventory_service as ais
import datetime
import dragos_notifications as dn
import pandas as pd
import re
import requests
import yaml
import os
import argparse

parser = argparse.ArgumentParser(description='WinLogBeats Events')
parser.add_argument('--verbose', dest='verbose', help='show verbose output', action='store_true')
parser.add_argument('--id', dest='id', help='ID for any notifications generated during this run', required=False)
parser.add_argument('--ingest_start', dest='ingest_start', help='Timestamp of the earliest event timestamp to consider in this run', required=False)
parser.add_argument('--ingest_finish', dest='ingest_finish', help='Timestamp of the last event timestamp to consider in this run', required=False)
parser.add_argument('--elastic_url', dest='elastic_url', help='This is the URL to the ElasticSearch Database (default: %(default))', default="http://elasticsearch.:9200", required=False)
parser.add_argument('--log_path', dest='log_path', help='Path to the folder where winlogbeat files are being stored (default: %(default))', required=False)
parser.add_argument('--delete_files', dest='delete_files', action='store_true', help='Delete the winlogbeat files after processing them.')
parser.add_argument('--config_file', dest='config_file', help='This is the path to the configuration file (default: %(default))', required=False)
args = parser.parse_args()

g_verbose = args.verbose
metrics = {"engine": "Python"}
metrics['start_time'] = datetime.datetime.now()
metrics['analytic_id'] = args.id

def load_config(input_file= os.path.dirname(os.path.abspath(__file__)) + '/winlogbeat_events.yml'):
    config = {}
    with open(input_file) as f:
        try:
            config = yaml.safe_load(f)
        except yaml.YAMLError as exc:
            print(exc)
    return config

def extract_fields(data, fields):
    output = {}
    for field, friendly_name in fields.items():
        event_id_match = re.search(field + r"=\"([^\"]+)\"", data)
        if event_id_match is not None:
            output[friendly_name] = event_id_match.groups()[0]
    return output

config = load_config()
ddb = DragosDataBroker()
results = {}
client = connections.create_connection(hosts=[args.elastic_url])
timerange = {'gte': args.ingest_start if args.ingest_start else 'now-10m', 'lt': args.ingest_finish if args.ingest_finish else 'now'}

post_data_json =

url = "http://localhost:9200/_bulk"
headers = {
    'Content-type': 'application/json; charset=UTF-8',
    'Accept': 'application/json',
    'Content-Length': str(len(post_data_json))
}
r = requests.post(url, data=post_data_json.encode("utf-8"), headers=headers)
print("SERVER RESPONSE STATUS CODE:")
print(r.status_code)
print("SERVER POST RESPONSE:")
print(r.content)


s = Search(using=client, index='pipeline')
s = s.filter('range', ingest_timestamp=timerange)
s = s.filter('term', type='Syslog')
s = s.filter('wildcard', body='*NXLOG*')
results = []
for i in s.scan():
    match = re.match(r"(.*) (\d+) [^ ]* \[(.*)\] (.*)", i.body)
    if match is None:
        continue
    event_source, pid, data, description = match.groups()
    fields = extract_fields(data, config['fields'])
    description = re.sub("(  +)", r'\n\1', description)

    result = {
        "timestamp": i.timestamp,
        "asset_id": i.asset_id,
        "sender": i.sender,
        "hostname": i.hostname,
        "severity": i.severity,
        "facility": i.facility,
        "event_source": event_source,
        "event": config['friendly_names'].get(event_source, {}).get(fields.get('EventID', ""), ""),
        "source": i.meta.id,
        "pid": pid,
        "description": re.sub("'", '"', description)

    }
    result.update(fields)
    results.append(result)
    for tag in config.get("tags", []):
        if result['event'] in tag.get("events", []):
            t = {
                'tag': tag['tag'],
                'type': "Asset",
                'category': "Event",
                'timestamp': i.timestamp,
                'asset_id': i.asset_id,
                'log_source': "Windows Event",
                'source_id': i.meta.id,
            }
            for key, value in tag.get('context', {}).items():
                t[key] = result.get(value, "")
            df = pd.DataFrame([t])
            ddb.upsert_pd("tags", df, ['tag', 'type', 'category', 'asset_id', 'timestamp'])

df = pd.DataFrame(results)
if len(df) > 0:
    inserts = ddb.upsert_pd("qfd_all_windows_events", df, ["source"])

    print("Found", len(results), "Host Logs")
    print(len(inserts), "were new")
else:
    print("No Results")
metrics['end_time'] = datetime.datetime.now()
metrics['run_time'] = (metrics['end_time'] - metrics['start_time']).seconds
metrics['start_time'] = metrics['start_time'].strftime('%Y-%m-%dT%H:%M:%S.000Z')
metrics['end_time'] = metrics['end_time'].strftime('%Y-%m-%dT%H:%M:%S.000Z')
metrics_df = pd.DataFrame([metrics])
ddb.insert_pd('analytic_metrics', metrics_df, create_mapping=False)