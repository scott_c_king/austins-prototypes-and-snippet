#!/usr/bin/env python3

'''
Dragos Platform Fully Integrated Sensor Test Suite
'''


import argparse
import ctypes
import datetime
import json
import os
import subprocess
import sys
import time
import paramiko
import requests
from fabric import Connection
from invoke import Responder
from invoke.exceptions import UnexpectedExit

__author__ = "Scott C. King, Austin Scott"
__version__ = "0.2.0"
__license__ = "MIT"

# Platform Sensor Test Suite
# ===============================
# This program is designed to perform "fully integrated" tests against the Dragos Platform Sensor.
# The project goal is to create small, atomic, and autonomous Platform Sensor test cases that can identify
# sensor issues including (but not limited to):
# * Throughput
# * Packet Loss
# * Data Integrity
# * Data context impact on packet loss
# Ultimately, this tool is to be integrated into a test framework like Jenkins, Xray and align with the Dragos Jira QA
# conventions.

# Dependencies:
# pip install requests
# pip install fabric

# TODO - On the sensor we must install the sysstat library in order to support the PCAP ingest process monitoring
# yum install sysstat
# Reference:
# https://stackoverflow.com/questions/43531543/get-memory-usage-per-process-with-sar-sysstat/59182595#59182595

# Adding support for color in a windows console window is hard
FORCE_LINUX_COLOR_CODES = True  # Force system to use Linux color codes so that they appear in PyCharm for debugging
STD_OUTPUT_HANDLE = -11
# Root User Shell Prompt:
ROOT_SHELL_PROMPT = r'\[root@{} ~\]# '
RESET_COMMAND = ('(/var/opt/releases/*_reset.sh ; ec=$? ; if [ ${ec} -eq 0 ] ; '
                 'then echo "-- reset exited successfully --" ; fi) '
                 '| tee /tmp/reset.log && exit\n')
NOW = datetime.datetime.utcnow()
NOW_MINUS_90_DAYS = NOW - datetime.timedelta(days=90)
CURRENT_PCAP_FILENAME = "test"

# Sets playback speed of pcaps:
# '' is pcap default, '10' is 10 Mbps, '100' is 100Mbps, '1000' is 1Gbps, etc.
PCAP_MBPS = ''


def color_blue():
    ''' Text color blue '''
    if os.name == 'nt' and not FORCE_LINUX_COLOR_CODES:
        ctypes.windll.kernel32.SetConsoleTextAttribute(STD_OUT_HANDLE, 1)
        return ''
    return '\033[0;34m'


def color_green():
    ''' Text color green '''
    if os.name == 'nt' and not FORCE_LINUX_COLOR_CODES:
        ctypes.windll.kernel32.SetConsoleTextAttribute(STD_OUT_HANDLE, 2)
        return ''
    return '\033[0;32m'


def color_cyan():
    ''' Text color cyan '''
    if os.name == 'nt' and not FORCE_LINUX_COLOR_CODES:
        ctypes.windll.kernel32.SetConsoleTextAttribute(STD_OUT_HANDLE, 3)
        return ''
    return '\033[0;36m'


def color_red():
    ''' Text color red '''
    if os.name == 'nt' and not FORCE_LINUX_COLOR_CODES:
        ctypes.windll.kernel32.SetConsoleTextAttribute(STD_OUT_HANDLE, 4)
        return ''
    return '\033[0;31m'


def color_reset():
    ''' Text color reset '''
    if os.name == 'nt' and not FORCE_LINUX_COLOR_CODES:
        ctypes.windll.kernel32.SetConsoleTextAttribute(STD_OUT_HANDLE, 7)
        return ''
    return '\033[0;39m'


def get_recursively(search_dict, field):
    """
    Takes a dict or list with nested lists and dicts,
    and searches all dicts for a key of the field
    provided.
    """
    fields_found = []

    if isinstance(search_dict, 'list'):
        for dict_item in search_dict:
            results = get_recursively(dict_item, field)
            for result in results:
                fields_found.append(result)
    elif isinstance(search_dict, 'dict'):
        for key, value in search_dict.items():
            if key == field:
                fields_found.append(value)
            elif isinstance(value, dict):
                results = get_recursively(value, field)
                for result in results:
                    fields_found.append(result)
            elif isinstance(value, list):
                for item in value:
                    if isinstance(item, dict):
                        more_results = get_recursively(item, field)
                        for another_result in more_results:
                            fields_found.append(another_result)
    else:
        print("ERROR: UNEXPECTED OBJECT TYPE - get_recursively()")

    return fields_found


def get_recursively_dict(search_dict, expected_dict):
    """
    Takes a dict or list with nested lists and dicts,
    and searches all dicts for an expected dict object.
    """
    dicts_found = []
    results_found = 0
    total_keys = 0

    if isinstance(search_dict, 'list'):
        for dict_item in search_dict:
            results = get_recursively_dict(dict_item, expected_dict)
            for result in results:
                dicts_found.append(result)
    elif isinstance(search_dict, 'dict'):
        results_found = 0
        total_keys = 0
        # If a variabe is unused, use '_' or prepent an underscore
        for _search_dict_key, search_dict_value in search_dict.items():  # look for Lists of Dicts within the Dict
            if isinstance(search_dict_value, 'list'):
                results = get_recursively_dict(search_dict_value, expected_dict)
                for result in results:
                    dicts_found.append(result)
        for key, value in expected_dict.items():
            total_keys += 1
            if key in search_dict:
                if search_dict[key] == value:
                    results_found += 1
        if total_keys == results_found:
            dicts_found.append(expected_dict)
    else:
        print("ERROR: UNEXPECTED OBJECT TYPE - get_recursively_dict()")

    return dicts_found


def delim(character='-', length=80, title=''):
    ''' Add a delimiter of chars, length, with optional title. '''
    head = '{:%s^%ss}' % (character, length)
    header = head.format(title)
    print(header)


def import_json(json_file):
    ''' Given a relative/absolute path to JSON file, return Python dictionary. '''
    with open(json_file, 'r') as jfile:
        data = json.loads(jfile.read())
    return data


def get_hostname(connection):
    ''' Fetch the hostname of the remote system '''
    result = connection.run('hostname', hide=True)
    return result.stdout.split('.')[0]


def ping_test(endpoints):
    ''' Pings a list of target FQDN or IPs '''
    try:
        for endpoint in endpoints:
            if os.name == 'nt':
                subprocess.run(f'ping -n 1 {endpoint}', shell=True, check=True)
            else:
                subprocess.run(f'ping -c 1 {endpoint}', shell=True, check=True)
    except (FileNotFoundError, subprocess.CalledProcessError):
        print(f'ERROR: Could not ping {endpoint}!.\nAre you connected to the VPN?\n'
              'Is local_env.json populated and valid?')
        # sys.exit(1)
    print('INFO: Ping tests passed...\n')
    return True


def reset_system(connection, server):
    ''' Reset Sitestore(s) and Midpoint(s) '''

    # Fetch the hostname for the root shell prompt
    hostname = get_hostname(connection)
    # Create a readable logfile in user's home directory

    connection.run('touch /tmp/reset.log && chmod 755 /tmp/reset.log')

    # Reset the server using the /var/opt/releases reset script
    delim(title=f' Resetting {server} ')
    try:
        reset_cmd = RESET_COMMAND
        responder = Responder(pattern=ROOT_SHELL_PROMPT.format(hostname), response=reset_cmd)
        begin = time.time()
        connection.run('sudo sudosh', pty=True, watchers=[responder])

    # When exiting sudosh, it will cause an exception - handle it gracefully
    except UnexpectedExit:
        # Ignore the failing exit code from sudosh and check the log file
        cmd = 'cat /tmp/reset.log'
        log_output = connection.run(cmd, hide=True)
        if '-- reset exited successfully --' in log_output.stdout:
            print(f'INFO: {server} reset was successful (Time elapsed: '
                  f'{round((time.time() - begin), 2)} seconds).')
    else:
        print(f'ERROR: {server} reset has failed!')
        sys.exit(1)


def ssh_connection(system, username, password, private_key):
    ''' Returns a ssh connection '''
    print(f'INFO [system]: {system}')
    print(f'INFO [private_key]: {private_key}')
    if len(private_key) > 0 and os.path.exists(private_key):
        key = paramiko.RSAKey.from_private_key_file(private_key)
        return Connection(system, username, connect_kwargs={"pkey": key}, connect_timeout=15)
    return Connection(system, username, connect_kwargs={"password": password}, connect_timeout=15)


def copy_pcaps(connection, pcaps, destination):
    ''' Copy a list of pcaps (abs/relative local paths) to target system (/tmp/pcaps) '''
    delim(title=' Copying PCAPs ')
    for pcap in pcaps:
        filename = pcap.split(os.sep)[-1]
        print(f'INFO: Copying {filename}')
        connection.put(pcap, destination)


def pcap_playback(connection, pcap_path, iface, mbps, username):
    ''' Playback pcaps on target system '''

    # Fetch the hostname for the root shell prompt
    hostname = get_hostname(connection)
    # Create a readable logfile in user's home directory
    filename = pcap_path.split(os.sep)[-1]
    cmd = (f'touch /home/{username}/{filename}_pcap_playback.log && '
           f'chmod 755 /home/{username}/{filename}_pcap_playback.log')
    connection.run(cmd)

    # Playback the desired pcap
    delim(title=f' pcap Playback: {filename} ')
    cmd = ('(tcpreplay-edit -C --mtu-trunc -i %s --stats=30 -M %s -T nano %s ; ec=$? ; if [ ${ec} -eq 0 ] ; '
           'then echo "--- pcap playback successful ---" ; fi) '
           '| tee /home/%s/%s_pcap_playback.log && exit\n') % (iface, mbps, pcap_path, username, filename)

    print('INFO: Command ', cmd)
    try:
        responder = Responder(pattern=ROOT_SHELL_PROMPT.format(hostname), response=cmd)
        begin = time.time()
        connection.run('sudo sudosh', pty=True, watchers=[responder])

    # When exiting sudosh, it will produce a failing exit code - handle it gracefully
    except UnexpectedExit:
        # Ignore the failing exit code from sudosh and check the log file
        cmd = f'cat /home/{username}/{filename}_pcap_playback.log'
        log_output = connection.run(cmd, hide=True)
        if '--- pcap playback successful ---' in log_output.stdout:
            print(f'INFO: Playback was successful (Time elapsed: '
                  f'{round((time.time() - begin), 2)} seconds).')
    else:
        print(f'ERROR: {filename} playback has failed!')
        sys.exit(1)


def single_operation(url, request, headers, timeout, api_dir, test_debug_filename):
    ''' Single operation '''
    print('INFO: Entered Single Operation')
    print(f'INFO [url]: {url}')
    print(f'INFO [request]: {request}')
    print(f'INFO [headers]: {headers}')

    payload = {"requests": {"r1": request}}
    payload_json = json.dumps(payload)

    return_json = requests.post(url, headers=headers, timeout=timeout, data=payload_json)
    print(f'INFO [return_json]: {return_json}')
    return_json_json = return_json.json()

    # Log the server response
    with open(os.path.join(api_dir, test_debug_filename), 'w') as json_file:
        if ARGS.pretty:
            json.dump(return_json_json, json_file, sort_keys=True, indent=4)
        else:
            json.dump(return_json_json, json_file)
    print(f'INFO [return_json_json]: {return_json_json}')
    return return_json_json["responses"]["r1"]


def get_asset_count(url, headers, timeout, api_dir, test_debug_filename):
    ''' Get asset count '''
    print('INFO: Entered Get Asset Count')
    print(f'INFO [url]: {url}')
    print(f'INFO [headers]: {headers}')
    print(f'INFO [timeout]: {timeout}')
    asset_count = single_operation(url, {"requestType": "SEARCH_ASSETS", "pageSize": 999999},
                                   headers, timeout, api_dir, test_debug_filename)
    print(f'INFO [asset_count]: {asset_count}')
    if ARGS.debug:
        print("Total Asset Count: {}".format(asset_count))
    print('INFO: Exiting Get Asset Count')
    return asset_count


def get_asset_count_by_attribute(url, attribute, headers, timeout, api_dir, test_debug_filename):
    ''' Get asset count by attribute '''
    attribute_value = [[attribute]]
    assets_by_attribute = single_operation(url, {"requestType": "GROUP_ASSETS_BY_ATTRIBUTES",
                                                 "groupByAttributes": attribute_value,
                                                 "from": NOW_MINUS_90_DAYS.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z',
                                                 "to": NOW.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z',
                                                 "onlyInternal": False},
                                           headers, timeout, api_dir, test_debug_filename)["summary"]

    if ARGS.debug:
        print("Assets by {}".format(attribute))

    if ARGS.debug:
        print("  --------------------------------------------------")
    if ARGS.debug:
        print("  {:<40s}{:>10s}".format(attribute, "count"))
    if ARGS.debug:
        print("  --------------------------------------------------")
    for group in assets_by_attribute:
        if attribute in group:
            if group[attribute]:
                if ARGS.debug:
                    print("  {:<40s}{:>10d}".format(group[attribute], group["count"]))
            else:
                if ARGS.debug:
                    print("  {:<40s}{:>10d}".format("EMPTY STRING", group["count"]))
        else:
            if ARGS.debug:
                print("  {:<40s}{:>10d}".format("UNKNOWN", group["count"]))
    if ARGS.debug:
        print("  --------------------------------------------------")

    return assets_by_attribute


def get_communication_summary(url, headers, timeout, api_dir, test_debug_filename):
    ''' Get communication summary '''
    commmunication_summary = single_operation(url, {
        "requestType": "GET_COMMUNICATIONS_SUMMARY",
        "from": NOW_MINUS_90_DAYS.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z',
        "to": NOW.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z'
    }, headers, timeout, api_dir, test_debug_filename)["total"]

    if ARGS.debug:
        print("Total Bytes: {}".format(commmunication_summary["total"]["bytes"]))
    if ARGS.debug:
        print("Total Packets: {}".format(commmunication_summary["total"]["packets"]))
    if ARGS.debug:
        print("Protocol Breakdown")
    if ARGS.debug:
        print("  --------------------------------------------------")
    if ARGS.debug:
        print("  {:<30s}{:>10s}{:>10s}".format("protocol", "bytes", "packets"))
    if ARGS.debug:
        print("  --------------------------------------------------")
    for protocol in commmunication_summary["protocols"]:
        if ARGS.debug:
            print("  {:<30s}{:>10d}{:>10d}".format(protocol["id"],
                                                   protocol["metrics"]["bytes"],
                                                   protocol["metrics"]["packets"]))
    if ARGS.debug:
        print("  --------------------------------------------------")
    return commmunication_summary


def get_notification_count(url, headers, timeout, api_dir, test_debug_filename):
    ''' Get notification count '''
    query_params = {
        "pageNumber": 1,
        "pageSize": 999999,
        "sortField": "id",
        "sortDescending": True,
        "filter": "occurredAt=lt='{}';occurredAt=gt='{}';type!='System'".format(
            NOW.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z',
            NOW_MINUS_90_DAYS.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z'),
        "resolveChildrenDepth": 1,
        "ignoreVisibilityPolicy": True,
        "limitTotalCount": 999999
    }
    notification_json = requests.get(url, headers=headers, timeout=timeout, params=query_params).json()

    # Log the server response
    with open(os.path.join(api_dir, test_debug_filename), 'w') as json_file:
        if ARGS.pretty:
            json.dump(notification_json, json_file, sort_keys=True, indent=4)
        else:
            json.dump(notification_json, json_file)
    notification_count = notification_json["totalCount"]
    if ARGS.debug:
        print("Total Notification Count: {}".format(notification_count))
    return notification_json


def build_files_list(a_directory):
    ''' Build Python list of files in a given directory '''
    print(f'INFO [a_directory]: {a_directory}')
    file_list = []
    files = os.listdir(a_directory)
    print(f'INFO [files in a_directory]: {files}')
    for file in files:
        file_list.append(os.path.join(a_directory, file))
    print(f'INFO [file_list]: {file_list}')
    return file_list


# Generate test cases based on a folder of PCAPS
def generate_test_cases(pcaps, authorization, sitestores, midpoints,
                        username, password, private_key, analytics_delay,
                        ais_url, notification_url, api_dir, timeout):
    ''' Generate the test cases '''

    print('INFO: Entering generate test cases.')
    print(f'INFO [pcaps]: {pcaps}')
    print(f'INFO [authorization]: {authorization}')
    print(f'INFO [sitestores]: {sitestores}')
    print(f'INFO [midpoints]: {midpoints}')

    headers = {"Content-Type": "application/json", "Authorization": authorization}

    # iterate over PCAPS in folder
    for pcap in pcaps:
        print(color_reset())
        delim()
        print(f'INFO [Building test case for pcap]: {pcap}')

        # Reset Sitestore(s) and Midpoint(s)
        servers = sitestores + midpoints
        for server in servers:
            with ssh_connection(server, username, password, private_key) as cxn:
                reset_system(cxn, server)

        # Copy pcaps to target systems
        for midpoint in midpoints:
            with ssh_connection(midpoint, username, password, private_key) as cxn:
                dest_path = '/home/{}/pcaps'.format(username)
                cxn.run(f'mkdir -p {dest_path} && chmod -R 755 {dest_path}')
                copy_pcaps(cxn, [pcap], dest_path)

        # Playback pcap(s)
        for midpoint in midpoints:
            with ssh_connection(midpoint, username, password, private_key) as cxn:
                print(f'INFO [Midpoint]: {midpoint}')
                interface = 'bond0'        # Hardware, Dragos HQ Systems
                if 'cloud' in midpoint:
                    interface = 'replay'   # AWS
                print(f'INFO [Replay interface]: {interface}')
                pcap_abs_path = '/home/{}/pcaps/{}'.format(username, pcap.split(os.sep)[-1])
                pcap_playback(cxn, pcap_abs_path, interface, PCAP_MBPS, username)

        # Wait for detections to fire
        print(color_green() + 'Finished Loading PCAPS!')
        print(color_blue() + f'Waiting for {str(analytics_delay)} seconds before running API test cases '
              '(give the platform time for analytics to fire...)')
        # time.sleep(int(analytics_delay))

        test_name = pcap.split('/')[-1].split('.')[0]
        pcap_name = pcap.split('/')[-1].lower()
        test_case_filename = test_name.lower() + '.json'
        print(f'INFO [test_name]: {test_name}')
        print(f'INFO [pcap_name]: {pcap_name}')
        print(f'INFO [test_case_filename]: {test_case_filename}')

        # Create test case structure
        test_case_dict = {'name': test_name,
                          'key': 'PLATDEV-5000',
                          'description': f'Generated test case based on {pcap_name}',
                          'pcap': {pcap_name: PCAP_MBPS},
                          'delay_time_seconds': int(analytics_delay),
                          'tests': []}

        # Append Asset Count Test Case
        test_debug_filename = 'get_asset_count_' + test_name.lower() + '.json'
        print(f'INFO [test_debug_filename]: {test_debug_filename}')
        test_result = get_asset_count(ais_url, headers, timeout, api_dir, test_debug_filename)
        test_case = {"name": "Test to see if we are seeing the expected Asset Count",
                     "testCase": "Asset Count",
                     "testKey": "PLATDEV-5830",
                     "expected_results": [{"name": "Asset Count Number",
                                           "key": "totalCount",
                                           "value": test_result['totalCount']}]}
        test_case_dict["tests"].append(test_case)

        # Append Notification Test Case
        test_debug_filename = 'get_notification_count_' + test_name.lower() + '.json'
        print(f'INFO [test_debug_filename]: {test_debug_filename}')
        test_result = get_notification_count(notification_url, headers, timeout, api_dir, test_debug_filename)
        test_case = {"name": "Test to see if we are seeing the expected number of Notifications",
                     "testCase": "Notification Count",
                     "testKey": "PLATDEV-5831",
                     "expected_results": [{"name": "Notification Count Number",
                                           "key": "totalCount",
                                           "value": test_result['totalCount']}]}
        test_case_dict["tests"].append(test_case)

        # Append total bytes and packets to test case
        test_debug_filename = 'get_communication_summary_' + test_name.lower() + '.json'
        print(f'INFO [test_debug_filename]: {test_debug_filename}')
        test_result = get_communication_summary(ais_url, headers, timeout, api_dir, test_debug_filename)
        test_case = {"name": "Verify the total number of Bytes and Packets processed by the sensor",
                     "testCase": "Communication Summary",
                     "testKey": "PLATDEV-5832",
                     "expected_dicts": [{"total": {"bytes": test_result['total']['bytes'],
                                                   "packets": test_result['total']['packets']}}]}
        test_case_dict["tests"].append(test_case)

        # Append protocols to test cases
        for protocol in test_result['protocols']:
            protocol_id = protocol['id']
            protocol_testcase = {"name": f"Verify the number of {protocol_id} packets processed by the sensor",
                                 "testCase": "Communication Summary",
                                 "testKey": "PLATDEV-5832",
                                 "expected_dicts": [protocol]}
            test_case_dict["tests"].append(protocol_testcase)

        # Append asset type test cases
        test_debug_filename = 'get_asset_count_by_type_' + test_name.lower() + '.json'
        print(f'INFO [test_debug_filename]: {test_debug_filename}')
        test_result = get_asset_count_by_attribute(ais_url, "type", headers, timeout, api_dir, test_debug_filename)
        for result in test_result:
            if "type" in result:
                type_name = result["type"]
            else:
                type_name = "None"
            asset_type_test_case = {"name": f"Test the number of Assets Type {type_name} is the expected value",
                                    "testCase": "Asset Types",
                                    "testKey": "PLATDEV-5833",
                                    "expected_dicts": [type]}
            test_case_dict["tests"].append(asset_type_test_case)

        # Append asset Vendor test cases
        test_debug_filename = 'get_asset_count_by_vendor_' + test_name.lower() + '.json'
        print(f'INFO [test_debug_filename]: {test_debug_filename}')
        test_result = get_asset_count_by_attribute(ais_url, "Vendor", headers, timeout, api_dir, test_case_filename)
        for vendor in test_result:
            if "Vendor" in vendor:
                vendor_name = vendor["Vendor"]
            else:
                vendor_name = "None"
            vendorstestcase = {"name": f"Test the number of Assets Type {vendor_name} is the expected value",
                               "testCase": "Asset Vendors",
                               "testKey": "PLATDEV-5834",
                               "expected_dicts": [vendor]}
            test_case_dict["tests"].append(vendorstestcase)
        test_file_path = os.path.join(ARGS.test_case_dir, test_case_filename)
        print(color_cyan() + "TEST GENERATION COMPLETE - WRITING TEST FILE TO " + test_file_path)
        with open(test_file_path, 'w') as json_file:
            if ARGS.pretty:
                json.dump(test_case_dict, json_file, sort_keys=True, indent=4)
            else:
                json.dump(test_case_dict, json_file)

        delim(title=' End of Generating Test Cases ')


def main():
    ''' Platform Sensor Test Main '''

    # Generate absolute paths for all relevant argparse options
    config_file = os.path.abspath(ARGS.config)
    test_case_dir = os.path.abspath(ARGS.test_case_dir)
    test_results_dir = os.path.abspath(ARGS.test_results_dir)
    pcap_dir = os.path.abspath(ARGS.pcap_dir)
    api_dir = os.path.abspath(ARGS.api_dir)

    # Sanitize args
    analytics_delay = ARGS.analytics_delay
    timeout = ARGS.timeout

    print(f'INFO [config_path]     : {config_file}')
    print(f'INFO [test_case_dir]   : {test_case_dir}')
    print(f'INFO [test_results_dir]: {test_results_dir}')
    print(f'INFO [ARGS.pcap_dir]   : {pcap_dir}')
    print(f'INFO [ARGS.pcap_dir]   : {api_dir}')

    # Import config.json
    config = import_json(config_file)
    username = config['ldap_user']
    password = config['ldap_pass']
    sitestores = config['sitestores']
    midpoints = config['midpoints']
    private_key = config['private_key']
    authorization = config['authorization']
    ais_url = config['ais_url']
    notification_url = config['notification_url']

    # Build list of pcaps
    pcaps = build_files_list(pcap_dir)

    # Verify we can ping all of the endpoints
    servers = sitestores + midpoints
    delim(title=' Preflight Check - Ping All Servers ')
    ping_test(servers)

    # Generate Test Cases
    if ARGS.generate_test_cases:
        delim(title=' Generating Test Cases ')
        generate_test_cases(pcaps, authorization, sitestores, midpoints, username,
                            password, private_key, analytics_delay, ais_url,
                            notification_url, api_dir, ARGS.timeout)

    if not os.path.exists(ARGS.test_results_dir):  # test results folder path
        os.makedirs(ARGS.test_results_dir)
    if not os.path.exists(ARGS.api_dir):  # api logging folder path
        os.makedirs(ARGS.api_dir)

    # Build a list of test cases
    test_files = build_files_list(test_case_dir)

    # Process each JSON test case
    for file in test_files:
        test_json = import_json(file)
        print(f'INFO [test_json]: {test_json}')
        test_name = test_json['name']
        print(f'INFO [test_name]: {test_name}')
        test_name_json = test_name + '.json'
        print(f'INFO [test_name_json]: {test_name_json}')
        test_description = test_json['description']
        print(f'INFO [test_description]: {test_description}')
        pcap_dict = test_json['pcap']
        print(f'INFO [pcap_dict]: {pcap_dict}')
        print(f'INFO [pcap_dir]: {pcap_dir}')

        # Verify test case pcap is in pcaps folder
        test_case_pcaps = []
        for key in pcap_dict.keys():
            pcap_abs_path = os.path.join(pcap_dir, key)
            print(f'INFO [pcap_abs_path]: {pcap_abs_path}')
            if os.path.isfile(pcap_abs_path):
                print(f'INFO [pcap found]: True')
                test_case_pcaps.append(pcap_abs_path)
            else:
                print(color_red() + f'ERROR: {os.path.join(pcap_dir, key)} was not found!')

        delay_time_seconds = test_json['delay_time_seconds']
        print(f'INFO [Delay]: {delay_time_seconds}')
        test_environments = sitestores + midpoints

        if ARGS.test_environment:
            test_environments.append(ARGS.test_environment)

        pass_count = 0
        fail_count = 0
        total_count = 0
        xray_report_json = {
            "info": {
                "summary": f'{test_name} PCAP [{file}] - {ARGS.test_plan_key}',
                "description": f'{test_description} - Performed at: {str(NOW)}',
                "version": ARGS.test_platform_version,
                "testPlanKey": ARGS.test_plan_key,
                "test_environments": test_environments
            },
            "tests": []
        }

        print(f'INFO [xray_report_json]: {xray_report_json}')

        # Reset Sitestore(s) and Midpoint(s)
        for server in servers:
            with ssh_connection(server, username, password, private_key) as cxn:
                print(f'INFO: Now resetting {server}')
                reset_system(cxn, server)

        # Copy test case pcap to target systems
        for midpoint in midpoints:
            with ssh_connection(midpoint, username, password, private_key) as cxn:
                dest_path = '/home/{}/pcaps'.format(username)
                cxn.run(f'mkdir -p {dest_path} && chmod -R 755 {dest_path}')
                copy_pcaps(cxn, test_case_pcaps, dest_path)

        # Playback pcap(s)
        for midpoint in midpoints:
            with ssh_connection(midpoint, username, password, private_key) as cxn:
                print(f'INFO [Midpoint]: {midpoint}')
                interface = 'bond0'        # Hardware, Dragos HQ Systems
                if 'cloud' in midpoint:
                    interface = 'replay'   # AWS
                print(f'INFO [Replay interface]: {interface}')
                for pcap in test_case_pcaps:
                    pcap_abs_path = '/home/{}/pcaps/{}'.format(username, pcap.split(os.sep)[-1])
                    pcap_playback(cxn, pcap_abs_path, interface, PCAP_MBPS, username)

        print(color_green() + 'INFO: PCAP playback operations complete.')
        print(color_reset() + f'INFO: Sleeping for {delay_time_seconds} seconds. Waiting for platform analytics.')
        time.sleep(int(delay_time_seconds))
        print(color_blue() + f'INFO: Executing Test Case: {test_name}')
        print(color_cyan() + test_description)
        print(color_reset())
        headers = {"Content-Type": "application/json", "Authorization": authorization}

        # Check the results of the pcap(s) playback
        for test_case in test_json['tests']:
            print(f'INFO [test_case]: {test_case}')
            total_count += 1
            print(f'INFO [Test Case Name]: {test_case["name"]}')
            print(f'INFO [Test Case Key ]: {test_case["testKey"]}')
            print(f'INFO [Test Case     ]: {test_case["testCase"]}')
            if test_case["testCase"] == "Asset Count":
                test_debug_filename = "get_asset_count_" + test_name_json  # logging of server response
                test_result = get_asset_count(ais_url, headers, timeout, api_dir, test_debug_filename)
            elif test_case["testCase"] == "Notification Count":
                test_debug_filename = "get_notification_count_" + test_name_json  # logging of server response
                test_result = get_notification_count(notification_url, headers, timeout, api_dir, test_debug_filename)
            elif test_case["testCase"] == "Communication Summary":
                test_debug_filename = "get_communication_summary_" + test_name_json  # logging of server response
                test_result = get_communication_summary(ais_url, headers, timeout, api_dir, test_debug_filename)
            elif test_case["testCase"] == "Asset Types":
                test_debug_filename = "get_asset_count_by_type_" + test_name_json  # logging of server response
                test_result = get_asset_count_by_attribute(ais_url, "type", headers, timeout,
                                                           api_dir, test_debug_filename)
            elif test_case["testCase"] == "Asset Vendors":
                test_debug_filename = "get_asset_count_by_vendor_" + test_name_json  # logging of server response
                test_result = get_asset_count_by_attribute(ais_url, "Vendor", headers, timeout,
                                                           api_dir, test_debug_filename)
            else:
                print(color_red() + f'ERROR: {test_case["testCase"]} is invalid!')
                continue
            # print(color_reset() + "Test Response")
            # print(test_result)
            if test_result:
                # We support two data structures in the test cases
                #  1.  expected_results ->  Key Value pair recursive search of Dragos API query JSON results
                #  2.  expected_dicts   ->  Dict objects recursive search of Dragos API query JSON results
                # The test cases can include one or both of these test case types and multiple of each.
                print(f'INFO [test_result]: {test_result}')
                if "expected_results" in test_case:
                    for expected_response_dict in test_case["expected_results"]:
                        # print(expected_response_dict)
                        matching_keys = get_recursively(test_result, expected_response_dict["key"])
                        # print(matching_keys)
                        if expected_response_dict["value"] in matching_keys:
                            print(color_green() + expected_response_dict["name"] + " TEST PASS!")
                            print("{} == {}".format(expected_response_dict, matching_keys))
                            xray_report_json["tests"].append({"testKey": test_case["testKey"],
                                                              "status": "PASSED"})
                            pass_count += 1
                        else:
                            print(color_red() + expected_response_dict["name"] + " TEST FAIL!")
                            print("{} != {}".format(expected_response_dict, matching_keys))
                            xray_report_json["tests"].append({"testKey": test_case["testKey"],
                                                              "status": "FAILED"})
                            fail_count += 1

                if "expected_dicts" in test_case:
                    matching_dicts = []
                    for expected_response_dict in test_case["expected_dicts"]:
                        list_matching_dicts = get_recursively_dict(test_result, expected_response_dict)
                        for dict_match in list_matching_dicts:
                            matching_dicts.append(dict_match)
                    if ARGS.debug:
                        print("expected_response_dict " + str(test_case["expected_dicts"]))
                    if ARGS.debug:
                        print("matching_dicts " + str(matching_dicts))
                    if matching_dicts == test_case["expected_dicts"]:
                        print(color_green() + "TEST CASE PASSED!")
                        xray_report_json["tests"].append({
                            "testKey": test_case["testKey"],
                            "status": "PASSED"
                        })
                        pass_count += 1
                    else:
                        print(color_red() + "TEST CASE FAILED!")
                        xray_report_json["tests"].append({
                            "testKey": test_case["testKey"],
                            "status": "FAILED"
                        })
                        unmatched_item = [item for item in test_case["expected_dicts"] if item not in matching_dicts]
                        print("TEST CASE FAILED BECAUSE THE FOLLOWING RESULTS WERE MISSING: " + str(unmatched_item))
                        fail_count += 1
            else:
                print(color_cyan() + "Dragos Platform API Error code returned!")
        # print(str(report_results_json))
        print(color_reset() + "Tests Run: " + str(total_count))
        print("Tests Failed: " + str(fail_count))
        print("Tests Passed: " + str(pass_count))
        with open(os.path.join(test_results_dir, test_name + ".json"), 'w') as json_file:
            if ARGS.pretty:
                json.dump(xray_report_json, json_file, sort_keys=True, indent=4)
            else:
                json.dump(xray_report_json, json_file)

    print(color_reset() + "FINISHED!")


if os.name == 'nt':
    STD_OUT_HANDLE = ctypes.windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)
print(color_cyan())
print('''
                   //////////////////////////////////__
         ////////////////////////////////////////////////////////_
    //////////////////      ///////////////////////////
  ////////               //////////////////////////
///                  //////////////////////////
/                 /////////    ////////////                   \\    \\
               ////////      ////////////         \\\\\\\\\\\\\\     \\\\   \\\\
             //////         ///////////             /////////////  \\\\\\\\
          //////           ///.//////      \\\\\\\\\\\\\\\\\\//////////////////\\\\
        ////.            //// /////            /////           ////////\\\\
      ///.              ///   ////     \\\\\\\\\\\\\\\\\\//               /////  \\\\\\
    ///                 //   ////           ////                   //// .\\\\\\
   /                   /     ////     \\\\\\\\\\\\///                   )///////\\\\\\
 /                    /     ////        //////                    )////////\\\\\\
                            .///   \\\\\\\\\\\\\\////                         //////\\\\
                             ////       //////                           /////\\\\
                              ////      //////                             ///\\\\\\
                               /////     //////                                 \\
                                 /////   ///////
                                   ///////////////
                                      ///////////////
                                          ///////////////
                                              //////////////
                                                  //////////////
                      ______                          ////////////////\\\\\\\\
                //////////////////                        ////////////
              ///////////////////////                        /////////////\\\\\\\\\\
            ///               ////////                          ///////////
           ///                  ////////                          //////////\\\\\\\\
           //                     ///////                          //////////\\  \\
          //                       ///////                          /////////\\\\
          \\\\                        ///////                         //////// \\\\\\
          \\\\\\                        ////////                       ///////     \\
           \\\\\\                         ///////                     /////
            \\\\                         ////////                  /////
              \\\\                        /////////              /////
               \\\\  \\\\                    ////////////////////////
                 \\\\\\\\\\                    ////////////////
               \\\\\\\\\\\\\\\\\\
''')

print(color_red() + f'DRAGOS Integrated Platform Sensor Test Suite {__version__}')
PARSER = argparse.ArgumentParser(description='Execute Dragos Platoform Sensor Test Cases ' + color_reset())
PARSER.add_argument("-config", type=str, default="config.json",
                    help='Specify the path to the test case config file (default: %(default)s)')
PARSER.add_argument("-test_case_dir", type=str, default="tests/",
                    help='Specify the path to the test case directory (default: %(default)s)')
PARSER.add_argument("-test_results_dir", type=str, default="test_results/",
                    help='Specify the path where we will store our JSON test results (default: %(default)s)')
PARSER.add_argument("-pcap_dir", type=str, default="pcaps/",
                    help='Specify the path where the pcaps can be found (default: %(default)s)')
PARSER.add_argument("-api_dir", type=str, default="api/",
                    help='Specify the path where the api responses will be logged (default: %(default)s)')
PARSER.add_argument("-timeout", type=int, default=600,
                    help='Dragos Platform API connection timeout (default: %(default)s)')
PARSER.add_argument("-test_plan_key", type=str, default="PLATDEV-####",
                    help='Dragos Platform automated test plan reference code (default: %(default)s)')
PARSER.add_argument("-test_platform_version", type=str, default="1.6RC1",
                    help='Dragos Platform version that was tested (default: %(default)s)')
PARSER.add_argument("-test_environment", type=str, default="",
                    help='Dragos Platform target environments for the test (default: %(default)s)')
PARSER.add_argument("-generate_test_cases", action='store_true', help='Generate a test case JSON for '
                    'each pcap in the pcap folder')
PARSER.add_argument("-analytics_delay", type=int, default=30,
                    help='Delay (in seconds) to allow analytics to fire after pcap playback (default: %(default)s)')
PARSER.add_argument("-pretty", action='store_true', help='Write JSON in pretty print formatting', default=True)
PARSER.add_argument("-debug", action='store_true', help='Display debug output during parsing')
ARGS = PARSER.parse_args()


if __name__ == "__main__":
    main()
