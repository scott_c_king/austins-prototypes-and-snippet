
# DRAGOS Integrated Platform Sensor Test Suite 

This program is designed to perform "fully integrated" tests against the Dragos Platform Sensor.
The project goal is to create small, atomic, and autonomous Platform Sensor test cases that can identify
sensor issues including (but not limited to):  
1. Throughput  
2. Packet Loss  
3. Data Integrity  
4. Data context impact on packet loss   
Ultimately, this tool is to be integrated into a test framework like Jenkins, Xray and align with the Dragos Jira QA
conventions.  
  
Dependencies:  
pip install requests  
pip install fabric  
  
## How does it work?

The Platform test suite is capable of two primary operating modes:  
1. Run a folder of test cases against a target Platform Sensor  
2. Generate test cases based on a folder of PCAPS  

![alt text](https://bitbucket.org/ascot/austins-prototypes-and-snippet/raw/a41bc8adb8417143dc70cefea5269f79d7878c95/PlatformSensorTestIntegration/readme_images/Slide3.PNG "How does it work?")

Each test case is a JSON file that contains:  
1. A reference to one or more PCAPS to be loaded at a specific ingest rate.  
2. One or more tests to be run against the Platform API to measure Throughput, Packet Loss, Data Integrity, Data context   

## What Happens when we run a Test Case?
The Platform Sensor Test automates the process of wiping a platform instance, copying multiple PCAPS (found in the *"pcaps"* folder) to the remote Sensor and replaying the PCAP traffic (based on *"config.json"* file).
Then it will wait for a specified amount of time before executing calls to the API and comparing the results to what it expects to see based on the JSON test case contents (found in the *"tests"* folder)
Finally it writes out the test results of the test cases in a JSON file format (*"testresults"* folder)
![alt text](https://bitbucket.org/ascot/austins-prototypes-and-snippet/raw/a41bc8adb8417143dc70cefea5269f79d7878c95/PlatformSensorTestIntegration/readme_images/Slide5.PNG "Test Case Run")

## What do the test case JSON files contain?

![alt text](https://bitbucket.org/ascot/austins-prototypes-and-snippet/raw/a41bc8adb8417143dc70cefea5269f79d7878c95/PlatformSensorTestIntegration/readme_images/Slide6.PNG "JSON")
![alt text](https://bitbucket.org/ascot/austins-prototypes-and-snippet/raw/a41bc8adb8417143dc70cefea5269f79d7878c95/PlatformSensorTestIntegration/readme_images/Slide7.PNG "JSON")


## Getting Started with the Dragos Sensor Test Suite

The Dragos Sensor test Suite has numerious parameters which can be specified on the command line.  
You can view these parameters by executing the following command:  
`$ python3 PlatformSensorTest.py -h `

Output:  
```
DRAGOS Integrated Platform Sensor Test Suite V0.2.0
usage: PlatformSensorTest.py [-h] [-config CONFIG]
                             [-test_case_folder TEST_CASE_FOLDER]
                             [-test_results_folder TEST_RESULTS_FOLDER]
                             [-pcap_dir PCAP_FOLDER]
                             [-api_folder API_FOLDER] [-timeout TIMEOUT]
                             [-test_plan_key TEST_PLAN_KEY]
                             [-test_platform_version TEST_PLATFORM_VERSION]
                             [-test_environment TEST_ENVIRONMENT]
                             [-generate_test_cases] [-wait_time WAIT_TIME]
                             [-pretty] [-debug]

Execute Dragos Platoform Sensor Test Cases 

optional arguments:
  -h, --help            show this help message and exit
  -config CONFIG        Specify the path to the test case config file
                        (default: config.json)
  -test_case_folder TEST_CASE_FOLDER
                        Specify the path to the test case folder (default:
                        ./tests)
  -test_results_folder TEST_RESULTS_FOLDER
                        Specify the path where we will store our JSON test
                        results (default: ./testresults)
  -pcap_dir PCAP_FOLDER
                        Specify the path where the pcaps can be found
                        (default: ./pcaps)
  -api_folder API_FOLDER
                        Specify the path where the api responses will be
                        logged (default: ./api)
  -timeout TIMEOUT      Dragos Platform API connection timeout (default: 600)
  -test_plan_key TEST_PLAN_KEY
                        Dragos Platform automated test plan reference code
                        (default: PLATDEV-####)
  -test_platform_version TEST_PLATFORM_VERSION
                        Dragos Platform version that was tested (default:
                        1.6RC1)
  -test_environment TEST_ENVIRONMENT
                        Dragos Platform target environments for the test
                        (default: )
  -generate_test_cases  Generate a test case JSON for each pcap in the pcap
                        folder
  -wait_time WAIT_TIME  Generate test case wait time (in seconds) before
                        collecting test case data (default: 1200)
  -pretty               Write JSON in pretty print formatting
  -debug                Display debug output during parsing

```

In order to communicate with a Platform Sensor and Platform Instance, you will need to set the parameters of the config.json file.
Config.json contents:
```json
{
   "ldap_user"          : "<USERNAME>",
   "ldap_pass"          : "<PASSWORD>",
   "private_key"        : "<PATH TO KEY FILE>",
   "pcap_dir"        : "<LOCATION ON REMOTE SENSOR WHERE PCAPS WILL BE COPIES AND EXECUTED> /tmp/",
   "Authorization"      : "<PLATFORM API AUTHORIZATION TOKEN>Basic YWRtaW46RHJAZ29zU3lzdDNt",
   "ais_url"            : "https://demo-dev.dragos.services/assets/api/v3/operations",
   "notification_url"   : "https://demo-dev.dragos.services/notifications/api/v2/notification",
   "sitestores"         : ["demo-dev.dragos.services"],
   "midpoints"          : ["midpoint-demodev.hq.dragos.services"]
}
```

The Authorization token included above is the default admin creds for the platform user. 
Most test environments will be setup with the default user, but this should never work in PROD.
If the instance doesn’t have the Admin user or the default login/password for the admin user changed that will stop working.

Once everything is setup in the config.json file, you can run the testsuite using the default settings which executes all the test cases
that are found in the ./tests/ subfolder.  

`$ python3 PlatformSensorTest.py `

If you wish to generate *new* test cases based on a folder of PCAPS, you can run a command like this:  
`$ python3 PlatformSensorTest.py -generate_test_cases -pcap_dir ./new_pcaps/ -test_case_folder ./new_test_cases/ -wait_time 1200`

The previous command will replay each of the PCAPS found in the `./new_pcaps/` folder, wait for 1200 seconds and then create test case JSON files
based on the API response of the Dragos Platform Site Store.  It will create one test case file per PCAP file. 
