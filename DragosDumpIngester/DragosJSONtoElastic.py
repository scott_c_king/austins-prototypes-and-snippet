import requests, json, os
from elasticsearch import Elasticsearch

directory = '.'

res = requests.get('http://localhost:9200')
print (res.content)
es = Elasticsearch([{'host': 'localhost', 'port': '9200'}])

for filename in os.listdir(directory):
    if filename.endswith(".json"):
        f = open(filename)
        print filename
        # One JSON object per line
        json_objects = []
        for line in f:
            json_objects.append(line)

        #docket_content = f.read()
        # Send the data into es
        try:
            es.index(index='dragos2', doc_type='docket', body=json.loads(json.dumps(json_objects)))
        except Exception as e:
            print(e.info)
