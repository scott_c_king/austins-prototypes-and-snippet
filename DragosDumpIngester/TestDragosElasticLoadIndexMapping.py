import unittest
import os
import DragosDumpIngester
import yaml
import json


with open("DragosIngesterConfig.yaml", 'r') as stream:
    config = yaml.safe_load(stream)

class IndexMappingGeneratorTest(unittest.TestCase):
    def test_assets_index_mapping_load(self):
        parser_type = "Assets"
        parser = config['File Parsers'][parser_type][0]
        json_index_mapping = DragosDumpIngester.do_generate_index_mapping_json(parser)
        print json_index_mapping
        result = DragosDumpIngester.do_load_index_mapping_to_elastic(parser, json_index_mapping)
        self.assertEqual(result["acknowledged"], True)

    def test_drivers_index_mapping_load(self):
        parser_type = "Drivers"
        parser = config['File Parsers'][parser_type][0]
        json_index_mapping = DragosDumpIngester.do_generate_index_mapping_json(parser)
        print json_index_mapping
        result = DragosDumpIngester.do_load_index_mapping_to_elastic(parser, json_index_mapping)
        self.assertEqual(result["acknowledged"], True)

    def test_system_csv_index_mapping_load(self):
        parser_type = "System CSV"
        parser = config['File Parsers'][parser_type][0]
        json_index_mapping = DragosDumpIngester.do_generate_index_mapping_json(parser)
        print json_index_mapping
        result = DragosDumpIngester.do_load_index_mapping_to_elastic(parser, json_index_mapping)
        self.assertEqual(result["acknowledged"], True)

    def test_network_connection_mapping_load(self):
        parser_type = "Network Connections"
        parser = config['File Parsers'][parser_type][0]
        json_index_mapping = DragosDumpIngester.do_generate_index_mapping_json(parser)
        print json_index_mapping
        result = DragosDumpIngester.do_load_index_mapping_to_elastic(parser, json_index_mapping)
        self.assertEqual(result["acknowledged"], True)

    def test_all_files_icacls_mapping_load(self):
        parser_type = "All Files ICACLS"
        parser = config['File Parsers'][parser_type][0]
        json_index_mapping = DragosDumpIngester.do_generate_index_mapping_json(parser)
        print json_index_mapping
        result = DragosDumpIngester.do_load_index_mapping_to_elastic(parser, json_index_mapping)
        self.assertEqual(result["acknowledged"], True)

    def test_all_files_mapping_load(self):
        parser_type = "All Files"
        parser = config['File Parsers'][parser_type][0]
        json_index_mapping = DragosDumpIngester.do_generate_index_mapping_json(parser)
        print json_index_mapping
        result = DragosDumpIngester.do_load_index_mapping_to_elastic(parser, json_index_mapping)
        self.assertEqual(result["acknowledged"], True)

    def test_wmic_process_load(self):
        parser_type = "WMIC Process"
        parser = config['File Parsers'][parser_type][0]
        json_index_mapping = DragosDumpIngester.do_generate_index_mapping_json(parser)
        print json_index_mapping
        result = DragosDumpIngester.do_load_index_mapping_to_elastic(parser, json_index_mapping)
        self.assertEqual(result["acknowledged"], True)

    def test_systeminfo_qbs_load(self):
        parser_type = "System Information"
        parser = config['File Parsers'][parser_type][0]
        json_index_mapping = DragosDumpIngester.do_generate_index_mapping_json(parser)
        print json_index_mapping
        result = DragosDumpIngester.do_load_index_mapping_to_elastic(parser, json_index_mapping)
        self.assertEqual(result["acknowledged"], True)

    def test_systeminfo_ips_load(self):
        parser_type = "System Information"
        parser = config['File Parsers'][parser_type][1]
        json_index_mapping = DragosDumpIngester.do_generate_index_mapping_json(parser)
        print json_index_mapping
        result = DragosDumpIngester.do_load_index_mapping_to_elastic(parser, json_index_mapping)
        self.assertEqual(result["acknowledged"], True)

    def test_routing_table_ipv4_load(self):
        parser_type = "Network Routes"
        parser = config['File Parsers'][parser_type][0]
        json_index_mapping = DragosDumpIngester.do_generate_index_mapping_json(parser)
        print json_index_mapping
        result = DragosDumpIngester.do_load_index_mapping_to_elastic(parser, json_index_mapping)
        self.assertEqual(result["acknowledged"], True)

    def test_routing_table_ipv6_load(self):
        parser_type = "Network Routes"
        parser = config['File Parsers'][parser_type][1]
        json_index_mapping = DragosDumpIngester.do_generate_index_mapping_json(parser)
        print json_index_mapping
        result = DragosDumpIngester.do_load_index_mapping_to_elastic(parser, json_index_mapping)
        self.assertEqual(result["acknowledged"], True)

    def test_arp_table_load(self):
        parser_type = "Network ARP Tables"
        parser = config['File Parsers'][parser_type][0]
        json_index_mapping = DragosDumpIngester.do_generate_index_mapping_json(parser)
        print json_index_mapping
        result = DragosDumpIngester.do_load_index_mapping_to_elastic(parser, json_index_mapping)
        self.assertEqual(result["acknowledged"], True)

    def test_firewall_rules_load(self):
        parser_type = "Firewall Rules"
        parser = config['File Parsers'][parser_type][0]
        json_index_mapping = DragosDumpIngester.do_generate_index_mapping_json(parser)
        print json_index_mapping
        result = DragosDumpIngester.do_load_index_mapping_to_elastic(parser, json_index_mapping)
        self.assertEqual(result["acknowledged"], True)

    def test_firewall_profiles_load(self):
        parser_type = "Firewall Profiles"
        parser = config['File Parsers'][parser_type][0]
        json_index_mapping = DragosDumpIngester.do_generate_index_mapping_json(parser)
        print json_index_mapping
        result = DragosDumpIngester.do_load_index_mapping_to_elastic(parser, json_index_mapping)
        self.assertEqual(result["acknowledged"], True)

    def test_firewall_legacy_load(self):
        parser_type = "Firewall Legacy"
        parser = config['File Parsers'][parser_type][0]
        json_index_mapping = DragosDumpIngester.do_generate_index_mapping_json(parser)
        print json_index_mapping
        result = DragosDumpIngester.do_load_index_mapping_to_elastic(parser, json_index_mapping)
        self.assertEqual(result["acknowledged"], True)

    def test_network_ipconfig_load(self):
        parser_type = "Network IP Config"
        parser = config['File Parsers'][parser_type][0]
        json_index_mapping = DragosDumpIngester.do_generate_index_mapping_json(parser)
        print json_index_mapping
        result = DragosDumpIngester.do_load_index_mapping_to_elastic(parser, json_index_mapping)
        self.assertEqual(result["acknowledged"], True)

    def test_wmic_product_load(self):
        parser_type = "WMIC Product"
        parser = config['File Parsers'][parser_type][0]
        json_index_mapping = DragosDumpIngester.do_generate_index_mapping_json(parser)
        print json_index_mapping
        result = DragosDumpIngester.do_load_index_mapping_to_elastic(parser, json_index_mapping)
        self.assertEqual(result["acknowledged"], True)

    def test_workstation_ipconfig_windows_xp_load(self):
        parser_type = "Network IP Config"
        parser = config['File Parsers'][parser_type][0]
        json_index_mapping = DragosDumpIngester.do_generate_index_mapping_json(parser)
        print json_index_mapping
        result = DragosDumpIngester.do_load_index_mapping_to_elastic(parser, json_index_mapping)
        self.assertEqual(result["acknowledged"], True)


if __name__ == '__main__':
    unittest.main()
