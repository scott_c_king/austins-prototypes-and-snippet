import unittest
import os
import DragosDumpIngester
import yaml
import json

test_folder = "testdata"
with open("DragosIngesterConfig.yaml", 'r') as stream:
    config = yaml.safe_load(stream)

class TestIngestion(unittest.TestCase):
    def test_drivers_parsing(self):
        with open(test_folder + os.sep + "driverquery.csv") as file:
            file_contents = file.read()
        index_type = "Drivers"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(json_data[index_name].__len__(), 399)

    def test_system_csv_parsing(self):
        with open(test_folder + os.sep + "system.csv") as file:
            file_contents = file.read()
        index_type = "System CSV"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(json_data[index_name].__len__(), 1)

    def test_network_connections_parsing(self):
        with open(test_folder + os.sep + "network_connections.txt") as file:
            file_contents = file.read()
        index_type = "Network Connections"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(index_name in json_data, True)

    def test_all_files_icacls_parsing(self):
        with open(test_folder + os.sep + "all_files_icacls.txt") as file:
            file_contents = file.read()
        index_type = "All Files ICACLS"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(index_name in json_data, True)

    def test_dragos_all_files_parsing(self):
        with open(test_folder + os.sep + "all_files.txt") as file:
            file_contents = file.read()
        index_type = "All Files"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(index_name in json_data, True)

    def test_dragos_wmic_processes_parsing(self):
        with open(test_folder + os.sep + "wmic_process.txt") as file:
            file_contents = file.read()
        index_type = "WMIC Process"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(index_name in json_data, True)

    def test_dragos_systeminfo_patches_parsing(self):
        with open(test_folder + os.sep + "systeminfo.txt") as file:
            file_contents = file.read()
        index_type = "System Information"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(index_name in json_data, True)

    def test_dragos_systeminfo_ips_parsing(self):
        with open(test_folder + os.sep + "systeminfo.txt") as file:
            file_contents = file.read()
        index_type = "System Information"
        index_name = config["File Parsers"][index_type][1]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(index_name in json_data, True)

    def test_dragos_network_routes_parsing(self):
        with open(test_folder + os.sep + "network_routes.txt") as file:
            file_contents = file.read()
        index_type = "Network Routes"
        index_name = config["File Parsers"][index_type][1]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(index_name in json_data, True)

    def test_dragos_network_routes_parsing(self):
        with open(test_folder + os.sep + "network_routes.txt") as file:
            file_contents = file.read()
        index_type = "Network Routes"
        index_name = config["File Parsers"][index_type][1]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(index_name in json_data, True)

    def test_dragos_arp_tables_parsing(self):
        with open(test_folder + os.sep + "arp_tables.txt") as file:
            file_contents = file.read()
        index_type = "Network ARP Tables"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(index_name in json_data, True)

    def test_dragos_network_external_dns_parsing(self):
        with open(test_folder + os.sep + "network_external_dns.txt") as file:
            file_contents = file.read()
        index_type = "Network External Dns"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(index_name in json_data, True)

    def test_dragos_network_firewall_rules_parsing(self):
        with open(test_folder + os.sep + "network_firewall_rules.txt") as file:
            file_contents = file.read()
        index_type = "Firewall Rules"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(index_name in json_data, True)

    def test_dragos_network_firewall_rules_parsing(self):
        with open(test_folder + os.sep + "network_firewall_profiles.txt") as file:
            file_contents = file.read()
        index_type = "Firewall Profiles"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(index_name in json_data, True)

    def test_dragos_network_firewall_legacy_parsing(self):
        with open(test_folder + os.sep + "network_firewall_legacy.txt") as file:
            file_contents = file.read()
        index_type = "Firewall Legacy"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(index_name in json_data, True)

    def test_dragos_network_firewall_legacy_state_parsing(self):
        with open(test_folder + os.sep + "network_firewall_legacy_state.txt") as file:
            file_contents = file.read()
        index_type = "Firewall Legacy State"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(index_name in json_data, True)

    def test_dragos_network_ipconfig_parsing(self):
        with open(test_folder + os.sep + "network_ipconfig.txt") as file:
            file_contents = file.read()
        index_type = "Network IP Config"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(json_data[index_name].__len__(), 11)

    def test_dragos_network_dns_parsing(self):
        with open(test_folder + os.sep + "network_dns.txt") as file:
            file_contents = file.read()
        index_type = "Network DNS"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(json_data[index_name].__len__(), 43)

    def test_dragos_tasks_parsing(self):
        with open(test_folder + os.sep + "software_tasks.txt") as file:
            file_contents = file.read()
        index_type = "Tasks"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(json_data[index_name].__len__(), 1)

    def test_dragos_tasks_csv_parsing(self):
        with open(test_folder + os.sep + "software_tasks.csv") as file:
            file_contents = file.read()
        index_type = "Tasks CSV"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(json_data[index_name].__len__() , 265)

    def test_dragos_tasks_csv_parsing(self):
        with open(test_folder + os.sep + "software_task_modules.csv") as file:
            file_contents = file.read()
        index_type = "Tasks Modules CSV"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(json_data[index_name].__len__(), 265)

    def test_dragos_tasks_store_apps_parsing(self):
        with open(test_folder + os.sep + "task_store_apps.csv") as file:
            file_contents = file.read()
        index_type = "Tasks Store Apps CSV"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(json_data[index_name].__len__(), 28)

    def test_dragos_scheduled_tasks_parsing(self):
        with open(test_folder + os.sep + "schtasks.txt") as file:
            file_contents = file.read()
        index_type = "Scheduled Tasks"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(json_data[index_name].__len__(), 284)

    def test_dragos_whoami_parsing(self):
        with open(test_folder + os.sep + "whoami.txt") as file:
            file_contents = file.read()
        index_type = "Whoami"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(json_data[index_name].__len__(), 37)

    def test_dragos_local_groups_parsing(self):
        with open(test_folder + os.sep + "localgroups.txt") as file:
            file_contents = file.read()
        index_type = "Localgroups"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(json_data[index_name].__len__(), 11)

    def test_dragos_windows_2000_local_groups_parsing(self):
        with open(test_folder + os.sep + "AUSTIN-0B82F776_system_localgroups.txt") as file:
            file_contents = file.read()
        index_type = "Localgroups"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(json_data[index_name].__len__(), 11)

    def test_dragos_local_group_administrator_parsing(self):
        with open(test_folder + os.sep + "localgroup_administrators.txt") as file:
            file_contents = file.read()
        index_type = "Localgroup Administrators"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(json_data[index_name].__len__(), 2)

    def test_dragos_local_group_users_parsing(self):
        with open(test_folder + os.sep + "localgroup_users.txt") as file:
            file_contents = file.read()
        index_type = " "
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(json_data[index_name].__len__(), 2)

    def test_dragos_local_group_users_parsing(self):
        with open(test_folder + os.sep + "system_accounts.txt") as file:
            file_contents = file.read()
        index_type = "System Accounts"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(json_data[index_name].__len__(), 1)

    def test_dragos_system_drives_parsing(self):
        with open(test_folder + os.sep + "system_drives.txt") as file:
            file_contents = file.read()
        index_type = "System Drives"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(json_data[index_name].__len__(), 1)

    def test_dragos_system_env_vars_parsing(self):
        with open(test_folder + os.sep + "system_env_vars.txt") as file:
            file_contents = file.read()
        index_type = "Env Vars"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(json_data[index_name].__len__(), 42)

    def test_dragos_wmic_bios_parsing(self):
        with open(test_folder + os.sep + "wmic_bios.txt") as file:
            file_contents = file.read()
        index_type = "WMIC BIOS"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(json_data[index_name][0].__len__(), 25)

    def test_dragos_wmic_dcom_parsing(self):
        with open(test_folder + os.sep + "wmic_dcom.txt") as file:
            file_contents = file.read()
        index_type = "WMIC DCOM"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(json_data[index_name].__len__(), 477)

    def test_dragos_wmic_group_parsing(self):
        with open(test_folder + os.sep + "wmic_group.txt") as file:
            file_contents = file.read()
        index_type = "WMIC Group"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(json_data[index_name].__len__(), 11)

    def test_dragos_wmic_useraccounts_parsing(self):
        with open(test_folder + os.sep + "wmic_useraccounts.txt") as file:
            file_contents = file.read()
        index_type = "WMIC Useraccounts"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(json_data[index_name].__len__(), 5)

    def test_dragos_wmic_product_parsing(self):
        with open(test_folder + os.sep + "wmic_product.txt") as file:
            file_contents = file.read()
        index_type = "WMIC Product"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(json_data[index_name].__len__(), 120)

    def test_dragos_wmic_qfe_parsing(self):
        with open(test_folder + os.sep + "wmic_qfe.txt") as file:
            file_contents = file.read()
        index_type = "WMIC QFE"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(json_data[index_name].__len__(), 8)

    def test_dragos_wmic_services_parsing(self):
        with open(test_folder + os.sep + "wmic_services.txt") as file:
            file_contents = file.read()
        index_type = "WMIC Services"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(json_data[index_name].__len__(), 300)

    def test_dragos_ini_file_parsing(self):
        with open(test_folder + os.sep + "ini_files.txt") as file:
            file_contents = file.read()
        index_type = "Software INI Files"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(json_data[index_name].__len__(), 12)

    # def test_dragos_windows_registry_parsing(self):
    #     with open(test_folder + os.sep + "registry.txt") as file:
    #         file_contents = file.read()
    #     index_type = "Windows Registry"
    #     index_name = config["File Parsers"][index_type][0]["Index Name"]
    #     json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
    #     self.assertEqual(json_data[index_name].__len__(), 215018)

    def test_dragos_martha_system_info_parsing(self):
        with open(test_folder + os.sep + "MARTHA_systeminfo.txt") as file:
            file_contents = file.read()
        index_type = "System Information"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "MARTHA")
        self.assertEqual(json_data[index_name][0].__len__(), 32)

    def test_dragos_ver_parsing(self):
        with open(test_folder + os.sep + "ver.txt") as file:
            file_contents = file.read()
        index_type = "Windows Version"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(json_data[index_name].__len__(), 1)

    def test_eng_workstation_ipconfig_windows_xp_parsing(self):
        with open(test_folder + os.sep + "ENG_WORKSTATION_network_ipconfig.txt") as file:
            file_contents = file.read()
        index_type = "Network IP Config"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(json_data[index_name].__len__(), 3)

    def test_net_users_parsing(self):
        with open(test_folder + os.sep + "net_users.txt") as file:
            file_contents = file.read()
        index_type = "Net Users"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(json_data[index_name].__len__(), 7)

    def test_windows_2000_net_users_parsing(self):
        with open(test_folder + os.sep + "AUSTIN-0B82F776_net_users.txt") as file:
            file_contents = file.read()
        index_type = "Net Users"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(json_data[index_name].__len__(), 7)

    def test_windows_2000_net_users_parsing(self):
        with open(test_folder + os.sep + "all_files_properties_brief.txt") as file:
            file_contents = file.read()
        index_type = "Net Users"
        index_name = config["File Parsers"][index_type][0]["Index Name"]
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        self.assertEqual(json_data[index_name].__len__(), 7)



if __name__ == '__main__':
    unittest.main()
