import argparse
import os
import json
import yaml
import re
import binascii
import ctypes
from datetime import datetime, timedelta
import time
import ntpath
import struct
import sys
import tempfile
import multiprocessing
from elasticsearch import Elasticsearch

# Dragos Dump Ingester
# Created By: Austin Scott - May 2019
# Recursively parses through the folder structure of the output of the Dragos.bat system information collector
# Maps the data types (date, time, int, float, string)
# Auto generates Elastic Search indexes and index mapping
# Creates relationship between all Elastic Documents based on asset_id and asset_name
# Adds system information data to Elastic Search indexes
# TODO speed up REGEX by compiling each regex statement
# TODO investigate issues with CSV parser
# TODO implement multi-process async indexing


# References elastic data type date / time formatting:
#  https://www.joda.org/joda-time/apidocs/org/joda/time/format/DateTimeFormat.html
#  https://docs.oracle.com/javase/8/docs/api/java/time/format/DateTimeFormatter.html

# Dependencies:
# pip install Elasticsearch
# pip install pyyaml

# GLOBALS
ignore_folders = set(["venv", ".idea"])
# Adding support for color in a windows console window is hard
force_linux_color_codes = False
STD_OUTPUT_HANDLE = -11


# The following Prefetch class below was taken and then modified from:
# Contact: <accidentalassist@gmail.com>
# Source: https://github.com/PoorBillionaire/Windows-Prefetch-Parser
class Prefetch(object):
    def __init__(self, infile):
        self.pFileName = infile

        with open(infile, "rb") as f:
            if f.read(3) == "MAM":
                f.close()

                d = DecompressWin10()
                decompressed = d.decompress(infile)

                t = tempfile.mkstemp()

                with open(t[1], "wb+") as f:
                    f.write(decompressed)
                    f.seek(0)

                    self.parseHeader(f)
                    self.fileInformation26(f)
                    self.metricsArray23(f)
                    self.traceChainsArray30(f)
                    self.volumeInformation30(f)
                    self.getTimeStamps(self.lastRunTime)
                    self.directoryStrings(f)
                    self.getFilenameStrings(f)
                    return

        with open(infile, "rb") as f:
            self.parseHeader(f)

            if self.version == 17:
                self.fileInformation17(f)
                self.metricsArray17(f)
                self.traceChainsArray17(f)
                self.volumeInformation17(f)
                self.getTimeStamps(self.lastRunTime)
                self.directoryStrings(f)

            elif self.version == 23:
                self.fileInformation23(f)
                self.metricsArray23(f)
                self.traceChainsArray17(f)
                self.volumeInformation23(f)
                self.getTimeStamps(self.lastRunTime)
                self.directoryStrings(f)

            elif self.version == 26:
                self.fileInformation26(f)
                self.metricsArray23(f)
                self.traceChainsArray17(f)
                self.volumeInformation23(f)
                self.getTimeStamps(self.lastRunTime)
                self.directoryStrings(f)

            self.getFilenameStrings(f)

    def parseHeader(self, infile):
        # Parse the file header
        # 84 bytes
        self.version = struct.unpack_from("I", infile.read(4))[0]
        self.signature = struct.unpack_from("I", infile.read(4))[0]
        unknown0 = struct.unpack_from("I", infile.read(4))[0]
        self.fileSize = struct.unpack_from("I", infile.read(4))[0]
        executableName = struct.unpack_from("60s", infile.read(60))[0]
        executableName = executableName.split("\x00\x00")[0]
        self.executableName = executableName.replace("\x00", "")
        rawhash = hex(struct.unpack_from("I", infile.read(4))[0])
        self.hash = rawhash.lstrip("0x")

        unknown1 = infile.read(4)

    def fileInformation17(self, infile):
        # File Information
        # 68 bytes
        self.metricsOffset = struct.unpack_from("I", infile.read(4))[0]
        self.metricsCount = struct.unpack_from("I", infile.read(4))[0]
        self.traceChainsOffset = struct.unpack_from("I", infile.read(4))[0]
        self.traceChainsCount = struct.unpack_from("I", infile.read(4))[0]
        self.filenameStringsOffset = struct.unpack_from("I", infile.read(4))[0]
        self.filenameStringsSize = struct.unpack_from("I", infile.read(4))[0]
        self.volumesInformationOffset = struct.unpack_from("I", infile.read(4))[0]
        self.volumesCount = struct.unpack_from("I", infile.read(4))[0]
        self.volumesInformationSize = struct.unpack_from("I", infile.read(4))[0]
        self.lastRunTime = infile.read(8)
        unknown0 = infile.read(16)
        self.runCount = struct.unpack_from("I", infile.read(4))[0]
        unknown1 = infile.read(4)

    def metricsArray17(self, infile):
        # File Metrics Array
        # 20 bytes
        unknown0 = infile.read(4)
        unknown1 = infile.read(4)
        self.filenameOffset = struct.unpack_from("I", infile.read(4))[0]
        self.filenameLength = struct.unpack_from("I", infile.read(4))[0]
        unknown2 = infile.read(4)

    def traceChainsArray17(self, infile):
        # Read through the Trace Chains Array
        # Not being parsed for information
        # 12 bytes
        infile.read(12)

    def volumeInformation17(self, infile):
        # Volume information
        # 40 bytes per entry in the array

        infile.seek(self.volumesInformationOffset)
        self.volumesInformationArray = []
        self.directoryStringsArray = []
        count = 0

        while count < self.volumesCount:
            self.volPathOffset = struct.unpack_from("I", infile.read(4))[0]
            self.volPathLength = struct.unpack_from("I", infile.read(4))[0]
            self.volCreationTime = struct.unpack_from("Q", infile.read(8))[0]
            self.volSerialNumber = hex(struct.unpack_from("I", infile.read(4))[0])
            self.volSerialNumber = self.volSerialNumber.rstrip("L").lstrip("0x")
            self.fileRefOffset = struct.unpack_from("I", infile.read(4))[0]
            self.fileRefSize = struct.unpack_from("I", infile.read(4))[0]
            self.dirStringsOffset = struct.unpack_from("I", infile.read(4))[0]
            self.dirStringsCount = struct.unpack_from("I", infile.read(4))[0]
            unknown0 = infile.read(4)

            self.directoryStringsArray.append(self.directoryStrings(infile))

            infile.seek(self.volumesInformationOffset + self.volPathOffset)
            volume = {}
            volume["Volume Name"] = infile.read(self.volPathLength * 2).replace("\x00", "")
            volume["Creation Date"] = self.convertTimestamp(self.volCreationTime)
            volume["Serial Number"] = self.volSerialNumber
            self.volumesInformationArray.append(volume)

            count += 1
            infile.seek(self.volumesInformationOffset + (40 * count))

    def fileInformation23(self, infile):
        # File Information
        # 156 bytes
        self.metricsOffset = struct.unpack_from("I", infile.read(4))[0]
        self.metricsCount = struct.unpack_from("I", infile.read(4))[0]
        self.traceChainsOffset = struct.unpack_from("I", infile.read(4))[0]
        self.traceChainsCount = struct.unpack_from("I", infile.read(4))[0]
        self.filenameStringsOffset = struct.unpack_from("I", infile.read(4))[0]
        self.filenameStringsSize = struct.unpack_from("I", infile.read(4))[0]
        self.volumesInformationOffset = struct.unpack_from("I", infile.read(4))[0]
        self.volumesCount = struct.unpack_from("I", infile.read(4))[0]
        self.volumesInformationSize = struct.unpack_from("I", infile.read(4))[0]
        unknown0 = infile.read(8)
        self.lastRunTime = infile.read(8)
        unknown1 = infile.read(16)
        self.runCount = struct.unpack_from("I", infile.read(4))[0]
        unknown2 = infile.read(84)

    def metricsArray23(self, infile):
        # File Metrics Array
        # 32 bytes per array, not parsed in this script
        infile.seek(self.metricsOffset)
        unknown0 = infile.read(4)
        unknown1 = infile.read(4)
        unknown2 = infile.read(4)
        self.filenameOffset = struct.unpack_from("I", infile.read(4))[0]
        self.filenameLength = struct.unpack_from("I", infile.read(4))[0]
        unknown3 = infile.read(4)
        self.mftRecordNumber = self.convertFileReference(infile.read(6))
        self.mftSeqNumber = struct.unpack_from("H", infile.read(2))[0]

    def volumeInformation23(self, infile):
        # This function consumes the Volume Information array
        # 104 bytes per structure in the array
        # Returns a dictionary object which holds another dictionary
        # for each volume information array entry

        infile.seek(self.volumesInformationOffset)
        self.volumesInformationArray = []
        self.directoryStringsArray = []

        count = 0
        while count < self.volumesCount:
            self.volPathOffset = struct.unpack_from("I", infile.read(4))[0]
            self.volPathLength = struct.unpack_from("I", infile.read(4))[0]
            self.volCreationTime = struct.unpack_from("Q", infile.read(8))[0]
            volSerialNumber = hex(struct.unpack_from("I", infile.read(4))[0])
            self.volSerialNumber = volSerialNumber.rstrip("L").lstrip("0x")
            self.fileRefOffset = struct.unpack_from("I", infile.read(4))[0]
            self.fileRefCount = struct.unpack_from("I", infile.read(4))[0]
            self.dirStringsOffset = struct.unpack_from("I", infile.read(4))[0]
            self.dirStringsCount = struct.unpack_from("I", infile.read(4))[0]
            unknown0 = infile.read(68)

            self.directoryStringsArray.append(self.directoryStrings(infile))

            infile.seek(self.volumesInformationOffset + self.volPathOffset)
            volume = {}
            volume["Volume Name"] = infile.read(self.volPathLength * 2).replace("\x00", "")
            volume["Creation Date"] = self.convertTimestamp(self.volCreationTime)
            volume["Serial Number"] = self.volSerialNumber
            self.volumesInformationArray.append(volume)

            count += 1
            infile.seek(self.volumesInformationOffset + (104 * count))

    def fileInformation26(self, infile):
        # File Information
        # 224 bytes
        self.metricsOffset = struct.unpack_from("I", infile.read(4))[0]
        self.metricsCount = struct.unpack_from("I", infile.read(4))[0]
        self.traceChainsOffset = struct.unpack_from("I", infile.read(4))[0]
        self.traceChainsCount = struct.unpack_from("I", infile.read(4))[0]
        self.filenameStringsOffset = struct.unpack_from("I", infile.read(4))[0]
        self.filenameStringsSize = struct.unpack_from("I", infile.read(4))[0]
        self.volumesInformationOffset = struct.unpack_from("I", infile.read(4))[0]
        self.volumesCount = struct.unpack_from("I", infile.read(4))[0]
        self.volumesInformationSize = struct.unpack_from("I", infile.read(4))[0]
        unknown0 = infile.read(8)
        self.lastRunTime = infile.read(64)
        unknown1 = infile.read(16)
        self.runCount = struct.unpack_from("I", infile.read(4))[0]
        unknown2 = infile.read(96)

    def traceChainsArray30(self, infile):
        # Trace Chains Array
        # Read though, not being parsed for information
        # 8 bytes
        infile.read(8)

    def volumeInformation30(self, infile):
        # Volumes Information
        # 96 bytes

        infile.seek(self.volumesInformationOffset)
        self.volumesInformationArray = []
        self.directoryStringsArray = []

        count = 0
        while count < self.volumesCount:
            self.volPathOffset = struct.unpack_from("I", infile.read(4))[0]
            self.volPathLength = struct.unpack_from("I", infile.read(4))[0]
            self.volCreationTime = struct.unpack_from("Q", infile.read(8))[0]
            self.volSerialNumber = hex(struct.unpack_from("I", infile.read(4))[0])
            self.volSerialNumber = self.volSerialNumber.rstrip("L").lstrip("0x")
            self.fileRefOffset = struct.unpack_from("I", infile.read(4))[0]
            self.fileRefCount = struct.unpack_from("I", infile.read(4))[0]
            self.dirStringsOffset = struct.unpack_from("I", infile.read(4))[0]
            self.dirStringsCount = struct.unpack_from("I", infile.read(4))[0]
            unknown0 = infile.read(60)

            self.directoryStringsArray.append(self.directoryStrings(infile))

            infile.seek(self.volumesInformationOffset + self.volPathOffset)
            volume = {}
            volume["Volume Name"] = infile.read(self.volPathLength * 2).replace("\x00", "")
            volume["Creation Date"] = self.convertTimestamp(self.volCreationTime)
            volume["Serial Number"] = self.volSerialNumber
            self.volumesInformationArray.append(volume)

            count += 1
            infile.seek(self.volumesInformationOffset + (96 * count))

    def getFilenameStrings(self, infile):
        # Parses filename strings from the PF file
        self.resources = []
        infile.seek(self.filenameStringsOffset)
        self.filenames = infile.read(self.filenameStringsSize)

        for i in self.filenames.split("\x00\x00"):
            self.resources.append(i.replace("\x00", ""))

    def convertTimestamp(self, timestamp):
        # Timestamp is a Win32 FILETIME value
        # This function returns that value in a human-readable format
        return str(datetime(1601, 1, 1) + timedelta(microseconds=timestamp / 10.))

    def getTimeStamps(self, lastRunTime):
        self.timestamps = []

        start = 0
        end = 8
        while end <= len(lastRunTime):
            timestamp = struct.unpack_from("Q", lastRunTime[start:end])[0]

            if timestamp:
                self.timestamps.append(self.convertTimestamp(timestamp))
                start += 8
                end += 8
            else:
                break

    def directoryStrings(self, infile):
        infile.seek(self.volumesInformationOffset)
        infile.seek(self.dirStringsOffset, 1)

        directoryStrings = []

        count = 0
        while count < self.dirStringsCount:
            stringLength = struct.unpack_from("<H", infile.read(2))[0] * 2
            directoryString = infile.read(stringLength).replace("\x00", "")
            infile.read(2)  # Read through the end-of-string null byte
            directoryStrings.append(directoryString)
            count += 1

        return directoryStrings

    def convertFileReference(self, buf):
        byteArray = map(lambda x: '%02x' % ord(x), buf)

        byteString = ""
        for i in byteArray[::-1]:
            byteString += i

        return int(byteString, 16)

    def prettyPrint(self):
        # Prints important Prefetch data in a structured format
        banner = "=" * (len(ntpath.basename(self.pFileName)) + 2)
        print "\n{0}\n{1}\n{0}\n".format(banner, ntpath.basename(self.pFileName))
        print "Executable Name: {}\n".format(self.executableName)
        print "Run count: {}".format(self.runCount)

        if len(self.timestamps) > 1:
            print "Last Executed:"
            for i in self.timestamps:
                print "    " + i
        else:
            print "Last Executed: {}".format(self.timestamps[0])

        print "\nVolume Information:"
        for i in self.volumesInformationArray:
            print "    Volume Name: " + i["Volume Name"]
            print "    Creation Date: " + i["Creation Date"]
            print "    Serial Number: " + i["Serial Number"]
            print ""

        print "Directory Strings:"
        for volume in self.directoryStringsArray:
            for i in volume:
                print "    " + i
        print ""

        print "Resources loaded:\n"
        count = 1
        for i in self.resources:
            if i:
                if count > 999:
                    print "{}: {}".format(count, i)
                if count > 99:
                    print "{}:  {}".format(count, i)
                elif count > 9:
                    print "{}:   {}".format(count, i)
                else:
                    print "{}:    {}".format(count, i)
            count += 1

        print ""


# The code in the class below was taken and then modified from Francesco
# Picasso's w10pfdecomp.py script. This modification makes two simple changes:
#
#    - Wraps Francesco's logic in a Python class
#    - Returns a bytearray of uncompressed data instead of writing it to a new
#      file, like Francesco's original code did
#
# Author's name: Francesco "dfirfpi" Picasso
# Author's email: francesco.picasso@gmail.com
# Source: https://github.com/dfirfpi/hotoloti/blob/master/sas/w10pfdecomp.py
# License: http://www.apache.org/licenses/LICENSE-2.0

# Windows-only utility to decompress MAM compressed files
class DecompressWin10(object):
    def __init__(self):
        pass

    def tohex(self, val, nbits):
        """Utility to convert (signed) integer to hex."""
        return hex((val + (1 << nbits)) % (1 << nbits))

    def decompress(self, infile):
        """Utility core."""

        NULL = ctypes.POINTER(ctypes.c_uint)()
        SIZE_T = ctypes.c_uint
        DWORD = ctypes.c_uint32
        USHORT = ctypes.c_uint16
        UCHAR = ctypes.c_ubyte
        ULONG = ctypes.c_uint32

        # You must have at least Windows 8, or it should fail.
        try:
            RtlDecompressBufferEx = ctypes.windll.ntdll.RtlDecompressBufferEx
        except AttributeError, e:
            sys.exit("[ - ] {}".format(e) + \
                     "\n[ - ] Windows 8+ required for this script to decompress Win10 Prefetch files")

        RtlGetCompressionWorkSpaceSize = \
            ctypes.windll.ntdll.RtlGetCompressionWorkSpaceSize

        with open(infile, 'rb') as fin:
            header = fin.read(8)
            compressed = fin.read()

            signature, decompressed_size = struct.unpack('<LL', header)
            calgo = (signature & 0x0F000000) >> 24
            crcck = (signature & 0xF0000000) >> 28
            magic = signature & 0x00FFFFFF
            if magic != 0x004d414d:
                sys.exit('Wrong signature... wrong file?')

            if crcck:
                # I could have used RtlComputeCrc32.
                file_crc = struct.unpack('<L', compressed[:4])[0]
                crc = binascii.crc32(header)
                crc = binascii.crc32(struct.pack('<L', 0), crc)
                compressed = compressed[4:]
                crc = binascii.crc32(compressed, crc)
                if crc != file_crc:
                    sys.exit('{} Wrong file CRC {0:x} - {1:x}!'.format(infile, crc, file_crc))

            compressed_size = len(compressed)

            ntCompressBufferWorkSpaceSize = ULONG()
            ntCompressFragmentWorkSpaceSize = ULONG()

            ntstatus = RtlGetCompressionWorkSpaceSize(USHORT(calgo),
                                                      ctypes.byref(ntCompressBufferWorkSpaceSize),
                                                      ctypes.byref(ntCompressFragmentWorkSpaceSize))

            if ntstatus:
                sys.exit('Cannot get workspace size, err: {}'.format(
                    self.tohex(ntstatus, 32)))

            ntCompressed = (UCHAR * compressed_size).from_buffer_copy(compressed)
            ntDecompressed = (UCHAR * decompressed_size)()
            ntFinalUncompressedSize = ULONG()
            ntWorkspace = (UCHAR * ntCompressFragmentWorkSpaceSize.value)()

            ntstatus = RtlDecompressBufferEx(
                USHORT(calgo),
                ctypes.byref(ntDecompressed),
                ULONG(decompressed_size),
                ctypes.byref(ntCompressed),
                ULONG(compressed_size),
                ctypes.byref(ntFinalUncompressedSize),
                ctypes.byref(ntWorkspace))

            if ntstatus:
                sys.exit('Decompression failed, err: {}'.format(ntstatus))

            if ntFinalUncompressedSize.value != decompressed_size:
                sys.exit('Decompressed with a different size than original!')

        return bytearray(ntDecompressed)


def stripped(s):  # strip bad chars
    return "".join(i for i in s if 9 < ord(i) < 127)


def color_blue():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 1)
        return ""
    else:
        return '\033[0;34m'


def color_green():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 2)
        return ""
    else:
        return '\033[0;32m'


def color_cyan():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 3)
        return ""
    else:
        return '\033[0;36m'


def color_red():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 4)
        return ""
    else:
        return '\033[0;31m'


def color_reset():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 7)
        return ""
    else:
        return '\033[0;39m'


def do_data_file_parsing(config, file_contents, parser_type, computer_json_dict, asset_name):
    global args
    first_cap_re = re.compile('(.)([A-Z][a-z]+)')  # Camel Case to Snake Case
    all_cap_re = re.compile('([a-z0-9])([A-Z])')  # Camel Case to Snake Case
    file_contents = stripped(file_contents)  # Remove Bad Chars - shit happens
    if args.debug: print "\t- do_data_file_parsing for: " + parser_type
    if config["File Parsers"][parser_type] is None:
        return computer_json_dict
    for this_parser in config["File Parsers"][parser_type]:
        if this_parser["Type"] == "regex_table":
            if args.debug: print "\t\t- Found Parser Type: " + this_parser["Type"]
            matches = re.finditer(this_parser["Regex"], file_contents, re.MULTILINE)
            if not all(False for _ in matches):
                computer_json_dict[this_parser["Index Name"]] = []

            for matchNum, match in enumerate(matches, start=1):
                json_table_row = {}
                for group_num in range(0, len(match.groups())):
                    group_num = group_num + 1
                    json_table_row[this_parser["JSON Fields"][group_num - 1]] = match.group(group_num).strip()
                computer_json_dict[this_parser["Index Name"]].append(json_table_row)
        elif this_parser["Type"] == "regex_key_value_pairs":
            if args.debug: print "\t\t- Found Parser Type: " + this_parser["Type"]
            findall = re.findall(this_parser["Regex"], file_contents, re.MULTILINE)
            json_key_value = {}
            if len(findall) > 0:
                computer_json_dict[this_parser["Index Name"]] = []
            for match in findall:
                new_key_name = first_cap_re.sub(r'\1_\2', match[0].strip())  # convert camel case to snake case
                new_key_name = all_cap_re.sub(r'\1_\2', new_key_name).lower()
                json_key_value[new_key_name.replace(" ", "_").replace("__","_")] = match[1].strip()
            if len(json_key_value) > 0:
                computer_json_dict[this_parser["Index Name"]].append(json_key_value)
        elif this_parser["Type"] == "regex_key_value_pairs_table":
            if args.debug: print "\t\t- Found Parser Type: " + this_parser["Type"]
            findall = re.findall(this_parser["Regex"], file_contents, re.MULTILINE)
            if len(findall) > 0:
                computer_json_dict[this_parser["Index Name"]] = []
            for match in findall:
                json_key_value = {
                    this_parser["Key Field Name"]: match[0].strip(),
                    this_parser["Value Field Name"]: match[1].strip(),
                }
                computer_json_dict[this_parser["Index Name"]].append(json_key_value)
        elif this_parser["Type"] == "regex_array_of_dictionary_lines":
            if args.debug: print "\t\t- Found Parser Type: " + this_parser["Type"]
            computer_json_dict[this_parser["Index Name"]] = []
            this_title = ""
            last_title = ""
            first_split = True
            list_counter = 0
            last_field_name_value = ""
            json_key_value = {}
            each_line_key_val_split = re.compile(this_parser["Regex Each Line Split"])
            file_content_lines = file_contents.split("\n")
            for file_line in file_content_lines:
                if "Title Regex" in this_parser:
                    find_title_name_matches = re.findall(this_parser["Title Regex"], file_line)
                    if not all(False for _ in find_title_name_matches):
                        last_title = this_title
                        this_title = find_title_name_matches[0]
                if "List Item Regex" in this_parser:
                    find_list_item_matches = re.findall(this_parser["List Item Regex"], file_line)
                    if not all(False for _ in find_list_item_matches):
                        list_counter += 1
                        json_key_value[last_field_name_value + "_" + str(list_counter)] = find_list_item_matches[0]
                find_new_entry_matches = re.findall(this_parser["Array Split By"], file_line)
                if not all(False for _ in find_new_entry_matches):
                    if not first_split:  # dont roll over until we have read all the keys from the first value
                        if "Title Regex" in this_parser:
                            json_key_value[this_parser["Title Field Name"]] = last_title
                        computer_json_dict[this_parser["Index Name"]].append(json_key_value)
                        json_key_value = {}
                    else:
                        if "Title Regex" in this_parser:
                            json_key_value[this_parser["Title Field Name"]] = last_title
                        first_split = False
                key_val = each_line_key_val_split.split(file_line)
                if len(key_val) == 2:
                    new_key_name = first_cap_re.sub(r'\1_\2', key_val[0].strip())  # convert camel case to snake case
                    new_key_name = all_cap_re.sub(r'\1_\2', new_key_name).lower()
                    last_field_name_value = new_key_name.replace(" ", "_").replace("__", "_").replace("-", "_")
                    json_key_value[last_field_name_value] = key_val[1].strip()
                    list_counter = 1
            if "Title Regex" in this_parser:
                if "Title Regex" in this_parser:
                    json_key_value[this_parser["Title Field Name"]] = this_title
            computer_json_dict[this_parser["Index Name"]].append(json_key_value) # add last entry
        elif this_parser["Type"] == "regex_list":
            if args.debug: print "\t\t- Found Parser Type: " + this_parser["Type"]
            findall = re.findall(this_parser["Regex"], file_contents, re.MULTILINE)
            if len(findall) > 0:
                computer_json_dict[this_parser["Index Name"]] = []
            for match in findall:
                if isinstance(match, tuple):
                    json_key_value = {this_parser["Key"]: match[0].strip()}
                else:
                    json_key_value = {this_parser["Key"]: match.strip()}
                computer_json_dict[this_parser["Index Name"]].append(json_key_value)
        elif this_parser["Type"] == "regex_finding":
            if args.debug: print "\t\t- Found Parser Type: " + this_parser["Type"]
            matches = re.finditer(this_parser["Regex"], file_contents, re.MULTILINE)
            if not all(False for _ in matches):
                if args.debug: print "\t\t\t- Finding Detected: " + this_parser["Index Name"]
                if not computer_json_dict.has_key("Findings"):
                    computer_json_dict[this_parser["Index Name"]] = []
                json_finding = {
                    "name": this_parser["Finding Name"],
                    "description": this_parser["Finding Description"]
                }
                computer_json_dict[this_parser["Index Name"]].append(json_finding)
        elif this_parser["Type"] == "file_as_document":
            if args.debug: print "\t\t- Found Parser Type: " + this_parser["Type"]
            file_content_lines_array = file_contents.split("\n")
            field_name = this_parser["Index Name"]
            if "Field Name" in this_parser:
                field_name = this_parser["Field Name"]
            if len(file_content_lines_array) > 1:
                computer_json_dict[this_parser["Index Name"]] = []
                computer_json_dict[this_parser["Index Name"]].append({field_name: file_contents})
        elif this_parser["Type"] == "file_as_multiple_documents":
            if args.debug: print "\t\t- Found Parser Type: " + this_parser["Type"]
            file_content_lines_array = file_contents.split("\n")
            #       Document Title Regex: '-\"(.+)\"-'
            #       Title Field Name: file
            #       Content Field Name: contents
            computer_json_dict[this_parser["Index Name"]] = []
            this_document_title = ""
            this_document_content = ""
            for file_line in file_contents.split("\n"):
                find_table_matches = re.findall(this_parser["Document Title Regex"], file_line)
                if len(find_table_matches) > 0:
                    if this_document_title != "":
                        computer_json_dict[this_parser["Index Name"]].append(
                            {
                                this_parser["Title Field Name"]: this_document_title,
                                this_parser["Content Field Name"]: this_document_content
                             }
                        )
                        this_document_content = ""
                    this_document_title = find_table_matches[0]
                else:
                    this_document_content += file_line + "\n"
            computer_json_dict[this_parser["Index Name"]].append(
                {
                    this_parser["Title Field Name"]: this_document_title,
                    this_parser["Content Field Name"]: this_document_content
                }
            )
        elif this_parser["Type"] == "csv":
            if args.debug: print "\t\t- Found Parser Type: " + this_parser["Type"]
            computer_json_dict[this_parser["Index Name"]] = []
            for csv_line in file_contents.split("\n"):
                if csv_line == "":
                    continue
                matches = re.findall(r'(?:^|,)(?=[^"]|(")?)"?((?(1)[^"]*|[^,"]*))"?(?=,|$)', csv_line, re.MULTILINE)
                json_table_row = {}
                group_num = 0
                for matchNum, match in enumerate(matches, start=1):
                    group_num = group_num + 1
                    json_table_row[this_parser["JSON Fields"][group_num - 1]] = match[1].strip()
                computer_json_dict[this_parser["Index Name"]].append(json_table_row)
        elif this_parser["Type"] == "regex_associative_table":
            if args.debug: print "\t\t- Found Parser Type: " + this_parser["Type"]
            table_header_after_content = True
            if this_parser["Table Header Location"].lower() == 'before':
                table_header_after_content = False
            table_name = ""
            last_table_name = ""
            additional_info = ""
            last_additional_info = ""
            computer_json_dict[this_parser["Index Name"]] = []
            json_table_row = {}
            this_associative_table_list = []
            do_roll_over = False
            for file_line in file_contents.split("\n"):
                find_table_matches = re.findall(this_parser["Table Entries Regex"], file_line)
                find_table_name_matches = re.findall(this_parser["Table Name Regex"], file_line)
                find_additional_info_matches = []
                if "Additional Info Regex" in this_parser:
                    find_additional_info_matches = re.findall(this_parser["Additional Info Regex"], file_line)
                if not all(False for _ in find_table_matches):
                    if args.debug: print "found TABLE Match" + file_line
                    group_num = 0
                    for matchNum, match in enumerate(find_table_matches, start=1):
                        for data_value in match:
                            json_field_name = this_parser["JSON Fields"][group_num]
                            json_table_row[json_field_name] = data_value.strip()
                            group_num = group_num + 1
                    this_associative_table_list.append(json_table_row)
                    json_table_row = {}
                if not all(False for _ in find_table_name_matches):
                    last_table_name = table_name
                    for matchNum, match in enumerate(find_table_name_matches, start=1):
                        table_name = match.strip()
                    for remove_char in this_parser["Table Name Remove Characters"]:
                        table_name = table_name.replace(remove_char, "")
                    if table_name != "":
                        for table_row in this_associative_table_list:
                            table_row[this_parser["Table Name Label"]] = table_name
                    if args.debug: print "found NAME Match: " + table_name
                    do_roll_over = True
                if not all(False for _ in find_additional_info_matches):
                    last_additional_info = additional_info
                    for matchNum, match in enumerate(find_additional_info_matches, start=1):
                        additional_info = match.strip()
                    if additional_info != "":
                        for table_row in this_associative_table_list:
                            table_row[this_parser["Additional Info Label"]] = additional_info
                    if args.debug: print "found additional INFO Match: " + additional_info
                if do_roll_over:  # do roll-over to next table
                    do_roll_over = False
                    if table_header_after_content == False: # update table values with table name & info
                        for table_row in this_associative_table_list:
                            table_row[this_parser["Additional Info Label"]] = last_additional_info
                        for table_row in this_associative_table_list:
                            table_row[this_parser["Table Name Label"]] = last_table_name
                    for table_entry in this_associative_table_list:
                        computer_json_dict[this_parser["Index Name"]].append(table_entry)
                    this_associative_table_list = []
                    json_table_row = {}
            # End of file - do final update if the table header is BEFORE the table content
            if table_header_after_content == False:  # update table values with table name & info
                for table_row in this_associative_table_list:
                    table_row[this_parser["Additional Info Label"]] = additional_info
                for table_row in this_associative_table_list:
                    table_row[this_parser["Table Name Label"]] = table_name
                for table_entry in this_associative_table_list:
                    computer_json_dict[this_parser["Index Name"]].append(table_entry)
        elif this_parser["Type"] == "variable_whitespace_list":
            if args.debug: print "\t\t- Found Parser Type: " + this_parser["Type"]
            computer_json_dict[this_parser["Index Name"]] = []
            list_header = ""
            list_content = ""
            whitespace_count = 0
            for file_line in file_contents.split("\n"):
                if not file_line.startswith(" "):
                    if list_header != "":  # rotate to next list header
                        left_split, right_split = list_header[:whitespace_count], list_header[whitespace_count:]
                        list_content = right_split.lstrip() + list_content
                        list_header_name = left_split.rstrip()
                        list_json = {
                                        this_parser["JSON Fields"][0]: list_header_name,
                                        this_parser["JSON Fields"][1]: list_content
                                    }
                        computer_json_dict[this_parser["Index Name"]].append(list_json)
                    whitespace_count = 0
                    list_header = file_line
                    list_content = ""
                elif file_line != this_parser["List Separator"]:
                    if whitespace_count == 0:
                        whitespace_count = len(file_line) - len(file_line.lstrip()) # count leading white space
                    list_content = list_content + "\n" + file_line.lstrip()
        elif this_parser["Type"] == "tabular_list":  # for parsing the annoying format that the net command uses
            if args.debug: print "\t\t- Found Parser Type: " + this_parser["Type"]
            computer_json_dict[this_parser["Index Name"]] = []
            start_parsing_table = False  # Gets a bit complex because user names can contain spaces too
            for file_line in file_contents.split("\n"):
                if len(re.findall(this_parser["Start at Regex"], file_line)) > 0:
                    start_parsing_table = True
                elif len(re.findall(this_parser["Stop at Regex"], file_line)) > 0:
                    start_parsing_table = False
                elif start_parsing_table:
                    find_table_matches = re.split(this_parser["Table Entries Split Regex"], file_line)
                    if len(find_table_matches) <= this_parser["Max Entries Per Row"]:
                        for match in find_table_matches:
                            json_table_row = { this_parser["Table Value Field Name"]: match.strip() }
                            computer_json_dict[this_parser["Index Name"]].append(json_table_row)
    return computer_json_dict


# Creates the asset index and returns the unique id created by elastic search which is used to link to other doc types
def do_elastic_search_asset_index(asset_name, config):
    global es
    result = ""
    index_name = ""
    if args.indexprefix == "":
        index_name = config['File Parsers']["Assets"][0]["Index Name"]
    else:
        index_name = str(args.indexprefix).lower() + "_" + config['File Parsers']["Assets"][0]["Index Name"]
    json_data = {
        "asset_name": asset_name,
        "asset_index_date": int(time.time()),
        "asset_id": "",
    }
    try:
        result = es.index(index=index_name, body=json_data)
    except Exception as e:
        print(str(e.info))
        return result

    # add the generated asset_id to the asset index also
    json_data = {
        "doc": {"asset_id": result['_id']}
    }
    try:
        es.update(index=index_name, id=result['_id'], body=json_data)
    except Exception as e:
        print(str(e.info))

    return result['_id']


# Use the Config YAML to generate the Index Mapping JSON
#  - json datatypes will ALWAYS convert the data to the specified data type
#  - optional datatypes will only add the data if the data matches a specific pattern
def do_generate_index_mapping_json(this_parser):
    json_datatypes = {}
    optional_datatypes = {}
    if "JSON Datatypes" in this_parser:
        json_datatypes = this_parser["JSON Datatypes"]
    if "Optional Datatypes" in this_parser:
        optional_datatypes = this_parser["Optional Datatypes"]

    index_mapping = {
                        "settings": {
                            "number_of_shards": 1,
                            "number_of_replicas": 0,
                            "analysis": {
                                "analyzer": {}
                            }
                        },
                        "mappings": {
                            "properties": {
                                "asset_id": {
                                    "type": "keyword"
                                },
                                "asset_name": {
                                    "type": "keyword"
                                },
                                "asset_index_date": {
                                    "type": "date",
                                    "format": "epoch_second"
                                }
                            }
                        },
                    }
    for field in json_datatypes:
        data_type = json_datatypes[field]

        if "date" in data_type:  # parse date
            data_type_format = data_type.split("=")
            index_mapping["mappings"]["properties"][field] = {"type": "date", "format": data_type_format[1]}
        elif "boolean" in data_type:
            index_mapping["mappings"]["properties"][field] = {"type": "boolean"}
        elif "keyword" in data_type:
            index_mapping["mappings"]["properties"][field] = {"type": "keyword"}
        elif "integer" in data_type:
            index_mapping["mappings"]["properties"][field] = {"type": "integer"}
        elif "long" in data_type:
            index_mapping["mappings"]["properties"][field] = {"type": "long"}
        elif "short" in data_type:
            index_mapping["mappings"]["properties"][field] = {"type": "short"}
        elif "ip" in data_type:
            index_mapping["mappings"]["properties"][field] = {"type": "ip"}
        elif "version" in data_type:  # break into -> major.minor[.build[.revision]]
            index_mapping["mappings"]["properties"][field] = {"type": "keyword"}
            index_mapping["mappings"]["properties"][field + "_major"] = {"type": "integer"}
            index_mapping["mappings"]["properties"][field + "_minor"] = {"type": "integer"}
            index_mapping["mappings"]["properties"][field + "_build"] = {"type": "integer"}
            index_mapping["mappings"]["properties"][field + "_revision"] = {"type": "integer"}
        else:  # default - whatever is specified
            index_mapping["mappings"]["properties"][field] = {"type": data_type}

    for field in optional_datatypes:
        data_type = optional_datatypes[field]

        property_field = field + "_" + data_type
        if "date" in data_type:  # parse date
            data_type_format = data_type.split("=")
            index_mapping["mappings"]["properties"][property_field] = {"type": "date", "format": data_type_format[1]}
        elif "boolean" in data_type:
            index_mapping["mappings"]["properties"][property_field] = {"type": "boolean"}
        elif "keyword" in data_type:
            index_mapping["mappings"]["properties"][property_field] = {"type": "keyword"}
        elif "integer" in data_type:
            index_mapping["mappings"]["properties"][property_field] = {"type": "integer"}
        elif "long" in data_type:
            index_mapping["mappings"]["properties"][property_field] = {"type": "long"}
        elif "short" in data_type:
            index_mapping["mappings"]["properties"][property_field] = {"type": "short"}
        elif "ip" in data_type:
            index_mapping["mappings"]["properties"][property_field] = {"type": "ip"}
        elif "version" in data_type:  # break into -> major.minor[.build[.revision]]
            index_mapping["mappings"]["properties"][field + "_major"] = {"type": "integer"}
            index_mapping["mappings"]["properties"][field + "_minor"] = {"type": "integer"}
            index_mapping["mappings"]["properties"][field + "_build"] = {"type": "integer"}
            index_mapping["mappings"]["properties"][field + "_revision"] = {"type": "integer"}
    return index_mapping

# Overwrite JSON String values with converted data types (date, time, int, float etc).
def do_datatype_mapping(this_parser, json_data):
    if this_parser is None:
        return json_data
    # transform all the entry names prior to processing the data types as the data types are referenced in final form
    json_field_name_transformations = {}
    if "JSON Field Name Transformations" in this_parser:
        json_field_name_transformations = this_parser["JSON Field Name Transformations"]
    for entry in json_data:
        for field in entry.keys():
            if field in json_field_name_transformations:
                entry[json_field_name_transformations[field]] = entry[field]
                del entry[field]
    # do data type transformation based on updated field names
    json_datatypes = {}
    json_data_regex_filters = {}
    optional_datatypes = {}
    if "JSON Datatypes" in this_parser:
        json_datatypes = this_parser["JSON Datatypes"]
    if "Optional Datatypes" in this_parser:
        optional_datatypes = this_parser["Optional Datatypes"]
    if "JSON Data Regex Filters" in this_parser:
        json_data_regex_filters = this_parser["JSON Data Regex Filters"]

    for entry in json_data:
        for field in entry.keys():
            if field in json_data_regex_filters:  # filter out any bad chars
                entry[field] = re.sub(json_data_regex_filters[field], "", entry[field])
            if field in json_datatypes:
                data_type = json_datatypes[field]
                if "date" in data_type:  # parse date
                    if "N/A" in entry[field] or "Disabled" in entry[field]:  # Delete bad date values
                        entry[field] = ""  # Blank Entries will be deleted
                elif "boolean" in data_type:
                    if entry[field].lower() == "true":
                        entry[field] = True
                    elif entry[field].lower() == "false":
                        entry[field] = False
                    elif entry[field].lower() == "yes":
                        entry[field] = True
                    elif entry[field].lower() == "no":
                        entry[field] = False
                    elif entry[field].lower() == "on":
                        entry[field] = True
                    elif entry[field].lower() == "off":
                        entry[field] = False
                    elif entry[field].lower() == "enable":
                        entry[field] = True
                    elif entry[field].lower() == "disable":
                        entry[field] = False
                    elif entry[field].lower() == "<dir>":
                        entry[field] = True
                    elif entry[field] == "1":
                        entry[field] = True
                    elif entry[field] == "0":
                        entry[field] = False
                    else:
                        entry[field] = ""
                elif "ip" in data_type:
                    if entry[field].lower() == "on-link" or entry[field].lower() == "default":
                        entry[field] = ""
                elif "version" in data_type:
                    version_numbers = re.findall(r'([\d+\.]+)', entry[field])
                    if len(version_numbers) > 0:
                        version_info = version_numbers[0].split(".")
                        if len(version_info) > 0:
                            entry[field + "_major"] = version_info[0]
                        if len(version_info) > 1:
                            entry[field + "_minor"] = version_info[1]
                        if len(version_info) > 2:
                            entry[field + "_build"] = version_info[2]
                        if len(version_info) > 3:
                            entry[field + "_revision"] = version_info[3]
                elif "integer" in data_type or "long" in data_type or "short" in data_type:
                    entry[field] = re.sub('[^0-9]', '', entry[field])  # remove non-numeric chars
    # parse Optional Data types that only are added if the value LOOKS like that datatype
    for entry in json_data:
        for field in entry.keys():
            if field in json_data_regex_filters:  # filter out any bad chars
                entry[field] = re.sub(json_data_regex_filters[field], "", entry[field])
            if field in optional_datatypes:
                data_type = optional_datatypes[field]
                if "date" in data_type and re.match(r'\d+\/\d+\/\d+', entry[field]):  # parse date
                    entry[field + "_" + data_type] = entry[field]
                elif "boolean" in data_type and re.match(r'(true|false|yes|no|<dir>|1|0)', entry[field].lower() ):
                    if entry[field].lower() == "true":
                        entry[field + "_" + data_type] = True
                    elif entry[field].lower() == "false":
                        entry[field + "_" + data_type] = False
                    elif entry[field].lower() == "yes":
                        entry[field + "_" + data_type] = True
                    elif entry[field].lower() == "no":
                        entry[field + "_" + data_type] = False
                    elif entry[field].lower() == "on":
                        entry[field + "_" + data_type] = True
                    elif entry[field].lower() == "off":
                        entry[field + "_" + data_type] = False
                    elif entry[field].lower() == "enable":
                        entry[field + "_" + data_type] = True
                    elif entry[field].lower() == "disable":
                        entry[field + "_" + data_type] = False
                    elif entry[field].lower() == "<dir>":
                        entry[field + "_" + data_type] = True
                    elif entry[field] == "1":
                        entry[field + "_" + data_type] = True
                    elif entry[field] == "0":
                        entry[field + "_" + data_type] = False
                elif "ip" in data_type and re.match(r'([0-9a-g]{1,4}(\:|\.)+[0-9a-g]{1,4}(\:|\.)+[0-9a-g]{1,4}(\:|\.)+[0-9a-g]{0,4}[^\s]+)', entry[field].lower()):
                    entry[field + "_" + data_type] = entry[field]
                elif ("integer" in data_type or "long" in data_type or "short" in data_type) and re.match(r'^[\d\s]+$', entry[field]):
                    entry[field + "_" + data_type] = re.sub('[^0-9]', '', entry[field])  # remove non-numeric chars
        # Do another pass to remove any blank values (we cannot change the dict size during iteration)
        for field in entry.keys():
            if entry[field] == "":  # Best to remove empty values before sending to elastic search
                del entry[field]
    return json_data


def elastic_search_indexing_worker(json_object, index_name, es):
    result = {}
    try:
        result = es.index(index=index_name, body=json_object)
    except Exception as e:
        print "Exception: @ " + index_name + " : " + " = " + json.dumps(json_object, indent=4, sort_keys=True)
        print(str(e))
        if "info" in e: print str(e.info)
        timeout = 10
        for retry_attempt in range(args.retries):
            timeout = timeout + 30
            print "Retry Attempt #" + str(retry_attempt) + " timeout: " + str(timeout) + " seconds"
            try:
                result = es.index(index=index_name, body=json_object, params={'timeout': str(timeout) + "s"})
            except Exception as e:
                print "Retry Exception: @ " + index_name + " : " + " = " + str(e)
            else:
                print "Finally Completed Successfully! " + index_name + " : "
                break
    return result


def elastic_search_indexing_results(result):
    print str(result)


def do_elastic_search_indexing(this_parser, json_data, asset_name, asset_id, args_meta={}):
    global es
    index_name = get_index_name(this_parser)
    #pool = multiprocessing.Pool(processes=args.threads)
    last_result = {}
    for json_object in json_data:
        json_object["asset_name"] = asset_name
        json_object["asset_id"] = asset_id
        json_object["site"] = args.site
        json_object["asset_index_date"] = int(time.time())
        for k, v in args_meta.items():  # Add any user-defined metadata JSON items - as specified in arguments
            json_object[k] = v
        #pool.apply_async(elastic_search_indexing_worker, args=(json_object, index_name, es, ), callback=elastic_search_indexing_results)
        last_result = elastic_search_indexing_worker(this_parser, index_name, es)
    return last_result


# Load an Index Mapping into Elastic Search
def do_load_index_mapping_to_elastic(this_parser, index_mapping):
    index_name = get_index_name(this_parser)
    result = {"success": False}
    try:
        if es.indices.exists(index_name): # delete index if exists
            if args.debug: print "Index Already Exists, skipping index creation: " + index_name
            return {"acknowledged": True}
    except Exception as e:
        print("Check for existing Index Mapping Exception: " + str(e))
    try:
        result = es.indices.create(index=index_name, ignore=400, body=index_mapping)
    except Exception as e:
        print("Index Mapping Exception: " + str(e))
        if "info" in e: print(str(e.info))
        timeout = 10
        for retry_attempt in range(args.retries):
            timeout = timeout + 30
            print "Retry Attempt #" + str(retry_attempt) + " timeout: " + str(timeout) + " seconds"
            try:
                result = es.indices.create(index=index_name, ignore=400, body=index_mapping, params={'timeout': str(timeout)+"s"})
            except Exception as e:
                print "Retry Exception: @ " + index_name + " = " + str(e)
            else:
                print "Finally Completed Successfully! " + index_name
                break
    return result


def get_index_name(this_parser):
    if "Asset Type" in this_parser:
        asset_type = this_parser["Asset Type"] + "_"
    else:
        asset_type = ""

    if args.indexprefix != "":
        index_name = str(args.indexprefix + "_" + asset_type + this_parser["Index Name"]).lower()
    else:
        index_name = str(asset_type + this_parser["Index Name"]).lower()
    return index_name


def prefetch_parser(prefetch_directory_path):
    json_object_list = []
    for i in os.listdir(prefetch_directory_path):
        if i.endswith(".pf"):
            print "Prefetch file found: " + prefetch_directory_path + " - " + i

            if os.path.getsize(os.path.join(prefetch_directory_path, i)):
                try:
                    p = Prefetch(os.path.join(prefetch_directory_path, i))
                    # p.prettyPrint()
                    directory_strings = ""
                    volume_information_strings = ""
                    for directory_list in p.directoryStringsArray:
                        directory_strings = "\n".join(directory_list)
                    for volume_list in p.volumesInformationArray:
                        volume_information_strings = "\n".join(volume_list)
                    resources_strings = "\n".join(p.resources)
                    timestamp_strings = "\n".join(p.timestamps)
                    this_json_object = {
                        "executable_name": p.executableName,
                        "directory_strings": directory_strings,
                        "file_size": p.fileSize,
                        "filename_length": p.filenameLength,
                        "filenames": p.filenames,
                        "hash": p.hash,
                        "pf_file_name": p.pFileName,
                        "resources": resources_strings,
                        "run_count": p.runCount,
                        "signature": p.signature,
                        "timestamps": timestamp_strings,
                        "trace_chains_count": p.traceChainsCount,
                        "trace_chains_offset": p.traceChainsOffset,
                        "version": p.version,
                        "vol_creation_time": p.volCreationTime,
                        "vol_serial_number": p.volSerialNumber,
                        "volumes_count": p.volumesCount,
                        "volumes_information_array": volume_information_strings,
                     }
                    json_object_list.append(this_json_object)
                except Exception, e:
                    print "- Prefetch Parser: {} could not be parsed".format(i)
            else:
                print "- Prefetch Parser: Zero-byte Prefetch file"
    return json_object_list


def get_computer_name_from_filename(report_filename, identifier_string):
    src_str = re.compile(identifier_string, re.IGNORECASE)
    return src_str.sub("", report_filename)


def main():
    global es
    current_computer = ""
    json_computer = {}
    json_computer_blocks = []
    findings = {}
    es = Elasticsearch([{'host': args.elastichost,
                         'port': args.elasticport,
                         'maxsize': args.threads,
                         'retry_on_timeout': True,
                         'max_retries': 10,
                         'timeout': 15
                       }])
    with open("DragosIngesterConfig.yaml", 'r') as stream:
        config = yaml.safe_load(stream)
    # Create Elastic Search Assets Index Mapping - This is used as a join for all indexes
    assets_parser_type = "Assets"
    assets_parser = config['File Parsers'][assets_parser_type][0]
    json_index_mapping = do_generate_index_mapping_json(assets_parser)
    do_load_index_mapping_to_elastic(assets_parser, json_index_mapping)

    # Process User Defined Meta JSON to add to each entry
    # Add metadata
    args_meta = json.loads(args.meta)

    # Ingest Data Dump Folders
    walk_dir = args.path
    walk_dir = os.path.abspath(walk_dir)
    asset_id = ""

    if args.debug: print('walk_dir (absolute) = ' + os.path.abspath(walk_dir))
    for root, subdirs, files in os.walk(walk_dir):
        subdirs[:] = [d for d in subdirs if d not in ignore_folders]  # do not walk ignored folders
        if args.debug: print('--\nroot = ' + root)
        for subdir in subdirs:
            if args.debug: print('\t- subdirectory ' + subdir)
        for filename in files:
            file_path = os.path.join(root, filename)
            if args.debug: print('\t- file %s (full path: %s)' % (filename, file_path))
            # Check if this matches our file type identifiers
            for file_type_id in config['File Type Identifiers']:
                if re.search(file_type_id, filename, re.IGNORECASE):  # run the parsers
                    if args.debug: print('\t- file type identifier hit: ' + file_type_id)
                    asset_name = get_computer_name_from_filename(filename, file_type_id)
                    print "\t- "+asset_name+": "+file_type_id + " @ "+ datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                    if current_computer != asset_name:  # change to next computer
                        if current_computer != "":
                            json_computer_blocks.append(json_computer)  # append this computer
                        json_computer = {}
                        current_computer = asset_name
                        json_computer["computer"] = asset_name
                        # create a new elastic index and grab the asset_id for this asset_name
                        asset_id = do_elastic_search_asset_index(asset_name, config)
                    with open(file_path, 'r') as data_file_handle:
                        assets_parser_type = config['File Type Identifiers'][file_type_id]
                        data_file_contents = ''.join(data_file_handle.readlines())
                        if len(data_file_contents) == 0:
                            print('\t\t- Warning: Data Empty file: ' + filename)
                        json_computer = do_data_file_parsing(config, data_file_contents, assets_parser_type, json_computer, asset_name)
                    # do elastic indexing - supports multiple parser types per file
                    for this_parser in config["File Parsers"][assets_parser_type]:
                        json_data_type_mapped = {}
                        json_index_mapping = do_generate_index_mapping_json(this_parser)
                        do_load_index_mapping_to_elastic(this_parser, json_index_mapping)
                        if this_parser["Index Name"] in json_computer and len(this_parser["Index Name"]) > 0:
                            try:
                                json_data_type_mapped = do_datatype_mapping(this_parser, json_computer[this_parser["Index Name"]])
                            except Exception as e:
                                print(e)
                                if "info" in e: print str(e.info)
                        else:
                            print('\t\t- Warning: No index data returned for ' + filename + " from parser: " + this_parser["Index Name"] )
                        do_elastic_search_indexing(this_parser, json_data_type_mapped, asset_name, asset_id, args_meta)
                    if args.deletefiles:
                        os.remove(file_path)


        # run prefetch parser
        prefetch_files = prefetch_parser(root)
        if len(prefetch_files) > 0:
            json_computer["prefetch_files"] = prefetch_files
            assets_parser_type = "Prefetch File Parser"
            # do elastic indexing - supports multiple parser types per file
            for this_parser in config["File Parsers"][assets_parser_type]:
                json_data_type_mapped = json_computer["prefetch_files"]
                json_index_mapping = do_generate_index_mapping_json(this_parser)
                do_load_index_mapping_to_elastic(this_parser, json_index_mapping)
                do_elastic_search_indexing(this_parser, json_data_type_mapped, asset_name, asset_id, args_meta)


    json_computer_blocks.append(json_computer)  # append last computer
    print color_green() + "FINISHED Indexing Data!"
    if args.path != "" and not os.path.isdir(args.path):
        print color_red() + "ERROR! The Dragos Dump Folder cannot be found: " + args.path
    else:
        print color_blue() + "__________________________________________________________________"
        print "\tDragos Dump Path: " + args.path
        print "\tDragos Dump JSON Database: " + args.json
        with open(args.json, 'w') as json_outfile:
            for json_computer_block in json_computer_blocks:
                if args.debugjson:
                    json.dump(json_computer_block, json_outfile, sort_keys=True, indent=4)
                else:
                    json.dump(json_computer_block, json_outfile)
                json_outfile.write("\n")
        if args.findings:
            print "REPORT FINDINGS:"
            for computer in findings:
                print computer
                for finding in findings[computer]:
                    print "\t" + finding["name"]
                    print "\t" + finding["description"]
        print color_reset()+"DONE!"


if os.name == 'nt':
    std_out_handle = ctypes.windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)
print(color_cyan() + '                   //////////////////////////////////__                    ')
print('         ////////////////////////////////////////////////////////_                        ')
print('    //////////////////      ///////////////////////////                                   ')
print('  ////////               //////////////////////////                                       ')
print('///                  //////////////////////////                                           ')
print('/                 /////////    ////////////                   \\    \\                    ')
print('               ////////      ////////////         \\\\\\\\\\\\\\     \\\\   \\\\          ')
print('             //////         ///////////             /////////////  \\\\\\\\               ')
print('          //////           ///.//////      \\\\\\\\\\\\\\\\\\//////////////////\\\\       ')
print('        ////.            //// /////            /////           ////////\\\\               ')
print('      ///.              ///   ////     \\\\\\\\\\\\\\\\\\//               /////  \\\\\\   ')
print('    ///                 //   ////           ////                   //// .\\\\\\           ')
print('   /                   /     ////     \\\\\\\\\\\\///                   )///////\\\\\\    ')
print(' /                    /     ////        //////                    )////////\\\\\\         ')
print('                            .///   \\\\\\\\\\\\\\////                         //////\\\\  ')
print('                             ////       //////                           /////\\\\        ')
print('                              ////      //////                             ///\\\\\\      ')
print('                               /////     //////                                 \\        ')
print('                                 /////   ///////                                          ')
print('                                   ///////////////                                        ')
print('                                      ///////////////                                     ')
print('                                          ///////////////                                 ')
print('                                              //////////////                              ')
print('                                                  //////////////                          ')
print('                      ______                          ////////////////\\\\\\\\            ')
print('                //////////////////                        ////////////                    ')
print('              ///////////////////////                        /////////////\\\\\\\\\\      ')
print('            ///               ////////                          ///////////               ')
print('           ///                  ////////                          //////////\\\\\\\\      ')
print('           //                     ///////                          //////////\\  \\       ')
print('          //                       ///////                          /////////\\\\         ')
print('          \\\\                        ///////                         //////// \\\\\\     ')
print('          \\\\\\                        ////////                       ///////     \\     ')
print('           \\\\\\                         ///////                     /////               ')
print('            \\\\                         ////////                  /////                  ')
print('              \\\\                        /////////              /////                    ')
print('               \\\\  \\\\                    ////////////////////////                     ')
print('                 \\\\\\\\\\                    ////////////////                           ')
print('               \\\\\\\\\\\\\\\\\\                                                         ')
print(color_red() + "DRAGOS Dump Ingester - V2.1 - Last Updated: September 7, 2019")
parser = argparse.ArgumentParser(description='A simple tool to Ingest Dragos.bat Dump files into JSON ' + color_reset())
parser.add_argument("-site", type=str, default="unspecified",
                    help='Specify the site name to be used as the site index value in elasticsearch (default: %(default)s)')
parser.add_argument("-path", type=str, default=".\test",
                    help='Specify the path to start the recursive folder ingestion (default: %(default)s)')
parser.add_argument("-json", type=str, default="dragosdump.json",
                    help='Specify the path to the JSON file that will be generated (default: %(default)s)')
parser.add_argument("-findings", dest='findings', action='store_true', help='Print a list of findings.')
parser.add_argument("-elastichost", type=str, default="localhost",  help='Elastic host address (default: %(default)s)')
parser.add_argument("-elasticport", type=str, default="9200",  help='Elastic port number (default: %(default)s)')
parser.add_argument("-elasticdeleteindex", action='store_true', help='If an index already exists, delete it')
parser.add_argument("-debug", dest='debug', action='store_true', help='Print debug information.')
parser.add_argument("-debugregex", dest='debugregex', action='store_true', help='Print regex debug information.')
parser.add_argument("-debugjson", dest='debugjson', action='store_true', help='Write JSON in pretty format - '
                                                                              'NOT SUITABLE for importing - ')
parser.add_argument("-meta", help='metadata added to each entry {"source"":"conn.log, "site":"plant A"}', default="{}")
parser.add_argument("-indexprefix", type=str, help='prefix text to add to each index (default: %(default)s)', default="forensic")
parser.add_argument("-retries", type=int, default=2, help='Number of Elasticsearch index retries (default: %(default)s)')
parser.add_argument("-threads", type=int, default=20, help='Number of Elastic Indexing Worker Threads (default: %(default)s)')
parser.add_argument("-deletefiles", dest='deletefiles', action='store_true', help='Delete files after ingest (handy if things '
                                                              'get stopped)')
args = parser.parse_args()


if __name__ == '__main__':
    #multiprocessing.freeze_support()
    main()