import requests
import os

url = "http://localhost:9200"
folder = "mappings"

for filename in os.listdir(folder):
    if filename.endswith(".json"):
        f = open(folder + os.sep + filename)
        print filename
        index_mapping_data = f.read()
        url_index_mapping = url+"/"+filename+"/"+"_mapping"
        response = requests.put(url_index_mapping, index_mapping_data)
