ECHO off
ECHO Dragos Windows Information Gathering V1.8 - August 22, 2019
:: Please run this batch file as the local ADMINISTRATOR
:: Collects: Windows Registry, System Information, File System Information, Software data, Event Logs, Prefetch, Network Information, Startup Information, Config Files, DLL and EXE versions
ECHO Started: %date% %time%
if not exist .\%computername% md .\%computername% 2>&1
if not exist .\%computername%\prefetch md .\%computername%\prefetch 2>&1
if not exist .\%computername%\logs md .\%computername%\logs 2>&1
ECHO NOTE: This script MUST be run as a local administrator
ECHO Starting host information collection
ECHO [          ] 0%%   Complete
msinfo32 /report .\%computername%\%computername%_msinfo.txt >nul 2>&1
winmsd /report .\%computername%\%computername%_legacy_msinfo.txt >nul 2>&1
:: ---------- WINDOWS REGISTRY ---------- 
%SystemRoot%\regedit.exe /E .\%computername%\%computername%_registry.txt 2>&1
ECHO [=         ] 10%%  Complete
:: ---------- SYSTEM  INFORMATION ----------
ver > .\%computername%\%computername%_ver.txt 2>&1
whoami /ALL /FO LIST > .\%computername%\%computername%_whoami.txt 2>&1
systeminfo /FO CSV /NH > .\%computername%\%computername%_system.csv 2>&1
systeminfo > .\%computername%\%computername%_systeminfo.txt 2>&1
net localgroup > .\%computername%\%computername%_system_localgroups.txt 2>&1
net localgroup administrators > .\%computername%\%computername%_system_admins.txt 2>&1
net localgroup Users > .\%computername%\%computername%_system_users.txt 2>&1
net users > .\%computername%\%computername%_net_users.txt 2>&1
net accounts > .\%computername%\%computername%_system_accounts_local.txt 2>&1
net accounts /domain > .\%computername%\%computername%_system_accounts_domain.txt 2>&1
ECHO [==        ] 20%%  Complete
net view > .\%computername%\%computername%_system_network_computers.txt 2>&1
ECHO [===       ] 30%%  Complete
net use > .\%computername%\%computername%_system_network_drives.txt 2>&1
net share > .\%computername%\%computername%_system_shares.txt 2>&1
net session > .\%computername%\%computername%_system_sessions.txt 2>&1
query user > .\%computername%\%computername%_logged_on_users.txt 2>&1
query session > .\%computername%\%computername%_users_sessions.txt 2>&1
query process * > .\%computername%\%computername%_active_processes.txt 2>&1
qwinsta > .\%computername%\%computername%_active_terminal_sessions.txt 2>&1
set > .\%computername%\%computername%_system_env_vars.txt 2>&1
fsutil fsinfo drives > .\%computername%\%computername%_system_drives.txt 2>&1
schtasks /query /fo LIST /v > .\%computername%\%computername%_schtasks.txt 2>&1
DRIVERQUERY /FO CSV /NH /V > .\%computername%\%computername%_drivers.csv 2>&1
DRIVERQUERY /FO CSV /NH /SI > .\%computername%\%computername%_drivers_signed.csv 2>&1
gpresult /V  > .\%computername%\%computername%_group_policy.txt 2>&1
ECHO [====      ] 40%%  Complete
:: ---------- WINDOWS EVENT LOGGING ----------
copy /Y %SystemRoot%\System32\Winevt\Logs .\%computername%\logs >nul 2>&1
copy /Y %SystemRoot%\System32\config\*.evt .\%computername%\logs >nul 2>&1
copy /Y %SystemRoot%\Inetpub\Logs\LogFiles\* .\%computername%\logs >nul 2>&1
copy /Y %SystemRoot%\Prefetch .\%computername%\prefetch >nul 2>&1
copy /Y %SystemRoot%\system32\WindowsPowerShell\v1.0\*.ps1 .\%computername%\ >nul 2>&1
copy /Y %USERPROFILE%\Documents\PowerShell\*.ps1 .\%computername%\ >nul 2>&1
:: ---------- SOFTWARE DATA ----------
tasklist /SVC /FO CSV /NH > .\%computername%\%computername%_software_tasks.csv 2>&1
tasklist /M /FO CSV /NH > .\%computername%\%computername%_software_modules.csv 2>&1
tasklist /APPS /FO CSV /V /NH > .\%computername%\%computername%_software_store_apps.csv 2>&1
net start > .\%computername%\%computername%_services.txt 2>&1
ECHO [=====     ] 50%%  Complete
dir %SystemRoot% /-C /Q /S /TW /A > .\%computername%\%computername%_all_files.txt 2>&1
ECHO [======    ] 60%%  Complete
icacls %SystemRoot% /T > .\%computername%\%computername%_all_files_icacls.txt 2>&1
cacls %SystemRoot% /T /C > .\%computername%\%computername%_all_files_cacls_legacy.txt 2>&1
copy /Y "%ProgramData%\Microsoft\Windows\Start Menu\Programs\Startup\*" .\%computername%\ >nul 2>&1
copy /Y "%AppData%\Microsoft\Windows\Start Menu\Programs\Startup\*" .\%computername%\ >nul 2>&1
copy /Y "%SystemDrive%\Documents and Settings\All Users\Start Menu\Programs\Startup\*" .\%computername%\ >nul 2>&1
copy /Y "%SystemDrive%\WINNT\Profiles\All Users\Start Menu\Programs\Startup\*" .\%computername%\ >nul 2>&1
ECHO [=======   ] 70%%  Complete
FOR /R "C:\Program Files\" %%G IN (*.ini,*.ps1,*.inf,*.conf,*.bat,*.vb,*.json,*.cfg,*.vbs,*.log,*.bps,*.bpt,*.bpp) do ECHO . >> .\%computername%\%computername%_config_files.txt && ECHO -"%%G"- >> .\%computername%\%computername%_config_files.txt && type "%%G" >> .\%computername%\%computername%_config_files.txt 2>&1
FOR /R "C:\Program Files (x86)\" %%G IN (*.ini,*.ps1,*.inf,*.conf,*.bat,*.vb,*.json,*.cfg,*.vbs,*.log,*.bps,*.bpt) do ECHO . >> .\%computername%\%computername%_config_files.txt && ECHO -"%%G"- >> .\%computername%\%computername%_config_files.txt && type "%%G" >> .\%computername%\%computername%_config_files.txt 2>&1
FOR /R "C:\ProgramData\" %%G IN (*.ini,*.ps1,*.inf,*.conf,*.bat,*.vb,*.json,*.cfg,*.vbs,*.log,*.bps,*.bpt) do ECHO . >> .\%computername%\%computername%_config_files.txt && ECHO -"%%G"- >> .\%computername%\%computername%_config_files.txt && type "%%G" >> .\%computername%\%computername%_config_files.txt 2>&1
FOR /R "C:\Inetpub\" %%G IN (*.ini,*.ps1,*.inf,*.conf,*.bat,*.vb,*.json,*.cfg,*.vbs,*.log,*.bps,*.bpt) do ECHO . >> .\%computername%\%computername%_config_files.txt && ECHO -"%%G"- >> .\%computername%\%computername%_config_files.txt && type "%%G" >> .\%computername%\%computername%_config_files.txt 2>&1
cmdkey /list > .\%computername%\%computername%_stored_tokens.txt 2>&1
ECHO [========  ] 80%%  Complete
:: ---------- NETWORK DATA ----------
ipconfig /all > .\%computername%\%computername%_network_ipconfig.txt 2>&1
ipconfig /displaydns > .\%computername%\%computername%_network_dns.txt 2>&1
netstat -abno > .\%computername%\%computername%_network_connections.txt 2>&1
netstat -r > .\%computername%\%computername%_network_routes.txt 2>&1
arp -a > .\%computername%\%computername%_network_arp_tables.txt 2>&1
netsh advfirewall firewall show rule name=all verbose > .\%computername%\%computername%_firewall_rules.txt 2>&1
netsh advfirewall show allprofiles > .\%computername%\%computername%_firewall_profiles.txt 2>&1
netsh firewall show config verbose = ENABLE > .\%computername%\%computername%_firewall_legacy.txt 2>&1
netsh firewall show state verbose = ENABLE > .\%computername%\%computername%_firewall_legacy_state.txt 2>&1
nbtstat -c > .\%computername%\%computername%_network_nbtstat_cache.txt 2>&1
nbtstat -n > .\%computername%\%computername%_network_nbtstat_local.txt 2>&1
netsh wlan show profile > .\%computername%\%computername%_network_wireless_aps.txt 2>&1
nltest /parentdomain > .\%computername%\%computername%_parent_domain.txt 2>&1
nltest /domain_trusts > .\%computername%\%computername%_domain_trusts.txt 2>&1
ECHO [========= ] 90%%  Complete
ping www.google.com > .\%computername%\%computername%_network_ping.txt 2>&1 
ping 8.8.8.8 >> .\%computername%\%computername%_network_ping.txt 2>&1
nslookup google.com 8.8.8.8 > .\%computername%\%computername%_network_external_dns.txt 2>&1
ECHO [==========] 100%% Complete
if exist "%systemroot%\system32\wbem\wmic.exe" (
    ECHO Starting WMIC Data Collection: %date% %time%
    ECHO [          ] 0%%   Complete
	wmic useraccount list full > .\%computername%\%computername%_useraccounts_wmic.txt 2>&1
	wmic cpu get loadpercentage /value > .\%computername%\%computername%_performance_cpu_load.txt 2>&1
	wmic logicaldisk get /value > .\%computername%\%computername%_performance_logical_disk.txt 2>&1
	wmic diskdrive list full  > .\%computername%\%computername%_performance_disk_drive.txt 2>&1
	ECHO [*         ] 10%%  Complete
	wmic /namespace:\\root\wmi PATH MSAcpi_ThermalZoneTemperature get CriticalTripPoint, CurrentTemperature /value > .\%computername%\%computername%_performance_temperature.txt 2>&1
	wmic process list full > .\%computername%\%computername%_process_wmic.txt 2>&1
	wmic bios list full > .\%computername%\%computername%_bios_wmic.txt 2>&1
	wmic dcomapp list full > .\%computername%\%computername%_dcom_wmic.txt 2>&1
	wmic group list full > .\%computername%\%computername%_group_wmic.txt 2>&1
	wmic netlogin list full > .\%computername%\%computername%_netlogin_wmic.txt 2>&1
	ECHO [**        ] 20%%  Complete
	wmic product list full > .\%computername%\%computername%_product_wmic.txt 2>&1
	wmic qfe list full > .\%computername%\%computername%_qfe_wmic.txt 2>&1
	ECHO [***       ] 30%%  Complete
	wmic service list full  > .\%computername%\%computername%_services_wmic.txt 2>&1
	wmic nic list full  > .\%computername%\%computername%_nic_wmic.txt 2>&1
	wmic nicconfig list full  > .\%computername%\%computername%_nicconfig_wmic.txt 2>&1
	ECHO [****      ] 40%%  Complete
	wmic nteventlog list full  > .\%computername%\%computername%_eventlog_wmic.txt 2>&1
	wmic csproduct list full  > .\%computername%\%computername%_csproduct_wmic.txt 2>&1
	ECHO [*****     ] 50%%  Complete
	wmic printer list full  > .\%computername%\%computername%_printer_wmic.txt 2>&1
	wmic startup list full  > .\%computername%\%computername%_startup_wmic.txt 2>&1
	ECHO [******    ] 60%%  Complete
	wmic share list full  > .\%computername%\%computername%_share_wmic.txt 2>&1
	wmic timezone list full  > .\%computername%\%computername%_timezone_wmic.txt 2>&1
	wmic bootconfig list full  > .\%computername%\%computername%_bootconfig_wmic.txt 2>&1
	::WMI Persistence Threat Detection
    wmic /NAMESPACE:"\\root\subscription" PATH __EventFilter  GET /Value > .\%computername%\%computername%_persist_event_filter_wmic.txt 2>&1
    wmic /NAMESPACE:"\\root\subscription" PATH __FilterToConsumerBinding GET /Value > .\%computername%\%computername%_persist_filter_consumer_wmic.txt 2>&1
    wmic /NAMESPACE:"\\root\subscription" PATH CommandLineEventConsumer GET /Value > .\%computername%\%computername%_persist_cmd_line_wmic.txt 2>&1
    wmic /NAMESPACE:"\\root\subscription" PATH ActiveScriptEventConsumer GET /Value > .\%computername%\%computername%_persist_active_script_wmic.txt 2>&1
    wmic /NAMESPACE:"\\root\subscription" PATH ScriptingStandardConsumerSetting GET /Value > .\%computername%\%computername%_persist_scripting_wmic.txt 2>&1
	SETLOCAL ENABLEDELAYEDEXPANSION
	ECHO [*******   ] 70%%  Complete
	FOR /R "C:\Program Files\" %%G IN (*.dll,*.exe) DO (
	set var_path=%%G 
	wmic datafile where Name="!var_path:\=\\!" GET Name,Caption,Description,LastAccessed,LastModified,InstallDate,CreationDate,AccessMask,Filesize,Version /Value >> .\%computername%\%computername%_all_files_properties_brief.txt 2>&1
	)
	ECHO [********  ] 80%%  Complete
	FOR /R "C:\Program Files (x86)\" %%G IN (*.dll,*.exe) DO (
	set var_path=%%G 
	wmic datafile where Name="!var_path:\=\\!" GET Name,Caption,Description,LastAccessed,LastModified,InstallDate,CreationDate,AccessMask,Filesize,Version /Value >> .\%computername%\%computername%_all_files_properties_brief.txt 2>&1
	)
	ECHO [********* ] 90%%  Complete
	FOR /R "C:\ProgramData\" %%G IN (*.dll,*.exe) DO (
	set var_path=%%G 
	wmic datafile where Name="!var_path:\=\\!" GET Name,Caption,Description,LastAccessed,LastModified,InstallDate,CreationDate,AccessMask,Filesize,Version /Value >> .\%computername%\%computername%_all_files_properties_brief.txt 2>&1
	)
	ECHO [**********] 100%% Complete
) ELSE (
ECHO WMIC is not available
)
::ECHO Compressing all collected files into a CAB: .\%computername%.cab
::FOR /R .\%computername%\ %%G IN (*.*) do ECHO "%%G" >> .\%computername%\cabfiles.txt
::makecab /D DiskDirectory1=. /D FolderFileCountThreshold=0 /D FolderSizeThreshold=0 /D CompressionType=LZX /D CompressionMemory=21 /D DiskLabel1=Dragosbat /D CabinetFileCountThreshold=0 /D MaxCabinetSize=0 /D MaxDiskSize=0 /D MaxDiskFileCount=0  /D "CabinetName1=.\%computername%.cab" /f .\%computername%\cabfiles.txt && DEL /F/Q/S ".\%computername%\" > NUL && RMDIR /Q/S ".\%computername%\"
ECHO Please send the following folder to the Dragos assessment team: .\%computername%
ECHO Completed: %date% %time%