import argparse
import traceback
import json
import csv
from elasticsearch import Elasticsearch, helpers

class ManageELK:
    def __init__(self):
        pass

    @staticmethod
    def delete_index(ip=None, index=None):
        es = Elasticsearch([ip])
        es.indices.delete(index=index)

    @staticmethod
    def delete_by_query(ip=None,index=None,query=None):
        es = Elasticsearch([ip])
        es.delete_by_query(index=index, body=query)

    @staticmethod
    def insert_record(ip=None,index=None,data=None,meta={}):
        es = Elasticsearch([ip])
        es.index(index=index, doc_type=index, body=data)

    @staticmethod
    def load_json_file(ip=None,index=None,filename=None,batch_size=500,meta={}):
        es = Elasticsearch([ip])
        data = ""
        with open(filename,"r") as infile:
            data = infile.read()
        data_batch = []
        for json_data in data.split("\n"):
            # Perform any needed data transformations
            if json_data != "":
                try:
                    temp_data = json.loads(json_data)
                    temp_data["_index"] = index
                    temp_data["_type"] = index

                    # Add metadata
                    for k, v in meta.items():
                        temp_data[k] = v
                    data_batch.append(temp_data)

                    #print("Temp data: " + json.dumps(temp_data,indent=2))
                except Exception as e:
                    print("Error with: " + json_data)
                    print(traceback.print_exc())
                # Insert the data into the data batch

            if len(data_batch) > batch_size:
                ManageELK.bulk_to_elasticsearch(es, data_batch)
                data_batch = []

        # Process the final batch of data
        if len(data_batch) > 0:
            ManageELK.bulk_to_elasticsearch(es, data_batch)

    @staticmethod
    def load_elk_csv_file(ip=None, index=None, filename=None, batch_size=500, meta={}):
        es = Elasticsearch([ip])
        data = ""
        with open(filename) as csvfile:
            snortreader = csv.reader(csvfile)
            data_batch = []
            for row in snortreader:
                try:
                    #print("processing: " + str(row))
                    # Make sure the row is long enough
                    if len(row) >= 27:
                        row_data = {
                            "_index": index,
                            "_type": index,
                            "timestamp": row[0],
                            "sig_generator": row[1],
                            "sig_id": row[2],
                            "sig_rev": row[3],
                            "msg": row[4],
                            "proto": row[5],
                            "src": row[6],
                            "srcport": row[7],
                            "dst": row[8],
                            "dstport": row[9],
                            "ethsrc": row[10],
                            "ethdst": row[11],
                            "ethlen": row[12],
                            "tcpflags": row[13],
                            "tcpseq": row[14],
                            "tcpack": row[15],
                            "tcpln": row[16],
                            "tcpwindow": row[17],
                            "ttl": row[18],
                            "tos": row[19],
                            "id": row[20],
                            "dgmlen": row[21],
                            "iplen": row[22],
                            "icmptype": row[23],
                            "icmpcode": row[24],
                            "icmpid": row[25],
                            "icmpseq": row[26],
                        }

                        # Add metadata
                        for k, v in meta.items():
                            row_data[k] = v
                        data_batch.append(row_data)

                        #print(json.dumps(row_data, indent=2))
                except Exception as e:
                    print("Error with: " + json.dumps(row_data))
                    print(traceback.print_exc())

                if len(data_batch) > batch_size:
                    ManageELK.bulk_to_elasticsearch(es, data_batch)
                    data_batch = []

        # Process the final batch of data
        if len(data_batch) > 0:
            ManageELK.bulk_to_elasticsearch(es, data_batch)

    @staticmethod
    def bulk_to_elasticsearch(es, bulk_queue):
        try:
            helpers.bulk(es, bulk_queue)
            return True
        except:
            print(traceback.print_exc())
            return False

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="manageELK.py: Management utility for ELK")
    subparsers = parser.add_subparsers(help="Commands", dest="option_selected")
    # Create a subcommand to delete an entire index
    parser_delete_index = subparsers.add_parser("delete_index")
    parser_delete_index.add_argument("ip",help="ip address of ELK instance")
    parser_delete_index.add_argument("i",help="index to delete")
    # Delete by query
    parser_delete_by_query = subparsers.add_parser("delete_by_query")
    parser_delete_by_query.add_argument("ip",help="ip address of ELK instance")
    parser_delete_by_query.add_argument("i",help="index to delete data from")
    parser_delete_by_query.add_argument("q", help="delete query")
    # Insert record
    parser_insert_record = subparsers.add_parser("insert_record")
    parser_insert_record.add_argument("ip",help="ip address of ELK instance")
    parser_insert_record.add_argument("i", help="index to insert data into")
    parser_insert_record.add_argument("data", help="data to insert")
    parser_insert_record.add_argument("-meta",help='metadata to associate with data: {"source"":"conn.log, "site":"plant A"}',default="{}")
    # Load json file
    parser_load_json_file = subparsers.add_parser("load_json_file")
    parser_load_json_file.add_argument("ip",help="ip address of ELK instance")
    parser_load_json_file.add_argument("i", help="index to insert data into")
    parser_load_json_file.add_argument("f", help="json file to load into ELK")
    parser_load_json_file.add_argument("-batch_size", help="batch size to load data into ELK", default="500")
    parser_load_json_file.add_argument("-meta",help='metadata to associate with data: {"source"":"conn.log, "site":"plant A"}',default="{}")
    # Load elk csv
    parser_load_elk_csv = subparsers.add_parser("load_elk_csv")
    parser_load_elk_csv.add_argument("ip", help="ip address of ELK instance")
    parser_load_elk_csv.add_argument("i", help="index to insert data into")
    parser_load_elk_csv.add_argument("f", help="elk csv file to load into ELK")
    parser_load_elk_csv.add_argument("-batch_size", help="batch size to load data into ELK", default="500")
    parser_load_elk_csv.add_argument("-meta", help='metadata to associate with data: {"source"":"conn.log, "site":"plant A"}', default="{}")

    args = parser.parse_args()

    if args.option_selected == 'delete_index':
        ManageELK.delete_index(args.ip, args.i)
    elif args.option_selected == "delete_by_query":
        ManageELK.delete_by_query(ip=args.ip, index=args.i, query=args.q)
    elif args.option_selected == "insert_record":
        ManageELK.insert_record(ip=args.ip,index=args.i,data=args.data,meta=json.loads(args.meta))
    elif args.option_selected == "load_json_file":
        ManageELK.load_json_file(ip=args.ip,index=args.i,filename=args.f,batch_size=int(args.batch_size),meta=json.loads(args.meta))
    elif args.option_selected == "load_elk_csv":
        ManageELK.load_elk_csv_file(ip=args.ip,index=args.i,filename=args.f,batch_size=int(args.batch_size),meta=json.loads(args.meta))
