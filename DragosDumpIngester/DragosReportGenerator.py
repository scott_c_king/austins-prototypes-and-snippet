import argparse
import os
import json
import yaml
import ctypes
import re
import sys
import io

# Dragos Report Generator
# Created By: Austin Scott - May 2019
# Loads the output of the Dragos Dump Ingestor and creates handy lists and reports

# Adding support for color in a windows console window is hard
force_linux_color_codes = False
STD_OUTPUT_HANDLE = -11
if os.name == 'nt':
    std_out_handle = ctypes.windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)


def color_blue():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 1)
        return ""
    else:
        return '\033[0;34m'


def color_green():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 2)
        return ""
    else:
        return '\033[0;32m'


def color_cyan():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 3)
        return ""
    else:
        return '\033[0;36m'


def color_red():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 4)
        return ""
    else:
        return '\033[0;31m'


def color_reset():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 7)
        return ""
    else:
        return '\033[0;39m'


print(color_cyan() + '                   //////////////////////////////////__                    ')
print('         ////////////////////////////////////////////////////////_                        ')
print('    //////////////////      ///////////////////////////                                   ')
print('  ////////               //////////////////////////                                       ')
print('///                  //////////////////////////                                           ')
print('/                 /////////    ////////////                   \\    \\                    ')
print('               ////////      ////////////         \\\\\\\\\\\\\\     \\\\   \\\\          ')
print('             //////         ///////////             /////////////  \\\\\\\\               ')
print('          //////           ///.//////      \\\\\\\\\\\\\\\\\\//////////////////\\\\       ')
print('        ////.            //// /////            /////           ////////\\\\               ')
print('      ///.              ///   ////     \\\\\\\\\\\\\\\\\\//               /////  \\\\\\   ')
print('    ///                 //   ////           ////                   //// .\\\\\\           ')
print('   /                   /     ////     \\\\\\\\\\\\///                   )///////\\\\\\    ')
print(' /                    /     ////        //////                    )////////\\\\\\         ')
print('                            .///   \\\\\\\\\\\\\\////                         //////\\\\  ')
print('                             ////       //////                           /////\\\\        ')
print('                              ////      //////                             ///\\\\\\      ')
print('                               /////     //////                                 \\        ')
print('                                 /////   ///////                                          ')
print('                                   ///////////////                                        ')
print('                                      ///////////////                                     ')
print('                                          ///////////////                                 ')
print('                                              //////////////                              ')
print('                                                  //////////////                          ')
print('                      ______                          ////////////////\\\\\\\\            ')
print('                //////////////////                        ////////////                    ')
print('              ///////////////////////                        /////////////\\\\\\\\\\      ')
print('            ///               ////////                          ///////////               ')
print('           ///                  ////////                          //////////\\\\\\\\      ')
print('           //                     ///////                          //////////\\  \\       ')
print('          //                       ///////                          /////////\\\\         ')
print('          \\\\                        ///////                         //////// \\\\\\     ')
print('          \\\\\\                        ////////                       ///////     \\     ')
print('           \\\\\\                         ///////                     /////               ')
print('            \\\\                         ////////                  /////                  ')
print('              \\\\                        /////////              /////                    ')
print('               \\\\  \\\\                    ////////////////////////                     ')
print('                 \\\\\\\\\\                    ////////////////                           ')
print('               \\\\\\\\\\\\\\\\\\                                                         ')
print(color_red() + "DRAGOS Report Generator - V1.0 - Last Updated: May 21rd, 2019")
parser = argparse.ArgumentParser(description='A simple tool to Read the JSON output of the Dragos Ingestor and create reports.' + color_reset())
parser.add_argument("-json", type=str, default="dragosdump.json",
                    help='Specify the path to the JSON file that will be read (default: %(default)s)')
parser.add_argument("-debug", dest='debug', action='store_true', help='Print debug information.')
args = parser.parse_args()
json_data = []
if args.json == "" or not os.path.isfile(args.json):
    print color_red() + "ERROR! The Dragos JSON File cannot be found: " + args.json
else:
    print color_blue() + "__________________________________________________________________"
    print "\tDragos Dump JSON Database: " + args.json
    users = []
    ips = []
    computer_names = []
    computer_name_and_ip = []
    groups = []
    with open(args.json, 'r') as json_file:
        json_lines = json_file.readlines()
        for json_line in json_lines:
            json_data.append(json.loads(json_line))
    for computer in json_data:
        ip_list = []
        users.append(computer['system_workstation_user'])
        computer_names.append(computer['computer'])
        for task in computer['scheduled_tasks']:
            if task.has_key('scheduled_tasks_run_as_user'):
                users.append(task['scheduled_tasks_run_as_user'])
        for ip in computer['system_ip_addresses']:
            ips.append(ip['ip'])
            ip_list.append(ip['ip'])
        computer_name_and_ip.append({computer['computer']: ip_list})
        for group in computer['system_groups']:
            if group.has_key('group'):
                users.append(group['group'])
    users = list(set(users))
    computers = list(set(computer_names))
    groups = list(set(groups))
    ips = list(set(ips))

    with open('users.txt', 'w') as f:
        for user in users:
            f.write("%s\n" % user)
    with open('computers.txt', 'w') as f:
        for computer in computers:
            f.write("%s\n" % computer)
    with open('ips.txt', 'w') as f:
        for ip in ips:
            f.write("%s\n" % ip)
    with open('ips.txt', 'w') as f:
        for ip in ips:
            f.write("%s\n" % ip)
    with open('comp_ips.txt', 'w') as f:
        yaml.dump(computer_name_and_ip, f)

print "DONE!"