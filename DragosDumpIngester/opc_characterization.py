import argparse
from datetime import datetime
import asset_inventory_service as ais
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
import pandas as pd
import yaml
from dragos_data_broker import DragosDataBroker

metrics = {"engine": "Python"}

ddb = DragosDataBroker()
client = Elasticsearch("http://elasticsearch.:9200")

def execute(timerange):

    cna_726_settings = {
        "src_roles": ["OPC Client"],
        "dst_roles": ["OPC Redirection Manager"],
        "dst_hardware": {"vendor": "Honeywell",
                         "model": "Experion PKS"}
    }

    clsids = {
        # https://dragosinc.atlassian.net/browse/CNA-722
        "6031BF75-9CF2-11d1-A97B-00C04FC01389": {
            "src_roles": ["OPC Client"],
            "dst_roles": ["OPC Server"],
            "dst_hardware": {"vendor": "Honeywell",
                             "model": "Experion PKS"}
        },
        
        # https://dragosinc.atlassian.net/browse/CNA-723
        "0336C76F-639E-45f9-963D-84594C792B7E": {
            "src_roles": ["OPC Client"],
            "dst_roles": ["OPC System Performance Server"],
            "dst_hardware": {"vendor": "Honeywell",
                             "model": "Experion PKS"}
        },
        
        # https://dragosinc.atlassian.net/browse/CNA-724
        "8C180EBF-AD1D-413F-8EF1-1B7455A562B1OPC": {
            "src_roles": ["OPC Client"],
            "dst_roles": ["OPC Historian Server"],
            "dst_hardware": {"vendor": "Honeywell",
                             "model": "Experion PKS"}
        },
        
        # https://dragosinc.atlassian.net/browse/CNA-725
        "A02412D8-F447-11D3-8F98-00C04F547784": {
            "src_roles": ["OPC Client"],
            "dst_roles": ["OPC Alarm and Event Server"],
            "dst_hardware": {"vendor": "Honeywell",
                             "model": "Experion PKS"}
        },

        # https://dragosinc.atlassian.net/browse/CNA-726
        "4EBE25D5-FDAC-413d-9618-56D5B70CA7EB": cna_726_settings,
        "F435503B-A8E7-4849-9494-1E698597E46E": cna_726_settings,
        "D5AF57A4-D78C-4a78-9293-7864C391F803": cna_726_settings,
        "0C6EA1EE-5F9F-4ddf-AFB2-C1C75720142A": cna_726_settings,
        "322C0EC1-2CE8-4409-8C98-96EEC8125D3D": cna_726_settings,
        "7EA5E670-EEFB-4dab-8BD0-8A66D97B86EE": cna_726_settings,
        "B281A6CF-6A3C-417f-B5B0-BCF189C1D8B2": cna_726_settings,
        "1F256B21-11F2-4bad-88BA-ADBF7A580B03": cna_726_settings,
        "8BEB4323-67F6-40de-BB8B-D53DC5B93D92": cna_726_settings,
        "E9338532-8134-442d-985A-1A8EF0494D2B": cna_726_settings,
        "DE46A4F1-0034-4372-B8EB-AFE1327D1B11": cna_726_settings,
        "D86451D9-8CFD-4e15-8567-EFF11AC6601A": cna_726_settings,
        "772E410D-229E-4e36-B8C1-704A8C176F58": cna_726_settings,
        "F191ACE9-9C1C-431a-B897-C0832EBB366B": cna_726_settings,
        "0953FC5D-6BA7-4e00-ADDC-73A651C766A6": cna_726_settings,
        "0BCB216C-5C9A-4938-BA9A-E4157BC9EE7D": cna_726_settings,
        "68ABD825-CCEB-43dc-86CA-BEE097F28C60": cna_726_settings,
        "34012350-0790-4385-A307-4CC5DE6E4265": cna_726_settings,
        "37F0AC27-0659-4e51-8D45-BBE07EADFAFE": cna_726_settings,
        "19822E4A-67A6-4fd9-836B-F2513F9050BB": cna_726_settings,

        # https://dragosinc.atlassian.net/browse/CNA-727
        "5C905440-FD5D-4610-A532-D9C8CE4A0393": {
            "src_roles": ["OPC Client"],
            "dst_roles": ["OPC HDC Server"],
            "dst_hardware": {"vendor": "KEPWare",
                             "model": "KEPServerEX"}
        },

        # https://dragosinc.atlassian.net/browse/CNA-728
        "FF9F1A4B-E79A-43E6-B79E-166F5ACC2944": {
            "src_roles": ["OPC Client"],
            "dst_roles": ["OPC AE Server"],
            "dst_hardware": {"vendor": "KEPWare",
                             "model": "KEPServerEX"}
        },

        # https://dragosinc.atlassian.net/browse/CNA-729
        "7BC0CC8E-482C-47CA-ABDC-0FE7F9C6E729": {
            "src_roles": ["OPC Client"],
            "dst_roles": ["OPC DA Server"],
            "dst_hardware": {"vendor": "KEPWare",
                             "model": "KEPServerEX"}
        },

        # https://dragosinc.atlassian.net/browse/CNA-730
        "A9BA2928-195E-11d1-8D41-000000000000": {
            "src_roles": ["OPC Client"],
            "dst_roles": ["OPC DA Server"],
            "dst_hardware": {"vendor": "Siemens",
                             "model": "FactoryLink"}
        },

        # https://dragosinc.atlassian.net/browse/CNA-730
        "DB02E011-F9F9-11d3-917E-0050DA6D9A28": {
            "src_roles": ["OPC Client"],
            "dst_roles": ["OPC DA Development Server"],
            "dst_hardware": {"vendor": "Siemens",
                             "model": "FactoryLink"}
        },

        # https://dragosinc.atlassian.net/browse/CNA-732
        "A05BB6D5-2F8A-11D1-9BB0-080009D01446": {
            "src_roles": ["OPC Client"],
            "dst_roles": ["OPC DA Server"],
            "dst_hardware": {"vendor": "Rockwell",
                             "model": "RSLinx"}
        },

        # https://dragosinc.atlassian.net/browse/CNA-733
        "B01241E8-921B-11d2-B43F-204C4F4F5020": {
            "src_roles": ["OPC Client"],
            "dst_roles": ["OPC DA Server"],
            "dst_hardware": {"vendor": "GE",
                             "model": "CIMplicity"}
        },

        # https://dragosinc.atlassian.net/browse/CNA-735
        "77EE1471-8AE9-11D3-8AF3-00105AA8866A": {
            "src_roles": ["OPC Client"],
            "dst_roles": ["OPC DA Server"],
            "dst_hardware": {"vendor": "GE",
                             "model": "iFIX"}
        },

        # https://dragosinc.atlassian.net/browse/CNA-735
        "3C570292-EB8E-11D4-83A4-00105A984CBD": {
            "src_roles": ["OPC Client"],
            "dst_roles": ["OPC DA Server"],
            "dst_hardware": {"vendor": "GE",
                             "model": "iFIX"}
        },

        # https://dragosinc.atlassian.net/browse/CNA-736
        "AAAA0210-CCCC-0001-CCCC-0080C7AC3298": {
            "src_roles": ["OPC Client"],
            "dst_roles": ["OPC DA Server"],
            "dst_hardware": {"vendor": "Phoenix Contact"}
        },

        # https://dragosinc.atlassian.net/browse/CNA-739
        "19D98A61-B3CA-4143-0088-35464D714EA0": {
            "src_roles": ["OPC Client"],
            "dst_roles": ["OPC DA Server"],
            "dst_hardware": {"vendor": "Aveva",
                             "model": "Wonderware"}
        }
    }
    
    s = Search(using=client)
    s = s.filter("term", type="opcda")
    s = s.filter("exists", field="dst_asset_id")
    s = s.filter("range", ingest_timestamp=timerange)

    hardware_info = {}
    role_info = {}
    for i in s.scan():
        clsid = getattr(i,"isystemactivator_properties_instninfo_clsid", None)
        
        if clsid in clsids:
            for role in clsids[clsid].get("src_roles", []):
                role_info[i.src_asset_id] = {"asset_id": i.src_asset_id,
                                              "role": role,
                                              "confidence": "50",
                                              "source": "OPCDA CLSID",
                                              "raw_data": clsid,
                                              "timestamp": i.timestamp}
                
            for role in clsids[clsid].get("dst_roles", []):
                role_info[i.dst_asset_id] = {"asset_id": i.dst_asset_id,
                                              "role": role,
                                              "confidence": "50",
                                              "source": "OPCDA CLSID",
                                              "raw_data": clsid,
                                              "timestamp": i.timestamp}
                
            if "dst_hardware" in clsids[clsid]:
                hardware = clsids[clsid]["dst_hardware"]
                hardware_info[i.dst_asset_id] = {"asset_id": i.dst_asset_id,
                                                  "vendor": hardware.get("vendor", None),
                                                  "model": hardware.get("model", None),
                                                  "confidence": "50",
                                                  "source": "OPCDA CLSID",
                                                  "raw_data": clsid,
                                                  "timestamp": i.timestamp}
    
        # https://dragosinc.atlassian.net/browse/CNA-714
        # https://dragosinc.atlassian.net/browse/PLATSS-484
        hostname = getattr(i, "isystemactivator_properties_si_ci_name", None)
        if hostname:
            ais.set_asset_attributes(i.dst_asset_id, {"Hostname": hostname})
    
    df = pd.DataFrame(list(hardware_info.values()))
    metrics['hardware_records'] = len(df) 
    if len(df) > 0:
        new = ddb.upsert_pd("qfd_hardware", df, ["asset_id", "source", "raw_data"])
        print("Found " + str(len(df)) + " asset hardware profiles")
        print(str(len(new)) + " were new")
    else:
        print("No Results")
    
        
    df = pd.DataFrame(list(role_info.values()))
    metrics['role_records'] = len(df) 
    
    if len(df) > 0:
        new = ddb.upsert_pd("qfd_roles", df, ["asset_id", "source", "raw_data"])
        
        print("Found " + str(len(df)) + " asset roles")
        print(str(len(new)) + " were new")
    else:
        print("No Results")


if __name__ == "__main__":
    metrics['start_time'] = datetime.now()

    # Parse command line arguments to get the config file... then read the config file as JSON
    parser = argparse.ArgumentParser(description='OPC')
    parser.add_argument('--ingest_start', dest='ingest_start', help='Timestamp of the earliest ingest timestamp to consider in this run', required=False)
    parser.add_argument('--ingest_finish', dest='ingest_finish', help='Timestamp of the earliest ingest timestamp to consider in this run', required=False)
    parser.add_argument('--id', dest='id', help='ID for any notifications generated during this run', required=False)
    args = parser.parse_args()

    execute(timerange = {'gte': args.ingest_start if args.ingest_start else 'now-1h', 'lt': args.ingest_finish if args.ingest_finish else 'now'})

    metrics['analytic_id'] = args.id
    metrics['end_time'] = datetime.now()
    metrics['run_time'] = (metrics['end_time'] - metrics['start_time']).seconds
    metrics['start_time'] = metrics['start_time'].strftime('%Y-%m-%dT%H:%M:%S.000Z')
    metrics['end_time'] = metrics['end_time'].strftime('%Y-%m-%dT%H:%M:%S.000Z')
    metrics_df = pd.DataFrame([metrics])
    ddb.insert_pd('analytic_metrics', metrics_df, create_mapping=False)
