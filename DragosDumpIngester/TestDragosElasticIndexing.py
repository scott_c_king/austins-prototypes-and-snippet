import unittest
import os
import DragosDumpIngester
import yaml
import json

test_folder = "testdata"
asset_id = ""
with open("DragosIngesterConfig.yaml", 'r') as stream:
    config = yaml.safe_load(stream)

class MyTestCase(unittest.TestCase):

    def test_drivers_indexing(self):
        DragosDumpIngester.main()
        asset_name = "testcase"
        with open(test_folder + os.sep + "driverquery.csv") as file:
            file_contents = file.read()
        # config, file_contents, parser_type, computer_json_dict, comp_name:
        indextype = "Drivers CSV"
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_system_csv_indexing(self):
        asset_name = "testcase"
        csvfile ="system.csv"
        indextype = "System CSV"
        with open(test_folder + os.sep + csvfile) as file:
            file_contents = file.read()
        # config, file_contents, parser_type, computer_json_dict, comp_name:
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_connections_indexing(self):
        asset_name = "testcase"
        txtfile ="network_connections.txt"
        indextype = "Network Connections"
        with open(test_folder + os.sep + txtfile) as file:
            file_contents = file.read()
        # config, file_contents, parser_type, computer_json_dict, comp_name:
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_all_files_icacls_indexing(self):
        DragosDumpIngester.main()
        asset_name = "testcase"
        txtfile ="all_files_icacls.txt"
        indextype = "All Files ICACLS"
        with open(test_folder + os.sep + txtfile) as file:
            file_contents = file.read()
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_all_files_indexing(self):
        DragosDumpIngester.main()
        asset_name = "testcase"
        txtfile ="all_files.txt"
        indextype = "All Files"
        with open(test_folder + os.sep + txtfile) as file:
            file_contents = file.read()
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_wmic_process_indexing(self):
        asset_name = "testcase"
        txtfile ="wmic_process.txt"
        indextype = "WMIC Process"
        with open(test_folder + os.sep + txtfile) as file:
            file_contents = file.read()
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_system_info_qb_indexing(self):
        asset_name = "testcase"
        txtfile ="systeminfo.txt"
        indextype = "System Information"
        with open(test_folder + os.sep + txtfile) as file:
            file_contents = file.read()
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]  # QB
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_system_info_ip_indexing(self):
        asset_name = "testcase"
        txtfile ="systeminfo.txt"
        indextype = "System Information"
        with open(test_folder + os.sep + txtfile) as file:
            file_contents = file.read()
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][1]  # IP
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_routing_table_ipv4_indexing(self):
        asset_name = "testcase"
        txtfile ="network_routes.txt"
        indextype = "Network Routes"
        with open(test_folder + os.sep + txtfile) as file:
            file_contents = file.read()
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]  # IPv4
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_routing_table_ipv6_indexing(self):
        asset_name = "testcase"
        txtfile ="network_routes.txt"
        indextype = "Network Routes"
        with open(test_folder + os.sep + txtfile) as file:
            file_contents = file.read()
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][1]  # IPv6
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_arp_table_indexing(self):
        asset_name = "testcase"
        txtfile ="arp_tables.txt"
        indextype = "Network ARP Tables"
        with open(test_folder + os.sep + txtfile) as file:
            file_contents = file.read()
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_firewall_legacy_indexing(self):
        asset_name = "testcase"
        txtfile ="network_firewall_legacy.txt"
        indextype = "Firewall Legacy"
        with open(test_folder + os.sep + txtfile) as file:
            file_contents = file.read()
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_firewall_rules_indexing(self):
        asset_name = "testcase"
        txtfile ="network_firewall_rules.txt"
        indextype = "Firewall Rules"
        with open(test_folder + os.sep + txtfile) as file:
            file_contents = file.read()
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_firewall_profiles_indexing(self):
        asset_name = "testcase"
        txtfile ="network_firewall_profiles.txt"
        indextype = "Firewall Profiles"
        with open(test_folder + os.sep + txtfile) as file:
            file_contents = file.read()
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_network_ip_config_indexing(self):
        asset_name = "testcase"
        txtfile ="network_ipconfig.txt"
        indextype = "Network IP Config"
        with open(test_folder + os.sep + txtfile) as file:
            file_contents = file.read()
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_wmic_product_indexing(self):
        asset_name = "testcase"
        txtfile ="wmic_product.txt"
        indextype = "WMIC Product"
        with open(test_folder + os.sep + txtfile) as file:
            file_contents = file.read()
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_eng_workstation_ipconfig_windows_xp_indexing(self):
        asset_name = "testcase"
        txtfile ="ENG_WORKSTATION_network_ipconfig.txt"
        indextype = "Network IP Config"
        with open(test_folder + os.sep + txtfile) as file:
            file_contents = file.read()
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_eng_workstation_ipconfig_windows_xp_indexing(self):
        asset_name = "WIN-BFE972DMF97"
        txtfile ="PuttySessions.txt"
        indextype = "Windows Registry"
        DragosDumpIngester.args.indexprefix = "lab"
        with open(test_folder + os.sep + txtfile) as file:
            file_contents = file.read()
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_eng_workstation_ipconfig_windows_xp_indexing(self):
        asset_name = "ENG_WORKSTATION"
        txtfile ="registry_always_elevated.txt"
        indextype = "Windows Registry"
        DragosDumpIngester.args.indexprefix = "lab"
        with open(test_folder + os.sep + txtfile) as file:
            file_contents = file.read()
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

if __name__ == '__main__':
    unittest.main()
