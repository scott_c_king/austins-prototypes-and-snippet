:: Dragos Windows Information Gathering V1.2 - May 2019
:: Please run this batch file as the ADMINISTRATOR 
:: Collects: Windows Registry, System Information, Software data, Event Logs, Prefetch, Network Information, Startup Information
if not exist .\%computername% md .\%computername% >nul
if not exist .\%computername%\prefetch md .\%computername%\prefetch >nul
if not exist .\%computername%\logs md .\%computername%\logs >nul
:: ---------- WINDOWS REGISTRY ---------- 
%SystemRoot%\regedit.exe /E .\%computername%\%computername%_reg.txt
:: ---------- SYSTEM  INFORMATION ---------- 
systeminfo /FO CSV /NH > .\%computername%\%computername%_system.csv
systeminfo > .\%computername%\%computername%_systeminfo.txt
net localgroup > .\%computername%\%computername%_system_accounts.txt
net localgroup administrators > .\%computername%\%computername%_system_accounts_admins.txt
net localgroup Users > .\%computername%\%computername%_system_accounts_users.txt
wmic /output:.\%computername%\%computername%_useraccounts.csv useraccount list full /format:csv
net user > .\%computername%\%computername%_system_accounts_this_user.txt
net accounts > .\%computername%\%computername%_system_accounts_local.txt
net accounts /domain > .\%computername%\%computername%_system_accounts_domain.txt
net use > .\%computername%\%computername%_system_use.txt
net share > .\%computername%\%computername%_system_shares.txt
set > .\%computername%\%computername%_system_env_vars.txt
fsutil fsinfo drives > .\%computername%\%computername%_system_drives.txt
schtasks /query /fo LIST /v > .\%computername%\%computername%_schtasks.txt
DRIVERQUERY /FO CSV /NH /V > .\%computername%\%computername%_drivers.csv
DRIVERQUERY /FO CSV /NH /SI > .\%computername%\%computername%_signed_drivers.csv
:: ---------- WINDOWS EVENT LOGGING ----------
copy /Y %SystemRoot%\System32\Winevt\Logs .\%computername%\logs
copy /Y %SystemRoot%\System32\config\*.evt .\%computername%\logs
copy /Y %SystemRoot%\Inetpub\Logs\LogFiles\* .\%computername%\logs
copy /Y %SystemRoot%\Prefetch .\%computername%\prefetch
copy /Y %SystemRoot%\system32\WindowsPowerShell\v1.0\*.ps1 .\%computername%\
copy /Y %USERPROFILE%\Documents\PowerShell\*.ps1 .\%computername%\
:: ---------- SOFTWARE DATA ----------
tasklist /SVC > .\%computername%\%computername%_software_tasks.txt
net start > .\%computername%\%computername%_services.txt
dir /b /s %SystemRoot% >> .\%computername%\%computername%_all_files.txt
type "%SystemDrive%\ProgramData\Microsoft\Windows\Start Menu\Programs\Startup\*" >> .\%computername%\%computername%_startup.txt
type "%SystemDrive%\Users\%UserName%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\*" >> .\%computername%\%computername%_startup.txt
type "%SystemDrive%\Documents and Settings\All Users\Start Menu\Programs\Startup\*" >> .\%computername%\%computername%_startup.txt
type "%SystemDrive%\WINNT\Profiles\All Users\Start Menu\Programs\Startup\*" >> .\%computername%\%computername%_startup.txt
FOR /R "C:\Program Files\" %G IN (*.ini) do echo "%G" .\%computername%\%computername%_ini_files.txt && type "%G" >> .\%computername%\%computername%_ini_files.txt
FOR /R "C:\Program Files (x86)\" %G IN (*.ini) do echo "%G" .\%computername%\%computername%_ini_files.txt && type "%G" >> .\%computername%\%computername%_ini_files.txt
:: ---------- NETWORK DATA ----------
ipconfig /all >> .\%computername%\%computername%_network_ipconfig.txt
netstat -abn >> .\%computername%\%computername%_network.txt
netstat -r >> .\%computername%\%computername%_network.txt
route print >> .\%computername%\%computername%_network.txt
arp -a >> .\%computername%\%computername%_network.txt
netsh firewall show state >> .\%computername%\%computername%_network_firewall_state.txt
netsh advfirewall firewall show rule name=all >> .\%computername%\%computername%_network_firewall_rules.txt
netsh advfirewall show allprofiles >> .\%computername%\%computername%_network_firewall_profiles.txt
ping www.google.com >> .\%computername%\%computername%_network_ping.txt
ping 8.8.8.8 >> .\%computername%\%computername%_network_ping.txt
nslookup google.com 8.8.8.8 >> .\%computername%\%computername%_network_external_dns.txt
echo & echo.*************& echo.*************& echo.WINDOWS INFORMATION GATHERING COMPLETE & echo. - Report Files Saved To .\%computername% & echo.************* & echo.************* 