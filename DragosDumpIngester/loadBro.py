import json
import argparse
import traceback
import pymongo
from collections import OrderedDict
from elasticsearch import Elasticsearch, helpers
from datetime import datetime

import time

class ParseBroLogsIterator(object):
    def __init__(self, filepath, batchsize=500, fields=None):
        self.fd = open(filepath,"r")
        self.options = OrderedDict()
        self.firstRun = True
        self.filtered_fields = fields
        self.batchsize = batchsize

        # Read the header option lines
        l = self.fd.readline()
        while l.strip().startswith("#"):
            #print("line: " + l)
            # Parse the options out
            if l.startswith("#separator"):
                key = str(l[1:].split(" ")[0])
                value = str.encode(l[1:].split(" ")[1].strip()).decode('unicode_escape')
                self.options[key] = value
            elif l.startswith("#"):
                key = str(l[1:].split(self.options.get('separator'))[0])
                value = l[1:].split(self.options.get('separator'))[1:]
                self.options[key] = value

            # Read the next line
            l = self.fd.readline()

        self.firstLine = l

        #print(json.dumps(self.options, indent=2))

    def __del__(self):
        self.fd.close()

    def __iter__(self):
        return self

    def __next__(self):
        retVal = ""
        if self.firstRun is True:
            retVal = self.firstLine
            self.firstRun = False
        else:
            retVal = self.fd.readline()

        # If an empty string is returned, readline is done reading
        if retVal == "":
            raise StopIteration

        return retVal.split(self.options.get('separator'))

    @staticmethod
    def bulk_to_elasticsearch(es, bulk_queue):
        try:
            helpers.bulk(es, bulk_queue)
            return True
        except:
            print(traceback.print_exc())
            return False

    @staticmethod
    def batch_to_elk(filepath=None, batch_size=500, fields=None, elk_ip="127.0.0.1", index="brologs",meta={}, ignore_keys=[]):
        # Create handle to ELK
        es = Elasticsearch([elk_ip])

        # Create a handle to the log data
        dataHandle = ParseBroLogsIterator(filepath, fields=fields)

        # Begin to process and output data
        dataBatch = []
        for record_line in dataHandle:
            # Make sure we aren't dealing with a comment line
            if len(record_line) > 0 and not str(record_line[0]).strip().startswith("#") \
                    and len(record_line) is len(dataHandle.options.get("fields")):
                record = {}
                for x in range(0, len(record_line) - 1):
                    if fields is None or dataHandle.options.get("fields")[x] in dataHandle.self.filtered_fields:
                        # Translate - to "" to fix a conversation error
                        if record_line[x] == "-":
                            record_line[x] = ""
                        # Save the record field
                        record[dataHandle.options.get("fields")[x]] = record_line[x]
                #print("Adding: " + json.dumps(record))
                #print("Pre time: " + str(record['ts']))
                #print("Pre time2: " + str(float(float(record['ts']) / 1000)))
                #record['timestamp'] = datetime.utcfromtimestamp(float(float(record['ts']) / 1000)).isoformat()
                #print("TS: " + str(record["ts"]))
                try:
                    record['timestamp'] = datetime.utcfromtimestamp(float(record['ts'])).isoformat()
                except:
                    pass
                record["_index"] = index
                record["_type"] = index

                # Convert record types
                record = ParseBroLogsIterator.convert_values(record,ignore_keys)

                # Add metadata
                for k, v in meta.items():
                    record[k] = v

                # Add the record to the batch queue
                dataBatch.append(record)

            if len(dataBatch) >= batch_size:
                # Batch the queue to ELK
                #print("Batching to elk: " + str(len(dataBatch)))
                dataHandle.bulk_to_elasticsearch(es, dataBatch)
                # Clear the data queue
                dataBatch = []

        # Batch the final data to ELK
        #print("Batching final data to elk: " + str(len(dataBatch)))
        dataHandle.bulk_to_elasticsearch(es, dataBatch)
        # Clear the data queue
        dataBatch = []

        print(str(len(dataBatch)))

    @staticmethod
    def convert_values(data, ignore_keys=[]):
        for k, v in data.items():
            if isinstance(v, dict):
                data[k] = ParseBroLogsIterator.convert_values(v)
            else:
                if k not in ignore_keys:
                    try:
                        # Try to change retval to an int
                        data[k] = int(v)
                    except Exception as e:
                        data[k] = v
                else:
                    data[k] = v

        return data

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="loadBroLogs: Load bro logs into ELK!")
    parser.add_argument("f",help="Bro log to process")
    parser.add_argument("-fields", help="fields to save", default=None)
    parser.add_argument("ip",help="IP address to batch to")
    parser.add_argument("-i", help="ELK index to push data into")
    parser.add_argument("-batch_size", help="Batch size to load data in", default=500)
    parser.add_argument("-meta",help="Metadata to add to the bro record {'source':'conn.log, 'site':'plant A'}", default="{}")
    parser.add_argument("-ignore", help="Fields to ignore when converting values", default="")
    args = parser.parse_args()

    ParseBroLogsIterator.batch_to_elk(filepath=args.f, batch_size=args.batch_size, fields=args.fields, elk_ip=args.ip, index=args.i, meta=json.loads(args.meta), ignore_keys=args.ignore.split(","))

