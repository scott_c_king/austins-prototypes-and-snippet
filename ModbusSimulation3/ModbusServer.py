#!/usr/bin/env python

# Dragos Modbus Simulator - Based on pymodbus
# https://pymodbus.readthedocs.io/en/latest/readme.html
import os
import ctypes
import argparse
import pickle
from optparse import OptionParser
from twisted.internet import reactor

from pymodbus.server.async import StartTcpServer
from pymodbus.datastore import ModbusServerContext,ModbusSlaveContext

# Adding support for color in a windows console window is hard
force_linux_color_codes = False
STD_OUTPUT_HANDLE = -11

def color_blue():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 1)
        return ""
    else:
        return '\033[0;34m'


def color_green():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 2)
        return ""
    else:
        return '\033[0;32m'


def color_cyan():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 3)
        return ""
    else:
        return '\033[0;36m'


def color_red():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 4)
        return ""
    else:
        return '\033[0;31m'


def color_reset():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 7)
        return ""
    else:
        return '\033[0;39m'

if os.name == 'nt':
    std_out_handle = ctypes.windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)
print(color_cyan() + '                   //////////////////////////////////__                    ')
print('         ////////////////////////////////////////////////////////_                        ')
print('    //////////////////      ///////////////////////////                                   ')
print('  ////////               //////////////////////////                                       ')
print('///                  //////////////////////////                                           ')
print('/                 /////////    ////////////                   \\    \\                    ')
print('               ////////      ////////////         \\\\\\\\\\\\\\     \\\\   \\\\          ')
print('             //////         ///////////             /////////////  \\\\\\\\               ')
print('          //////           ///.//////      \\\\\\\\\\\\\\\\\\//////////////////\\\\       ')
print('        ////.            //// /////            /////           ////////\\\\               ')
print('      ///.              ///   ////     \\\\\\\\\\\\\\\\\\//               /////  \\\\\\   ')
print('    ///                 //   ////           ////                   //// .\\\\\\           ')
print('   /                   /     ////     \\\\\\\\\\\\///                   )///////\\\\\\    ')
print(' /                    /     ////        //////                    )////////\\\\\\         ')
print('                            .///   \\\\\\\\\\\\\\////                         //////\\\\  ')
print('                             ////       //////                           /////\\\\        ')
print('                              ////      //////                             ///\\\\\\      ')
print('                               /////     //////                                 \\        ')
print('                                 /////   ///////                                          ')
print('                                   ///////////////                                        ')
print('                                      ///////////////                                     ')
print('                                          ///////////////                                 ')
print('                                              //////////////                              ')
print('                                                  //////////////                          ')
print('                      ______                          ////////////////\\\\\\\\            ')
print('                //////////////////                        ////////////                    ')
print('              ///////////////////////                        /////////////\\\\\\\\\\      ')
print('            ///               ////////                          ///////////               ')
print('           ///                  ////////                          //////////\\\\\\\\      ')
print('           //                     ///////                          //////////\\  \\       ')
print('          //                       ///////                          /////////\\\\         ')
print('          \\\\                        ///////                         //////// \\\\\\     ')
print('          \\\\\\                        ////////                       ///////     \\     ')
print('           \\\\\\                         ///////                     /////               ')
print('            \\\\                         ////////                  /////                  ')
print('              \\\\                        /////////              /////                    ')
print('               \\\\  \\\\                    ////////////////////////                     ')
print('                 \\\\\\\\\\                    ////////////////                           ')
print('               \\\\\\\\\\\\\\\\\\                                                         ')
print(color_red() + "DRAGOS Modbus Simulation Server - V1.0 - Last Updated: March 6, 2020")
parser = argparse.ArgumentParser(description='A simple Modbus server based on uMobus 1.0.3' + color_reset())
parser.add_argument("-ip", type=str, default="127.0.0.1",
                    help='Specify the ip to listen on (default: %(default)s)')
parser.add_argument("-port", type=str, default="5020",
                    help='Specify the port to listen on for modbus traffic (default: %(default)s)')
args = parser.parse_args()


#--------------------------------------------------------------------------#
# Logging
#--------------------------------------------------------------------------#
import logging
logging.basicConfig()

server_log   = logging.getLogger("pymodbus.server")
protocol_log = logging.getLogger("pymodbus.protocol")

#---------------------------------------------------------------------------#
# Extra Global Functions
#---------------------------------------------------------------------------#
# These are extra helper functions that don't belong in a class
#---------------------------------------------------------------------------#
import getpass
def root_test():
    ''' Simple test to see if we are running as root '''
    return True # removed for the time being as it isn't portable
    #return getpass.getuser() == "root"

#--------------------------------------------------------------------------#
# Helper Classes
#--------------------------------------------------------------------------#
class ConfigurationException(Exception):
    ''' Exception for configuration error '''

    def __init__(self, string):
        ''' Initializes the ConfigurationException instance

        :param string: The message to append to the exception
        '''
        Exception.__init__(self, string)
        self.string = string

    def __str__(self):
        ''' Builds a representation of the object

        :returns: A string representation of the object
        '''
        return 'Configuration Error: %s' % self.string

class Configuration:
    '''
    Class used to parse configuration file and create and modbus
    datastore.

    The format of the configuration file is actually just a
    python pickle, which is a compressed memory dump from
    the scraper.
    '''

    def __init__(self, config):
        '''
        Trys to load a configuration file, lets the file not
        found exception fall through

        :param config: The pickled datastore
        '''
        try:
            self.file = open(config, "r")
        except Exception:
            raise ConfigurationException("File not found %s" % config)

    def parse(self):
        ''' Parses the config file and creates a server context
        '''
        handle = pickle.load(self.file)
        try: # test for existance, or bomb
            dsd = handle['di']
            csd = handle['ci']
            hsd = handle['hr']
            isd = handle['ir']
        except Exception:
            raise ConfigurationException("Invalid Configuration")
        slave = ModbusSlaveContext(d=dsd, c=csd, h=hsd, i=isd)
        return ModbusServerContext(slaves=slave)

#--------------------------------------------------------------------------#
# Main start point
#--------------------------------------------------------------------------#
def main():
    ''' Server launcher '''
    parser = OptionParser()
    parser.add_option("-c", "--conf",
                    help="The configuration file to load",
                    dest="file")
    parser.add_option("-D", "--debug",
                    help="Turn on to enable tracing",
                    action="store_true", dest="debug", default=False)
    (opt, arg) = parser.parse_args()

    # enable debugging information
    if opt.debug:
        try:
            server_log.setLevel(logging.DEBUG)
            protocol_log.setLevel(logging.DEBUG)
        except Exception, e:
    	    print "Logging is not supported on this system"

    # parse configuration file and run
    try:
        conf = Configuration(opt.file)
        StartTcpServer(context=conf.parse())
    except ConfigurationException, err:
        print err
        parser.print_help()

#---------------------------------------------------------------------------#
# Main jumper
#---------------------------------------------------------------------------#
if __name__ == "__main__":
    if root_test():
        main()
    else: print "This script must be run as root!"

