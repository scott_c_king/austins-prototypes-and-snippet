#!/usr/bin/env python

# Dragos Modbus Simulator Client - Based on uModbus 1.03
# https://pypi.org/project/uModbus/
# scripts/examples/simple_tcp_server.py
import os
import ctypes
import argparse
#---------------------------------------------------------------------------#
# import the various server implementations
#---------------------------------------------------------------------------#
from pymodbus.client.async import ModbusTcpClient as ModbusClient
#from pymodbus.client.async import ModbusUdpClient as ModbusClient
#from pymodbus.client.async import ModbusSerialClient as ModbusClient

#---------------------------------------------------------------------------#
# configure the client logging
#---------------------------------------------------------------------------#
import logging
logging.basicConfig()
log = logging.getLogger()
log.setLevel(logging.DEBUG)


# Adding support for color in a windows console window is hard
force_linux_color_codes = False
STD_OUTPUT_HANDLE = -11

def color_blue():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 1)
        return ""
    else:
        return '\033[0;34m'


def color_green():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 2)
        return ""
    else:
        return '\033[0;32m'


def color_cyan():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 3)
        return ""
    else:
        return '\033[0;36m'


def color_red():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 4)
        return ""
    else:
        return '\033[0;31m'


def color_reset():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 7)
        return ""
    else:
        return '\033[0;39m'

if os.name == 'nt':
    std_out_handle = ctypes.windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)
print(color_cyan() + '                   //////////////////////////////////__                    ')
print('         ////////////////////////////////////////////////////////_                        ')
print('    //////////////////      ///////////////////////////                                   ')
print('  ////////               //////////////////////////                                       ')
print('///                  //////////////////////////                                           ')
print('/                 /////////    ////////////                   \\    \\                    ')
print('               ////////      ////////////         \\\\\\\\\\\\\\     \\\\   \\\\          ')
print('             //////         ///////////             /////////////  \\\\\\\\               ')
print('          //////           ///.//////      \\\\\\\\\\\\\\\\\\//////////////////\\\\       ')
print('        ////.            //// /////            /////           ////////\\\\               ')
print('      ///.              ///   ////     \\\\\\\\\\\\\\\\\\//               /////  \\\\\\   ')
print('    ///                 //   ////           ////                   //// .\\\\\\           ')
print('   /                   /     ////     \\\\\\\\\\\\///                   )///////\\\\\\    ')
print(' /                    /     ////        //////                    )////////\\\\\\         ')
print('                            .///   \\\\\\\\\\\\\\////                         //////\\\\  ')
print('                             ////       //////                           /////\\\\        ')
print('                              ////      //////                             ///\\\\\\      ')
print('                               /////     //////                                 \\        ')
print('                                 /////   ///////                                          ')
print('                                   ///////////////                                        ')
print('                                      ///////////////                                     ')
print('                                          ///////////////                                 ')
print('                                              //////////////                              ')
print('                                                  //////////////                          ')
print('                      ______                          ////////////////\\\\\\\\            ')
print('                //////////////////                        ////////////                    ')
print('              ///////////////////////                        /////////////\\\\\\\\\\      ')
print('            ///               ////////                          ///////////               ')
print('           ///                  ////////                          //////////\\\\\\\\      ')
print('           //                     ///////                          //////////\\  \\       ')
print('          //                       ///////                          /////////\\\\         ')
print('          \\\\                        ///////                         //////// \\\\\\     ')
print('          \\\\\\                        ////////                       ///////     \\     ')
print('           \\\\\\                         ///////                     /////               ')
print('            \\\\                         ////////                  /////                  ')
print('              \\\\                        /////////              /////                    ')
print('               \\\\  \\\\                    ////////////////////////                     ')
print('                 \\\\\\\\\\                    ////////////////                           ')
print('               \\\\\\\\\\\\\\\\\\                                                         ')
print(color_red() + "DRAGOS Modbus Simulation Client - V1.0 - Last Updated: March 6, 2020")
parser = argparse.ArgumentParser(description='A simple Modbus client based on uMobus 1.0.3' + color_reset())
parser.add_argument("-ip", type=str, default="127.0.0.1",
                    help='Specify the ip of the Modbus Server to connect with (default: %(default)s)')
parser.add_argument("-port", type=str, default="5020",
                    help='Specify the port of the modbus server to connect with (default: %(default)s)')
args = parser.parse_args()

#!/usr/bin/env python
'''
Pymodbus Asynchrnonous Client Examples
--------------------------------------------------------------------------

The following is an example of how to use the asynchronous modbus
client implementation from pymodbus.
'''


#---------------------------------------------------------------------------#
# choose the client you want
#---------------------------------------------------------------------------#
# make sure to start an implementation to hit against. For this
# you can use an existing device, the reference implementation in the tools
# directory, or start a pymodbus server.
#---------------------------------------------------------------------------#
client = ModbusClient('127.0.0.1')

#---------------------------------------------------------------------------#
# helper method to test deferred callbacks
#---------------------------------------------------------------------------#
def dassert(deferred, callback):
    def _tester():
        assert(callback())
    deferred.callback(_tester)
    deferred.errback(lambda _: assert(False))

#---------------------------------------------------------------------------#
# example requests
#---------------------------------------------------------------------------#
# simply call the methods that you would like to use. An example session
# is displayed below along with some assert checks.
#---------------------------------------------------------------------------#
rq = client.write_coil(1, True)
rr = client.read_coils(1,1)
dassert(rq, lambda r: r.function_code < 0x80)     # test that we are not an error
dassert(rr, lambda r: r.bits[0] == True)          # test the expected value

rq = client.write_coils(1, [True]*8)
rr = client.read_coils(1,8)
dassert(rq, lambda r: r.function_code < 0x80)     # test that we are not an error
dassert(rr, lambda r: r.bits == [True]*8)         # test the expected value

rq = client.write_coils(1, [False]*8)
rr = client.read_discrete_inputs(1,8)
dassert(rq, lambda r: r.function_code < 0x80)     # test that we are not an error
dassert(rr, lambda r: r.bits == [False]*8)        # test the expected value

rq = client.write_register(1, 10)
rr = client.read_holding_registers(1,1)
dassert(rq, lambda r: r.function_code < 0x80)     # test that we are not an error
dassert(rr, lambda r: r.registers[0] == 10)       # test the expected value

rq = client.write_registers(1, [10]*8)
rr = client.read_input_registers(1,8)
dassert(rq, lambda r: r.function_code < 0x80)     # test that we are not an error
dassert(rr, lambda r: r.registers == [10]*8)      # test the expected value

rq = client.readwrite_registers(1, [20]*8)
rr = client.read_input_registers(1,8)
dassert(rq, lambda r: r.function_code < 0x80)     # test that we are not an error
dassert(rr, lambda r: r.registers == [20]*8)      # test the expected value

#---------------------------------------------------------------------------#
# close the client
#---------------------------------------------------------------------------#
client.close()