import unittest
import os
import DragosDumpIngester
import yaml
import json

test_folder = "testdata"
with open("DragosIngesterConfig.yaml", 'r') as stream:
    config = yaml.safe_load(stream)

class MyTestCase(unittest.TestCase):
    def test_drivers_datatype_mapping(self):
        with open(test_folder + os.sep + "driverquery.csv") as file:
            file_contents = file.read()
        # config, file_contents, parser_type, computer_json_dict, comp_name:
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, "Drivers CSV", {}, "testcase")
        parser = config['File Parsers']['Drivers'][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        self.assertEqual("drivers" in json_data, True)

    def test_system_datatype_mapping(self):
        with open(test_folder + os.sep + "system.csv") as file:
            file_contents = file.read()
        # config, file_contents, parser_type, computer_json_dict, comp_name:
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, "System CSV", {}, "testcase")
        parser = config['File Parsers']['System CSV'][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        self.assertEqual("system" in json_data, True)

    def test_wmic_product_datatype_mapping(self):
        with open(test_folder + os.sep + "wmic_product.txt") as file:
            file_contents = file.read()
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, "WMIC Product", {}, "testcase")
        parser = config['File Parsers']['WMIC Product'][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        self.assertEqual("product_wmic" in json_data, True)

    def test_workstation_ipconfig_windows_xp_datatype_mapping(self):
        with open(test_folder + os.sep + "ENG_WORKSTATION_network_ipconfig.txt") as file:
            file_contents = file.read()
        index_type = "Network IP Config"
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, index_type, {}, "testcase")
        parser = config['File Parsers'][index_type][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        self.assertEqual("network_ipconfig" in json_data, True)

if __name__ == '__main__':
    unittest.main()
