import unittest
import os
import DragosDumpIngester
import yaml
import json

test_folder = "testdata"
asset_id = ""
with open("DragosIngesterConfig.yaml", 'r') as stream:
    config = yaml.safe_load(stream)

class MyTestCase(unittest.TestCase):

    def test_drivers_indexing(self):
        DragosDumpIngester.main()
        asset_name = "testcase"
        csvfile = "drivers.csv"
        file_contents = DragosDumpIngester.open_file_for_parsing(test_folder + os.sep + csvfile)
        # config, file_contents, parser_type, computer_json_dict, comp_name:
        indextype = "Drivers CSV"
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_system_csv_indexing(self):
        asset_name = "testcase"
        csvfile ="system.csv"
        indextype = "System CSV"
        file_contents = DragosDumpIngester.open_file_for_parsing(test_folder + os.sep + csvfile)
        # config, file_contents, parser_type, computer_json_dict, comp_name:
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_connections_indexing(self):
        asset_name = "testcase"
        txtfile ="network_connections.txt"
        indextype = "Network Connections"
        file_contents = DragosDumpIngester.open_file_for_parsing(test_folder + os.sep + txtfile)
        # config, file_contents, parser_type, computer_json_dict, comp_name:
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_all_files_icacls_indexing(self):
        DragosDumpIngester.main()
        asset_name = "testcase"
        txtfile ="all_files_icacls.txt"
        indextype = "All Files ICACLS"
        file_contents = DragosDumpIngester.open_file_for_parsing(test_folder + os.sep + txtfile)
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_all_files_indexing(self):
        DragosDumpIngester.main()
        asset_name = "testcase"
        txtfile ="all_files.txt"
        indextype = "All Files"
        file_contents = DragosDumpIngester.open_file_for_parsing(test_folder + os.sep + txtfile)
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_wmic_process_indexing(self):
        asset_name = "testcase"
        txtfile ="wmic_process.txt"
        indextype = "WMIC Process"
        file_contents = DragosDumpIngester.open_file_for_parsing(test_folder + os.sep + txtfile)
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_system_info_qb_indexing(self):
        asset_name = "testcase"
        txtfile ="systeminfo.txt"
        indextype = "System Information"
        file_contents = DragosDumpIngester.open_file_for_parsing(test_folder + os.sep + txtfile)
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]  # QB
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_system_info_ip_indexing(self):
        asset_name = "testcase"
        txtfile ="systeminfo.txt"
        indextype = "System Information"
        file_contents = DragosDumpIngester.open_file_for_parsing(test_folder + os.sep + txtfile)
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][1]  # IP
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_routing_table_ipv4_indexing(self):
        asset_name = "testcase"
        txtfile ="network_routes.txt"
        indextype = "Network Routes"
        file_contents = DragosDumpIngester.open_file_for_parsing(test_folder + os.sep + txtfile)
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]  # IPv4
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_routing_table_ipv6_indexing(self):
        asset_name = "testcase"
        txtfile ="network_routes.txt"
        indextype = "Network Routes"
        file_contents = DragosDumpIngester.open_file_for_parsing(test_folder + os.sep + txtfile)
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][1]  # IPv6
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_arp_table_indexing(self):
        asset_name = "testcase"
        txtfile ="arp_tables.txt"
        indextype = "Network ARP Tables"
        file_contents = DragosDumpIngester.open_file_for_parsing(test_folder + os.sep + txtfile)
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_firewall_legacy_indexing(self):
        asset_name = "testcase"
        txtfile ="network_firewall_legacy.txt"
        indextype = "Firewall Legacy"
        file_contents = DragosDumpIngester.open_file_for_parsing(test_folder + os.sep + txtfile)
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_firewall_rules_indexing(self):
        asset_name = "testcase"
        txtfile ="network_firewall_rules.txt"
        indextype = "Firewall Rules"
        file_contents = DragosDumpIngester.open_file_for_parsing(test_folder + os.sep + txtfile)
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_firewall_profiles_indexing(self):
        asset_name = "testcase"
        txtfile ="network_firewall_profiles.txt"
        indextype = "Firewall Profiles"
        file_contents = DragosDumpIngester.open_file_for_parsing(test_folder + os.sep + txtfile)
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_network_ip_config_indexing(self):
        asset_name = "testcase"
        txtfile ="network_ipconfig.txt"
        indextype = "Network IP Config"
        file_contents = DragosDumpIngester.open_file_for_parsing(test_folder + os.sep + txtfile)
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_wmic_product_indexing(self):
        asset_name = "testcase"
        txtfile ="wmic_product.txt"
        indextype = "WMIC Product"
        file_contents = DragosDumpIngester.open_file_for_parsing(test_folder + os.sep + txtfile)
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_eng_workstation_ipconfig_windows_xp_indexing(self):
        asset_name = "testcase"
        txtfile ="ENG_WORKSTATION_network_ipconfig.txt"
        indextype = "Network IP Config"
        file_contents = DragosDumpIngester.open_file_for_parsing(test_folder + os.sep + txtfile)
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_eng_workstation_ipconfig_windows_xp_indexing(self):
        asset_name = "WIN-BFE972DMF97"
        txtfile ="PuttySessions.txt"
        indextype = "Windows Registry"
        #DragosDumpIngester.args.indexprefix = "unittest"
        file_contents = DragosDumpIngester.open_file_for_parsing(test_folder + os.sep + txtfile)
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_eng_workstation_ipconfig_windows_xp_indexing(self):
        asset_name = "ENG_WORKSTATION"
        txtfile ="registry_always_elevated.txt"
        indextype = "Windows Registry"
        #DragosDumpIngester.args.indexprefix = "unittest"
        file_contents = DragosDumpIngester.open_file_for_parsing(test_folder + os.sep + txtfile)
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_win7_drivers_csv_parsing(self):
        asset_name = "ENG_WORKSTATION"
        txtfile = "win7_drivers.csv"
        indextype = "Drivers CSV"
        #DragosDumpIngester.args.indexprefix = "unittest"
        file_contents = DragosDumpIngester.open_file_for_parsing(test_folder + os.sep + txtfile)
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_msinfo_indexing(self):
        asset_name = "ENG_WORKSTATION"
        txtfile = "HSTNADM40_msinfo.txt"
        indextype = "Windows System Information"
        #DragosDumpIngester.args.indexprefix = "unittest"
        DragosDumpIngester.args.exceptiondetails = True
        DragosDumpIngester.args.elasticnonbulk = True
        DragosDumpIngester.args.debug = True
        file_contents = DragosDumpIngester.open_file_for_parsing(test_folder + os.sep + txtfile)
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_cpu_load(self):
        asset_name = "ENG_WORKSTATION"
        txtfile = "performance_cpu_load.txt"
        indextype = "WMIC Performance CPU Load"
        #DragosDumpIngester.args.indexprefix = "unittest"
        DragosDumpIngester.args.exceptiondetails = True
        DragosDumpIngester.args.elasticnonbulk = True
        DragosDumpIngester.args.debug = True
        file_contents = DragosDumpIngester.open_file_for_parsing(test_folder + os.sep + txtfile)
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)

    def test_file_md5_hash(self):
        asset_name = "ENG_WORKSTATION"
        txtfile = "performance_cpu_load.txt"
        indextype = "md5_hash"
        #DragosDumpIngester.args.indexprefix = "unittest"
        DragosDumpIngester.args.exceptiondetails = True
        DragosDumpIngester.args.elasticnonbulk = True
        DragosDumpIngester.args.debug = True
        file_contents = DragosDumpIngester.open_file_for_parsing(test_folder + os.sep + txtfile)
        json_data = DragosDumpIngester.do_data_file_parsing(config, file_contents, indextype, {}, asset_name)
        parser = config['File Parsers'][indextype][0]
        index_name = parser['Index Name']
        json_data_type_mapped = DragosDumpIngester.do_datatype_mapping(parser, json_data[index_name])
        result = DragosDumpIngester.do_elastic_search_indexing(parser, json_data_type_mapped, asset_name, asset_id)
        self.assertEqual(result["result"] == "created", True)



if __name__ == '__main__':
    unittest.main()
