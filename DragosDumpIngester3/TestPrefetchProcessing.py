import unittest
import os
import DragosDumpIngester
import yaml
import json


with open("DragosIngesterConfig.yaml", 'r') as stream:
    config = yaml.safe_load(stream)

class TestPrefetchParsing(unittest.TestCase):

    def test_prefetch_file_parsing_load(self):
        prefix_path = ".\\testdata\\"
        prefetcher = DragosDumpIngester.prefetch_parser(prefix_path)
        self.assertEqual(True, True)


if __name__ == '__main__':
    unittest.main()
