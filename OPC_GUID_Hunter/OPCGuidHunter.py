import argparse
import os
import csv
import ctypes
import re

# OPC GUID Hunter
# Created By: Austin Scott - May 2019
# Searches TShark PCAP Dumps for OPC Related GUIDs

# Adding support for color in a windows console window is hard
force_linux_color_codes = False
STD_OUTPUT_HANDLE = -11
if os.name == 'nt':
    std_out_handle = ctypes.windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)


def color_blue():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 1)
        return ""
    else:
        return '\033[0;34m'


def color_green():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 2)
        return ""
    else:
        return '\033[0;32m'


def color_cyan():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 3)
        return ""
    else:
        return '\033[0;36m'


def color_red():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 4)
        return ""
    else:
        return '\033[0;31m'


def color_reset():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 7)
        return ""
    else:
        return '\033[0;39m'


print(color_cyan() + '                   //////////////////////////////////__                    ')
print('         ////////////////////////////////////////////////////////_                        ')
print('    //////////////////      ///////////////////////////                                   ')
print('  ////////               //////////////////////////                                       ')
print('///                  //////////////////////////                                           ')
print('/                 /////////    ////////////                   \\    \\                    ')
print('               ////////      ////////////         \\\\\\\\\\\\\\     \\\\   \\\\          ')
print('             //////         ///////////             /////////////  \\\\\\\\               ')
print('          //////           ///.//////      \\\\\\\\\\\\\\\\\\//////////////////\\\\       ')
print('        ////.            //// /////            /////           ////////\\\\               ')
print('      ///.              ///   ////     \\\\\\\\\\\\\\\\\\//               /////  \\\\\\   ')
print('    ///                 //   ////           ////                   //// .\\\\\\           ')
print('   /                   /     ////     \\\\\\\\\\\\///                   )///////\\\\\\    ')
print(' /                    /     ////        //////                    )////////\\\\\\         ')
print('                            .///   \\\\\\\\\\\\\\////                         //////\\\\  ')
print('                             ////       //////                           /////\\\\        ')
print('                              ////      //////                             ///\\\\\\      ')
print('                               /////     //////                                 \\        ')
print('                                 /////   ///////                                          ')
print('                                   ///////////////                                        ')
print('                                      ///////////////                                     ')
print('                                          ///////////////                                 ')
print('                                              //////////////                              ')
print('                                                  //////////////                          ')
print('                      ______                          ////////////////\\\\\\\\            ')
print('                //////////////////                        ////////////                    ')
print('              ///////////////////////                        /////////////\\\\\\\\\\      ')
print('            ///               ////////                          ///////////               ')
print('           ///                  ////////                          //////////\\\\\\\\      ')
print('           //                     ///////                          //////////\\  \\       ')
print('          //                       ///////                          /////////\\\\         ')
print('          \\\\                        ///////                         //////// \\\\\\     ')
print('          \\\\\\                        ////////                       ///////     \\     ')
print('           \\\\\\                         ///////                     /////               ')
print('            \\\\                         ////////                  /////                  ')
print('              \\\\                        /////////              /////                    ')
print('               \\\\  \\\\                    ////////////////////////                     ')
print('                 \\\\\\\\\\                    ////////////////                           ')
print('               \\\\\\\\\\\\\\\\\\                                                         ')
print(color_red()+"DRAGOS OPC GUID Hunter - V1.1 - Last Updated: May 17rd, 2019")
parser = argparse.ArgumentParser(description='A simple tool to Identify GUIDs from TShark Dumps!'+color_reset())
parser.add_argument("-txt", type=str, default="OPC_REPORT.txt", help='Specify the TShark report TXT file that we will attempt to map against (default: %(default)s)')
parser.add_argument("-csv", type=str, default="OPC_GUIDS.csv", help='A CSV file containing a list of Descriptions (Column 1) and GUIDs (Column 2) fields to be substituted into the TShark report text file and summarized in the console (default: %(default)s)')
parser.add_argument("-output", type=str, default="OPC_GUIDS_REPORT.txt", help='A Output text file that contains the GUID substitutions (default: %(default)s)')
parser.add_argument("-show",  dest='show_report', action='store_true', help='Print the updated OPC GUID report to the console when finished.')
args = parser.parse_args()
opc_guid_results = []
opc_guid_count = 0

if args.csv != "" and os.path.isfile(args.csv) and args.txt != "" and os.path.isfile(args.txt):
    with open(args.txt, "r+") as txtfilein:
        opc_report = ''.join(txtfilein.readlines())
        with open(args.csv, "r") as csvfilein:
            csv_reader = csv.reader(csvfilein)
            for csv_guid_search_line in csv_reader:
                if len(csv_guid_search_line[1]) >= 20:  # dont substitute empty lines...
                    src_str = re.compile(csv_guid_search_line[1], re.IGNORECASE)
                    opc_report = src_str.sub(csv_guid_search_line[1] + " = " + csv_guid_search_line[0], opc_report)
                    if re.search(csv_guid_search_line[1], opc_report, re.IGNORECASE):
                        opc_guid_results.append(csv_guid_search_line[1] + " = " + csv_guid_search_line[0])
                        opc_guid_count += 1
    if args.output != "":
        with open(args.output, "w") as output_report:
            output_report.write(opc_report)


if args.csv != "" and not os.path.isfile(args.csv):
    print color_red() + "ERROR! The OPC CSV GUID Mapping file cannot be found: " + args.csv
elif args.txt != "" and not os.path.isfile(args.txt):
    print color_red() + "ERROR! OPC Tshark Report file cannot be found " + args.txt
else:
    print color_blue()+"__________________________________________________________________\n\tOPC Tshark Report Path: " + args.txt + "\n\tOPC CSV GUID Mapping: " + args.csv
    print str(opc_guid_count) + " matching GUIDs were found in the report."
    print "The following OPC GUIDs were mapped and written to the Tshark report:"
    unique_opc_guid_results = list(set(opc_guid_results))
    unique_results_with_count = []
    for result in unique_opc_guid_results:
        allmatches = re.findall(result.replace("(", "\(").replace(")", "\)"), opc_report, re.IGNORECASE)
        unique_results_with_count.append(result + " - Matches Found: " + str(len(allmatches)))
    print("\n".join(unique_results_with_count))
    if args.show_report:
        print opc_report
    print color_reset()+"DONE!"


