# Vanquish

Vanquish is an Adversarial Tradecraft Execution Engine capable of orchestrating both a victim and an attacker then capturing the traffic generated.

![alt text](Dragos_Tradecraft_Execution_Engine.png "Dragos Tradecraft Execution Engine")

Demo Video can be viewed below:  

[Dragos Tradecraft Execution Engine Demo](https://bitbucket.org/dragosinc/vanquish/src/master/Vanquish%20Demo.mp4)


**BOLD**

