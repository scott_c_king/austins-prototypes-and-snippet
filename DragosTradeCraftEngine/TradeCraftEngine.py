import argparse
import os
import socket
import threading
import time
import math
import datetime
import sys
import yaml
import ConfigParser
import Vanquish2
import subprocess
import signal
import ctypes # Calm down, this has become standard library since 2.5

# Vanquish 3.0
# Created By: Austin Scott - November 2018
# Whats new in Vanquish 3.0?
# - CLI and HTTP Web Interface Support
# - Improved orchastration
# Dependencies:
# pip install pyyaml

CONFIG_FILE_INI = "config.ini"
HTTP_CLIENT_TIMEOUT = 60
AP_FILE_EXT = ".ini"


def log(log_file, request="", status="200", size=0, host="-", client="-", user="-", agent="-"):
    try:  # try to account of file locking issues
        with open(log_file, "a") as fileout:
            d = (datetime.datetime.now().strftime("[%d/%b/%Y:%H:%M:%S %z]"))  # [24/Mar/2017:19:37:57 +0000]
            request = request.replace('\n', "<br>").replace('\r', "")
            request = request.decode('utf-8', 'ignore').encode("utf-8")
            fileout.write("{host} {client} {user} {time} \"{request}\" {status} {size} - {agent}\n".format(
                host=host, client=client, user=user, time=d, request=request, status=status, size=size, agent=agent))
    except IOError as e:
        print e.errno
        print e


def load_yaml(yaml_file):
    stream = file(yaml_file, 'r')  # 'document.yaml' contains a single YAML document.
    return yaml.load(stream)


def process_http_request(http_request, address):
    http_request_dict = parse_http_request_to_dict(http_request)
    log(args.logfile, http_request_dict['url'], "200", len(http_request), http_request_dict['Host'], address, http_request_dict['User-Agent'])
    print http_request_dict['url']
    if "?" in http_request_dict['url']:  # Process GET Request
        url_path, get_request = sanitize_characters(http_request_dict['url']).split("?")
        if "execute=" in get_request:
            attack_plan = get_request.split("=")[1]
            attack_plan_path = config_yaml['attack plan folder'] + url_path + attack_plan + AP_FILE_EXT
            attack_plan_path = attack_plan_path.replace("/", os.path.sep)
            attack_plan_path = attack_plan_path.replace(os.path.sep+os.path.sep, os.path.sep)  # Remove duplicate seps
            return execute_attack_plan(attack_plan_path)
        if "pcapdownload=" in get_request:
            pcap_filename = get_request.split("=")[1]
            if not pcap_filename.lower().endswith(".pcap"):
                return html_error_not_found_page()
            pcap_path = config_yaml['pcaps folder'] + os.path.sep + pcap_filename
            pcap_download = config_yaml['http download response header']
            pcap_download += "Content-Disposition: attachment; filename=\""+pcap_filename+"\"\r\n"
            pcap_download += "Content-Length: " + str(os.stat(pcap_path).st_size) + "\r\n\r\n"
            with(open(pcap_path)) as file:
                pcap_download += file.read()
            return pcap_download
        if "pcapdelete=" in get_request:
            pcap_filename = get_request.split("=")[1]
            if not pcap_filename.lower().endswith(".pcap"):
                return html_error_not_found_page()
            pcap_path = config_yaml['pcaps folder'] + os.path.sep + pcap_filename
            os.remove(pcap_path)
            return html_pcaps_page(http_request_dict)
        if "pcapcontrol=" in get_request:
            pcap_function = get_request.split("=")[1]
            if "start" in pcap_function:
                return start_pcap_recording()
            if "stop" in pcap_function:
                return stop_pcap_recording()
            if "deleteall" in pcap_function:
                files = os.listdir(config_yaml['pcaps folder'])
                for pcap_file in files:
                    if pcap_file.lower().endswith(".pcap"):
                        os.remove(pcap_file)
                return html_pcaps_page()
        if "jobstop=" in get_request:
            job_to_stop = get_request.split("=")[1]
            return stop_job(job_to_stop)
        return html_error_not_found_page()
    elif len(http_request_dict['url'].split("/")) == 2:  # Root Path
        return html_home_page(http_request_dict)
    elif http_request_dict['url'].lower().endswith("pcaps/"):  # Pcap management
        return html_pcaps_page(http_request_dict)
    elif http_request_dict['url'].lower().endswith("jobs/"):  # Job management
        return html_jobs_page(http_request_dict)
    else:  # Sub Folders - Open ini files in sub folders
        return html_attack_page(http_request_dict)


def html_error_not_found_page():
    http_response_header = config_yaml['http not found response header']
    http_response_body = "<h1><a href='/'>..</a>/404 - FILE NOT FOUND!</h1>\n"  # Display Header
    http_content_length = "Content-Length: %d\r\n\r\n" % len(http_response_body)
    return http_response_header + http_content_length + http_response_body


def html_home_page(http_request_dict=""):
    http_response_header = config_yaml['http response header']
    http_response_body = html_file_header
    # Display ALL Attack Plans
    for path, dirs, files in os.walk(config_yaml['attack plan folder']):
        path = path.replace(config_yaml['attack plan folder'], "").replace("\\", "/")
        path_title = path.replace("/", "")
        http_response_body += "<tr><td><a href='" + path + "/' class='bin-name'>" + path + "</a></td>\n"
        http_response_body += "<td><ul class='function-list'>\n"
        for f in files:
            f = f.replace(AP_FILE_EXT, "")
            http_response_body += "<li><a href='" + path + "/#" + f + "'>" + f + "</a></li>\n"
        http_response_body += "</ul></td></tr>\n"
    http_response_body = http_response_body + html_file_footer
    http_content_length = "Content-Length: %d\r\n\r\n" % len(http_response_body)
    return http_response_header + http_content_length + http_response_body


def html_attack_page(http_request_dict):
    http_response_header = config_yaml['http response header']
    http_response_body = html_file_subheader
    path = config_yaml['attack plan folder'] + sanitize_characters(http_request_dict['url']).replace("/", os.path.sep)
    url = sanitize_characters(http_request_dict['url']).replace("/", os.path.sep)
    http_response_body += "<h1><a href='/'>..</a>" + url.replace("\\", "/") + "</h1>\n"  # Display Header
    if not os.path.isdir(path):
        return html_error_not_found_page()
    attack_files = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
    # Display Quick Navigation Buttons
    http_response_body += "<ul class='function-list'>\n"
    for attack_file in attack_files:
        attack_file = attack_file.replace(AP_FILE_EXT, "")
        http_response_body += "\t<li><a href='"+url.replace("\\", "/")+"#"+attack_file+"'>"+attack_file+"</a></li>\n"
    http_response_body += "</ul>\n"
    # Display Attack Plans Details
    for attack_file in attack_files:
        config_parser = ConfigParser.SafeConfigParser()
        config_parser.read(path + attack_file)
        attack_file = attack_file.replace(AP_FILE_EXT, "")
        http_response_body += "<h2 id='" + attack_file + "' class='function-name'>" + attack_file + "</h2>\n"
        http_response_body += "<ul class='function-list'><li><a href='?execute="+attack_file+"'>EXECUTE</a></li></ul>\n"
        for section_name in config_parser.sections():
            if section_name == "Documentation":
                for name, value in config_parser.items(section_name):
                    if name.lower() == "title":
                        http_response_body += '<h3>%s</h3>\n' % value
                    elif name.lower() == "description":
                        http_response_body += '<p>%s</p>\n' % value
            else:
                http_response_body += "<ul class='examples'>\n"
                http_response_body += '\t<li><p>' + section_name + "</p>\n"
                http_response_body += "<pre><code>"
                for name, value in config_parser.items(section_name):
                    if name.lower() == "description":
                        http_response_body += '<p>%s</p>\n' % value
                    else:
                        http_response_body += '  %s = %s\n' % (name, value)
                http_response_body += "</code></pre></li></ul>\n"
    http_response_body = http_response_body + html_file_subfooter
    http_content_length = "Content-Length: %d\r\n\r\n" % len(http_response_body)
    return http_response_header + http_content_length + http_response_body


def html_pcaps_page(http_request_dict=""):
    http_response_header = config_yaml['http response header']
    http_response_body = html_file_pcaps_header
    # Display PCAP Control Buttons
    http_response_body += "<ul class='function-list'>\n"
    http_response_body += "\t<li><a href='/?pcapcontrol=start'>START PCAP RECORDING</a></li>\n"
    http_response_body += "\t<li><a href='/?pcapcontrol=stop'>STOP PCAP RECORDING</a></li>\n"
    http_response_body += "\t<li><a href='/?pcapcontrol=deleteall'>DELETE ALL PCAPS</a></li>\n"
    http_response_body += "</ul>\n<br><br><br>\n"
    # Display ALL PCAPS
    for path, dirs, files in os.walk(config_yaml['pcaps folder']):
        files = os.listdir(path)
        for name in files:
            http_response_body += "<tr><td><a href='/?pcapdownload=" + name + "' class='bin-name'>" + name + "</a></td>\n"
            http_response_body += "<td>"
            full_path = os.path.join(path, name)
            inode = os.stat(full_path)
            http_response_body += convert_size(inode.st_size)
            http_response_body += "</td><td>\n"
            http_response_body += str(datetime.datetime.fromtimestamp(inode.st_ctime).strftime("%d/%b/%Y:%H:%M:%S %z"))
            http_response_body += "</td><td>\n"
            http_response_body += "<ul class='function-list'>"
            http_response_body += "<li><a href='/?pcapdelete=" + name + "' class='bin-name'>X</a></li>\n"
            http_response_body += "</ul></td></tr>\n"
    http_response_body = http_response_body + html_file_pcaps_footer
    http_content_length = "Content-Length: %d\r\n\r\n" % len(http_response_body)
    return http_response_header + http_content_length + http_response_body


def html_jobs_page(http_request_dict=""):
    http_response_header = config_yaml['http response header']
    http_response_body = html_file_jobs_header
    # Display JOBS Control Buttons
    for this_job_thread in job_threads:
        this_job_thread_name = this_job_thread.name
        this_job_thread_id = this_job_thread.ident
        job_name = this_job_thread.attack_plan.replace(AP_FILE_EXT,"").replace(config_yaml['attack plan folder'],"")
        start_time = str(this_job_thread.start_time.strftime("%d/%b/%Y:%H:%M:%S %z"))
        tooltip = this_job_thread_name + "&nbsp;" + str(this_job_thread_id) + " &#013;&#10;"
        tooltip += " Start&nbsp;Time:&nbsp;" + start_time + "&#013;&#10;"
        if not this_job_thread.isAlive():
            end_time = this_job_thread.end_time
            tooltip += " End&nbsp;Time:&nbsp;" + this_job_thread.end_time.strftime("%d/%b/%Y:%H:%M:%S %z") + " "
        else:
            end_time = datetime.datetime.now()
        elapsed_time = end_time - this_job_thread.start_time
        hours, remainder = divmod(elapsed_time.seconds, 3600)
        minutes, seconds = divmod(remainder, 60)
        duration_formatted = '%d:%02d:%02d' % (hours, minutes, seconds)

        tooltip += " Percentage&nbsp;Complete:&nbsp;" + str(this_job_thread.percentage_complete) + " &#013;&#10;"
        http_response_body += "<tr>\n"
        http_response_body += "\t<td><div id='bin-search-wrapper'>\n"
        http_response_body += "\t\t<ul  id='bin-search-filters' class='function-list'>"
        http_response_body += "\t\t<li>"
        http_response_body += "<a href='#"+this_job_thread.name+"' data-title='" + tooltip + "'>" + job_name + "</a>"
        http_response_body += "</li></ul>\n"
        http_response_body += "\t</div></td>\n"
        status = "Running"
        if not this_job_thread.isAlive():
            status = "Completed"
        http_response_body += "\t<td>" + status + "</td>\n"
        http_response_body += "\t<td>" + duration_formatted + "</td>\n"
        http_response_body += "\t<td>\n"
        http_response_body += "\t\t<ul class='function-list'>"
        if this_job_thread.isAlive():
            http_response_body += "\t\t<li><a href='/?jobstop=" + str(this_job_thread.name) + "' class='bin-name'>Stop</a></li>\n"
        http_response_body += "\t\t</ul>\n"
        http_response_body += "\t</td>\n"
        http_response_body += "</tr>\n"
    http_response_body = http_response_body + html_file_jobs_footer
    http_content_length = "Content-Length: %d\r\n\r\n" % len(http_response_body)
    return http_response_header + http_content_length + http_response_body


def parse_http_request_to_dict(http_request):
    headers = http_request.splitlines()
    firstLine = headers.pop(0)
    (action, url, version) = firstLine.split()
    d = {'action': action, 'url': url, 'version': version, 'body': '', 'host': 'unknown', 'User-Agent': 'unknown'}
    for h in headers:
        key_value = h.split(': ')
        if len(key_value) < 2:
            d['body'] = d['body'] + h
            continue
        field = key_value[0]
        value = key_value[1]
        d[field] = value
    return d


def stop_job(job_to_stop):
    for job in job_threads:
        if job.name == job_to_stop:
            # Raise Exception to thread
            job.end_time = datetime.datetime.now()
            job.percentage_complete = 100
            ret = ctypes.pythonapi.PyThreadState_SetAsyncExc(job.ident, ctypes.py_object(SystemExit))
    return html_jobs_page()


def execute_attack_plan(attack_plan_path):
    local_thread = threading.Thread(target=execute_attack_plan_thread, args=(attack_plan_path, CONFIG_FILE_INI))
    job_threads.append(local_thread)
    local_thread.start()
    return html_home_page()


def execute_attack_plan_thread(attack_plan_path, config_file=CONFIG_FILE_INI):
    t = threading.currentThread()
    t.attack_plan = attack_plan_path
    t.start_time = datetime.datetime.now()
    t.percentage_complete = 50
    vanquish_args = [
        '-configFile', 'config.ini',
        '-attackPlanFile', attack_plan_path,
        '-noResume'
        ]
    old_sys_argv = sys.argv
    sys.argv = [old_sys_argv[0]] + vanquish_args
    vanquish2 = Vanquish2.Vanquish(sys.argv)
    try:
        vanquish2.main()
    except:
        True
    t.end_time = datetime.datetime.now()
    t.percentage_complete = 100


def start_pcap_recording():
    local_thread = threading.Thread(target=start_pcap_recording_thread)
    pcap_threads.append(local_thread)
    local_thread.start()
    return html_pcaps_page()


def start_pcap_recording_thread():
    t = threading.currentThread()
    pcap_file_path = os.path.join(config_yaml["pcaps folder"], "LiveCapture.pcap").replace(".pcap", str(t.ident)+".pcap")
    t.is_running = True
    t.pcap_file = pcap_file_path
    execute_process = [
                       config_yaml["packet capture tshark path"],
                       "-i", config_yaml["packet capture interface"],
                       "-w", pcap_file_path
                       ]
    # "-f", config_yaml["packet capture filter"],
    t.subprocess = subprocess.Popen(
        execute_process,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )
    while t.is_running:
        tshark_output = t.subprocess.stdout.readline()
        if tshark_output != '':
            print tshark_output
    # process.stdin.write(f"{message.strip()}\n".encode("utf-8"))
    # process.stdin.flush()


def stop_pcap_recording():
    for pcap_thread in pcap_threads:
        if pcap_thread.is_running:
            if hasattr(signal, 'CTRL_C_EVENT'):
                # windows. Need CTRL_C_EVENT to raise the signal in the whole process group
                pcap_thread.subprocess.send_signal(signal.CTRL_C_EVENT)  ## Send interrupt signal
            else:
                # unix.
                pcap_thread.subprocess.send_signal(signal.SIGINT)  ## Send interrupt signal
            pcap_thread.subprocess.stdin.write(str(0x03)) ## CTRL + C
            pcap_thread.subprocess.stdin.flush()
            time.sleep(.1)
            pcap_thread.is_running = False
            #pcap_thread.subprocess.stdin.close()
            #pcap_thread.subprocess.terminate()
    return html_pcaps_page()


def sanitize_characters(user_input):
    user_input = user_input.replace("%20", " ")
    user_input = user_input.replace("%", "")
    user_input = user_input.replace("|", "")
    user_input = user_input.replace("`", "")
    user_input = user_input.replace("..", "")
    user_input = user_input.replace(";", "")
    user_input = user_input.replace("\"", "")
    user_input = user_input.replace("\'", "")
    user_input = user_input.replace("<", "")
    user_input = user_input.replace(">", "")
    user_input = user_input.replace("\x0a", "")
    user_input = user_input.replace("\x0d", "")
    user_input = user_input.replace("\x1a", "")
    user_input = user_input.replace("\x00", "")
    return user_input


def convert_size(size_bytes):
   if size_bytes == 0:
       return "0B"
   size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
   i = int(math.floor(math.log(size_bytes, 1024)))
   p = math.pow(1024, i)
   s = round(size_bytes / p, 2)
   return "%s %s" % (s, size_name[i])


def vanquish_cli():
    cmd = raw_input("?>")
    if cmd == "help":
        print "Vanquish CLI Help:"
        print " help - Display list of available commands"
        print " stop http server - Stop the http server"
        print " start http server - Start the http server"
        print " execute <path to attack plan> - Execute an attack plan"
        print " quit - Exit the application"
    elif cmd == "stop http server":
        print "Stopping HTTP Server..."
        http_server.stop()
        time.sleep(1)
    elif cmd == "start http server":
        print "Starting HTTP Server..."
        http_server.__init__(load_yaml(args.yaml))
        http_server.listener()
        time.sleep(1)
    elif cmd == "quit":
        print "Exiting..."
        http_server.stop()
        time.sleep(1)
        sys.exit(0)
    elif cmd.startswith("execute"):
        attack_plan_path = sanitize_characters(cmd.replace("execute ", ""))
        print "Executing Attack Plan: " + attack_plan_path
        execute_attack_plan(attack_plan_path)


class ThreadedHttpServer(object):
    def __init__(self, server_yaml):
        self.is_listening = True
        self.threads = []
        self.host = ''
        self.server_yaml = server_yaml
        self.recv_buffer = 1024
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
        self.sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
        self.sock.bind((self.host, self.server_yaml['http server port']))
        print "Listening on port " + str(self.server_yaml['http server port'])
        log(args.logfile, "Listening on port: " + str(self.server_yaml['http server port']),
            "200", 0, "127.0.0.1", "Vanquish")
        self.sock.listen(5)

    def listener(self):
        local_thread = threading.Thread(target=self.listener_thread, args=())
        self.threads.append(local_thread)
        local_thread.start()

    def listener_thread(self):
        if self.is_listening:
            listen_thread = threading.Thread(target=self.listen_thread, args=())
            self.threads.append(listen_thread)
            listen_thread.start()

    def listen_thread(self):
        t = threading.currentThread()
        while getattr(t, "do_run", True) and self.is_listening:
            client, address = self.sock.accept()
            client.settimeout(HTTP_CLIENT_TIMEOUT)
            local_thread = threading.Thread(target=self.listen_to_client, args=(client, address))
            self.threads.append(local_thread)
            local_thread.start()
            log(args.logfile,
                "accept_new_connection() - thread.listen()" + str(self.server_yaml['http server port']),
                "200", 0, "127.0.0.1", "Vanquish")

    def listen_to_client(self, client, address):
        size = 1024
        if 'default response' in self.server_yaml:
            client.send("".join(self.server_yaml['default response']))
        t = threading.currentThread()
        while getattr(t, "do_run", True):
            try:
                data = client.recv(size)
            except:
                return False  # timeouts and other errors we can probably ignore...
            if data:
                response = process_http_request(data, address)
                client.send(response)
                log(args.logfile, "Sent Response Length: "+str(len(response)))
                client.close()
                return True
            else:
                log(args.logfile, "Client Disconnected")
                return False

    def stop(self):
        local_thread = threading.Thread(target=self.stop_thread, args=())
        local_thread.start()

    def stop_thread(self):
        if self.is_listening:
            self.sock.close()
            self.is_listening = False
            log(args.logfile,
                "Http Server is Stopping: Threat Count = " + str(len(self.threads)),
                "200", 0, "127.0.0.1", "Vanquish")
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # cleanly shutdown final HTTP thread
            s.connect(("127.0.0.1", int(self.server_yaml['http server port'])))
            s.send("GET / HTTP/1.0\r\n")
            s.close()
            for this_thread in self.threads:
                this_thread.join()
                if this_thread.isAlive():
                    this_thread.do_run = False


if __name__ == "__main__":
    print('VANQUISH 3.0 ALPHA')
    parser = argparse.ArgumentParser(
        description='Vanquish is Kali Linux based Enumeration and Attack Orchestrator')
    parser.add_argument("-yaml", type=str, default="setup.yaml",  help='Setup YAML file (default: %(default)s)')
    parser.add_argument("-logfile", type=str, default="vanquish.log",
                        help='Specify name and location of the vanquish log file (default: %(default)s)')
    args = parser.parse_args()
    config_yaml = load_yaml(args.yaml)
    if not os.path.exists(config_yaml['attack plan folder']):
        os.makedirs(config_yaml['attack plan folder'])
    if not os.path.exists(config_yaml['pcaps folder']):
        os.makedirs(config_yaml['pcaps folder'])
    http_server = ThreadedHttpServer(config_yaml)
    job_threads = []
    pcap_threads = []
    http_server.listener()
    with open(config_yaml['template path'] + os.path.sep + config_yaml['html header file']) as file_header:
        html_file_header = "".join(file_header.readlines())
    with open(config_yaml['template path'] + os.path.sep + config_yaml['html footer file']) as file_footer:
        html_file_footer = "".join(file_footer.readlines())
    with open(config_yaml['template path'] + os.path.sep + config_yaml['html subheader file']) as file_header:
        html_file_subheader = "".join(file_header.readlines())
    with open(config_yaml['template path'] + os.path.sep + config_yaml['html subfooter file']) as file_footer:
        html_file_subfooter = "".join(file_footer.readlines())
    with open(config_yaml['template path'] + os.path.sep + config_yaml['html pcaps header file']) as file_header:
        html_file_pcaps_header = "".join(file_header.readlines())
    with open(config_yaml['template path'] + os.path.sep + config_yaml['html pcaps footer file']) as file_footer:
        html_file_pcaps_footer = "".join(file_footer.readlines())
    with open(config_yaml['template path'] + os.path.sep + config_yaml['html jobs header file']) as file_header:
        html_file_jobs_header = "".join(file_header.readlines())
    with open(config_yaml['template path'] + os.path.sep + config_yaml['html jobs footer file']) as file_footer:
        html_file_jobs_footer = "".join(file_footer.readlines())
    while not os.path.isfile("terminate"):  # socket listening loop - this doesnt really work anymore!
        try:
            vanquish_cli()
        except KeyboardInterrupt:
            print 'Interrupted - Shutting down Vanquish threads...'
            http_server.stop()
            time.sleep(1)
            sys.exit(0)

    print "Terminated Vanquish!"
    exit(0)
